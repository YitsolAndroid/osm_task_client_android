package com.wdget.yitsol.osm_app.model;

public class OrderTaskModel
{
    private String order_taskName;
    private boolean task_status;

    public OrderTaskModel() {
    }

    public OrderTaskModel(String order_taskName, boolean task_status) {
        this.order_taskName = order_taskName;
        this.task_status = task_status;
    }

    public String getOrder_taskName() {
        return order_taskName;
    }

    public void setOrder_taskName(String order_taskName) {
        this.order_taskName = order_taskName;
    }

    public boolean isTask_status() {
        return task_status;
    }

    public void setTask_status(boolean task_status) {
        this.task_status = task_status;
    }
}
