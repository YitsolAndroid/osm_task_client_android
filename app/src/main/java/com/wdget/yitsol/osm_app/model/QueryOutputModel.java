package com.wdget.yitsol.osm_app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QueryOutputModel implements Parcelable
{
    @SerializedName("_order_type")
    @Expose
    private String orderType;
    @SerializedName("_date_pos_started")
    @Expose
    private String datePosStarted;
    @SerializedName("_order_source")
    @Expose
    private String orderSource;
    @SerializedName("_order_state")
    @Expose
    private String orderState;
    @SerializedName("_num_remarks")
    @Expose
    private String numRemarks;
    @SerializedName("_grace_period_completion_date")
    @Expose
    private String gracePeriodCompletionDate;
    @SerializedName("_date_pos_created")
    @Expose
    private String datePosCreated;
    @SerializedName("_header")
    @Expose
    private HeaderModel header;
    @SerializedName("_execution_mode")
    @Expose
    private String executionMode;
    @SerializedName("_version")
    @Expose
    private String version;
    @SerializedName("_task_id")
    @Expose
    private String taskId;
    @SerializedName("_namespace")
    @Expose
    private String namespace;
    @SerializedName("_reference_number")
    @Expose
    private String referenceNumber;
    @SerializedName("_current_order_state")
    @Expose
    private String currentOrderState;
    @SerializedName("_priority")
    @Expose
    private String priority;
    @SerializedName("_order_seq_id")
    @Expose
    private String orderSeqId;
    @SerializedName("_target_order_state")
    @Expose
    private String targetOrderState;
    @SerializedName("_process_status")
    @Expose
    private String processStatus;
    @SerializedName("_user")
    @Expose
    private String user;
    @SerializedName("_order_hist_seq_id")
    @Expose
    private String orderHistSeqId;
    @SerializedName("_compl_date_expected")
    @Expose
    private String complDateExpected;


    protected QueryOutputModel(Parcel in) {
        orderType = in.readString();
        datePosStarted = in.readString();
        orderSource = in.readString();
        orderState = in.readString();
        numRemarks = in.readString();
        gracePeriodCompletionDate = in.readString();
        datePosCreated = in.readString();
        header = in.readParcelable(HeaderModel.class.getClassLoader());
        executionMode = in.readString();
        version = in.readString();
        taskId = in.readString();
        namespace = in.readString();
        referenceNumber = in.readString();
        currentOrderState = in.readString();
        priority = in.readString();
        orderSeqId = in.readString();
        targetOrderState = in.readString();
        processStatus = in.readString();
        user = in.readString();
        orderHistSeqId = in.readString();
        complDateExpected = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderType);
        dest.writeString(datePosStarted);
        dest.writeString(orderSource);
        dest.writeString(orderState);
        dest.writeString(numRemarks);
        dest.writeString(gracePeriodCompletionDate);
        dest.writeString(datePosCreated);
        dest.writeParcelable(header, flags);
        dest.writeString(executionMode);
        dest.writeString(version);
        dest.writeString(taskId);
        dest.writeString(namespace);
        dest.writeString(referenceNumber);
        dest.writeString(currentOrderState);
        dest.writeString(priority);
        dest.writeString(orderSeqId);
        dest.writeString(targetOrderState);
        dest.writeString(processStatus);
        dest.writeString(user);
        dest.writeString(orderHistSeqId);
        dest.writeString(complDateExpected);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<QueryOutputModel> CREATOR = new Creator<QueryOutputModel>() {
        @Override
        public QueryOutputModel createFromParcel(Parcel in) {
            return new QueryOutputModel(in);
        }

        @Override
        public QueryOutputModel[] newArray(int size) {
            return new QueryOutputModel[size];
        }
    };

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getDatePosStarted() {
        return datePosStarted;
    }

    public void setDatePosStarted(String datePosStarted) {
        this.datePosStarted = datePosStarted;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public String getNumRemarks() {
        return numRemarks;
    }

    public void setNumRemarks(String numRemarks) {
        this.numRemarks = numRemarks;
    }

    public String getGracePeriodCompletionDate() {
        return gracePeriodCompletionDate;
    }

    public void setGracePeriodCompletionDate(String gracePeriodCompletionDate) {
        this.gracePeriodCompletionDate = gracePeriodCompletionDate;
    }

    public String getDatePosCreated() {
        return datePosCreated;
    }

    public void setDatePosCreated(String datePosCreated) {
        this.datePosCreated = datePosCreated;
    }

    public HeaderModel getHeader() {
        return header;
    }

    public void setHeader(HeaderModel header) {
        this.header = header;
    }

    public String getExecutionMode() {
        return executionMode;
    }

    public void setExecutionMode(String executionMode) {
        this.executionMode = executionMode;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getCurrentOrderState() {
        return currentOrderState;
    }

    public void setCurrentOrderState(String currentOrderState) {
        this.currentOrderState = currentOrderState;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getOrderSeqId() {
        return orderSeqId;
    }

    public void setOrderSeqId(String orderSeqId) {
        this.orderSeqId = orderSeqId;
    }

    public String getTargetOrderState() {
        return targetOrderState;
    }

    public void setTargetOrderState(String targetOrderState) {
        this.targetOrderState = targetOrderState;
    }

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getOrderHistSeqId() {
        return orderHistSeqId;
    }

    public void setOrderHistSeqId(String orderHistSeqId) {
        this.orderHistSeqId = orderHistSeqId;
    }

    public String getComplDateExpected() {
        return complDateExpected;
    }

    public void setComplDateExpected(String complDateExpected) {
        this.complDateExpected = complDateExpected;
    }

}
