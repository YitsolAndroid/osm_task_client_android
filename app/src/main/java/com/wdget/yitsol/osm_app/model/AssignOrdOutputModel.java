package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssignOrdOutputModel
{
    @SerializedName("AssignOrder.Response")
    @Expose
    private AssignOrderResponse assignOrderResponse;

    public AssignOrderResponse getAssignOrderResponse() {
        return assignOrderResponse;
    }

    public void setAssignOrderResponse(AssignOrderResponse assignOrderResponse) {
        this.assignOrderResponse = assignOrderResponse;
    }
}
