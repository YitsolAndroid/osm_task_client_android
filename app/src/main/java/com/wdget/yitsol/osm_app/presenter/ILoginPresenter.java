package com.wdget.yitsol.osm_app.presenter;

import android.content.Context;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.wdget.yitsol.osm_app.model.LoginInput;
import com.wdget.yitsol.osm_app.presenter.base.IBasePresenter;


public interface ILoginPresenter extends IBasePresenter
{
    void performUserLogin(Context context, EditText edt_username, EditText edt_password, LinearLayout linearLayout);

    void performUserLogout(Context context);
}
