package com.wdget.yitsol.osm_app.presenter.impl;

import android.content.Context;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.wdget.yitsol.osm_app.model.LoginInput;
import com.wdget.yitsol.osm_app.model.LoginOutput;
import com.wdget.yitsol.osm_app.presenter.ILoginPresenter;
import com.wdget.yitsol.osm_app.repository.ILoginRepository;
import com.wdget.yitsol.osm_app.repository.impl.LoginRepository;
import com.wdget.yitsol.osm_app.utils.AppPreference;
import com.wdget.yitsol.osm_app.view.activity.LoginActivity;
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity;

public class LoginPresenter implements ILoginPresenter
{
    private LoginActivity view;
    private BaseNavigationActivity view2;
    private ILoginRepository repository;
    private ILoginRepository.OnUserLoginCallback loginCallback;
    private ILoginRepository.OnUserLogoutCallback logoutCallback;

    public LoginPresenter(LoginActivity view) {
        this.view = view;
        repository=new LoginRepository();
    }

    public LoginPresenter(BaseNavigationActivity view) {
        this.view2 = view;
        repository=new LoginRepository();
    }

    @Override
    public void subscribeCallbacks() {
        loginCallback=new ILoginRepository.OnUserLoginCallback() {
            @Override
            public void onSuccess(LoginOutput output) {
                view.hideloader();
                view.loginComplete();
            }

            @Override
            public void onError(String message) {
                view.hideloader();
                view.showSnackMessage(message);
            }
        };

        logoutCallback=new ILoginRepository.OnUserLogoutCallback() {
            @Override
            public void onSuccess(LoginOutput output) {
                view2.hideloader();
                view2.logoutComplete();
            }

            @Override
            public void onError(String message) {
                view2.hideloader();
                view2.showMessage(message);
            }
        };
    }

    @Override
    public void unSubscribeCallbacks() {
        loginCallback=null;
        logoutCallback = null;
    }

    @Override
    public void performUserLogin(Context context,EditText edt_username, EditText edt_password, LinearLayout linearLayout) {
        if(validateLoginField(context,edt_username.getText().toString().trim(),edt_password.getText().toString().trim(),linearLayout))
        {
            view.showLoaderNew();
            repository.performUserLogin(context,edt_username.getText().toString().trim(),edt_password.getText().toString().trim()
            ,loginCallback);
        }
    }

    @Override
    public void performUserLogout(Context context) {
        view2.showLoaderNew();
        repository.performUserLogout(context,logoutCallback);
    }


    private boolean validateLoginField(Context context, String username, String password, LinearLayout linearLayout)
    {
        if(username.equals("")&&username.length()==0&&username.isEmpty())
        {
            view.showCustomSnackBar("Please Enter Username",linearLayout);
            return false;
        }
        else if(password.equals("")&&password.length()==0&&password.isEmpty())
        {
            view.showCustomSnackBar("Please Enter Password",linearLayout);
            return false;
        }
        else if(checkPublicIPAndPort(context))
        {
            view.showCustomSnackBar("Please provide public IP and Port in settings",linearLayout);
            return false;
        }
        else
        {
            return true;
        }

    }

    private boolean checkPublicIPAndPort(Context context) {
        AppPreference appPreference=new AppPreference(context);
        if(appPreference.getPublicIP().equals("NA")&&appPreference.getPort().equals("NA"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


}
