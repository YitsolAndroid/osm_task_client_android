package com.wdget.yitsol.osm_app.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.TransitionData
import com.wdget.yitsol.osm_app.utils.CurrentDate
import kotlinx.android.synthetic.main.row_processhistory_item.view.*


public class ProcessHistAdapter(context: Context,ordtransitiondatalist: List<TransitionData>): RecyclerView.Adapter<ProcessHistAdapter.ViewHolder>()
{
    private var context: Context? = null
    private var ordtransitiondatalist: List<TransitionData>?=null

    init {
        this.context=context
        this.ordtransitiondatalist=ordtransitiondatalist
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val processhistview = LayoutInflater.from(context).inflate(R.layout.row_processhistory_item, parent, false)
        return ViewHolder(processhistview)
    }

    override fun getItemCount(): Int {
        return this.ordtransitiondatalist!!.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(this.ordtransitiondatalist?.get(position), position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindItems(ordProcssHist: TransitionData?, position: Int) {
            itemView.txt_Task.text="Task : "+ordProcssHist?.taskID+" Task Type : "+ordProcssHist?.taskType
            itemView.txt_ActualDuration.text="Actual Duration :"+convertMillisToTime(ordProcssHist!!.actualDuration)
            itemView.txt_started.text="Start Date : "+CurrentDate().getOSMOrderFormattedDate(ordProcssHist.startDate)
            itemView.txt_complted.text="Exp Completed Date : "+CurrentDate().getOSMOrderFormattedDate(ordProcssHist.expectedCompletionDate)
        }

    }

    fun convertMillisToTime(millivalue:Long): String
    {
        val millis = millivalue % 1000
        val second = millivalue / 1000 % 60
        val minute = millivalue / (1000 * 60) % 60
        val hour = millivalue / (1000 * 60 * 60) % 24

        val time = String.format("%02d:%02d:%02d.%d"+"(hr:min:sec:mils)", hour, minute, second, millis)

        return time
    }

    fun refreshAdapter(ordtransitiondatalist: List<TransitionData>)
    {
        this.ordtransitiondatalist=ordtransitiondatalist
        notifyDataSetChanged()
    }


}