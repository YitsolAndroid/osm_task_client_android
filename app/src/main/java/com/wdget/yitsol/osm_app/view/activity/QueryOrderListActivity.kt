package com.wdget.yitsol.osm_app.view.activity

import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.QueryOutputModel
import com.wdget.yitsol.osm_app.presenter.IReasonDialogListener
import com.wdget.yitsol.osm_app.presenter.IWorkListItemClickListener
import com.wdget.yitsol.osm_app.presenter.IWorkListPresenter
import com.wdget.yitsol.osm_app.presenter.impl.WorkListPresenter
import com.wdget.yitsol.osm_app.utils.AppConstant
import com.wdget.yitsol.osm_app.utils.LinearLayoutSpaceItemDecoration
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity
import com.wdget.yitsol.osm_app.view.adapter.QueryOrderAdapter
import com.wdget.yitsol.osm_app.view.dialog.ReasonDialog
import kotlinx.android.synthetic.main.worklist.*

class QueryOrderListActivity : BaseNavigationActivity()
{
    private var container: FrameLayout?=null
    private var presenter: IWorkListPresenter? = null
    private var queryorderlist:MutableList<QueryOutputModel>?=null
    private var queryOrderAdapter:QueryOrderAdapter?=null
    private var fromclass:String?=null

    override fun getActContext(): Context {
       return this@QueryOrderListActivity
    }

    override fun initScreen() {
        container=getContainerLayout()
        container?.addView(inflater?.inflate(R.layout.worklist, null),
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        presenter=WorkListPresenter(this)
        getDataFromIntent()
        setOrderListAdapter(queryorderlist)
        edt_orderId.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length > 0) {
                    queryOrderAdapter?.filter?.filter(s.toString().trim())
                } else {
                    queryOrderAdapter?.filter?.filter(s.toString().trim())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }

    private fun setOrderListAdapter(queryorderlist: MutableList<QueryOutputModel>?) {
        rcly_worklist.visibility= View.VISIBLE
        txt_noordfound.visibility=View.GONE
        rcly_worklist.layoutManager = LinearLayoutManager(getActContext(), LinearLayoutManager.VERTICAL, false)
        rcly_worklist.addItemDecoration(LinearLayoutSpaceItemDecoration(5))
        queryOrderAdapter= QueryOrderAdapter(getActContext(),queryorderlist!!)
        queryOrderAdapter?.registerItemClickListener(object:IWorkListItemClickListener
        {
            override fun OnClickEditor(orderId: String?, taskId: String?, source: String?, orderType: String?, ref_no: String?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun OnClickChangeStatus(orderId: String?, taskId: String?, ref_no: String?) {
                val chngeStatusIntent = Intent(getActContext(), ChangeStatusActivity::class.java)
                chngeStatusIntent.putExtra(AppConstant.ORDER_ID, orderId)
                chngeStatusIntent.putExtra(AppConstant.TASK_ID, taskId)
                chngeStatusIntent.putExtra(AppConstant.REFERNCE, ref_no)
                startActivity(chngeStatusIntent)
            }

            override fun OnClickProcessHistory(orderId: String?) {
                val procHistIntent = Intent(getActContext(), ProcessHistActivity::class.java)
                procHistIntent.putExtra(AppConstant.ORDER_ID, orderId)
                startActivity(procHistIntent)
            }

            override fun OnSuspendOrder(orderId: String?, taskId: String?) {
                showReasonDialog(AppConstant.ACTIONSUSPEND, orderId, taskId!!)
            }

            override fun OnCancelOrder(orderId: String?, taskId: String?) {
                showReasonDialog(AppConstant.ACTIONCANCEL, orderId, taskId!!)
            }

            override fun OnResumeOrder(orderId: String?, taskId: String?) {
                showReasonDialog(AppConstant.ACTIONRESUME, orderId, taskId!!)
            }

            override fun OnAbortOrder(orderId: String?, taskId: String?) {
                showReasonDialog(AppConstant.ACTIONABORT, orderId, taskId!!)
            }

            override fun showSnackMessage(message: String?) {
                showCustomSnackBar(message!!,ll_worklist)
            }

            override fun showWorkList() {
                txt_noordfound.visibility=View.GONE
                rcly_worklist.visibility=View.VISIBLE
            }

            override fun hideWorkList() {
                txt_noordfound.visibility=View.VISIBLE
                rcly_worklist.visibility=View.GONE
            }

        })
        rcly_worklist.adapter=queryOrderAdapter
    }

    private fun showReasonDialog(actionsuspend: String, orderId: String?, taskId: String) {
        val reasonDialog = ReasonDialog(getActContext(), orderId, actionsuspend)
        reasonDialog.setCanceledOnTouchOutside(false)
        reasonDialog.registerClickListener(object : IReasonDialogListener {
            override fun onOkButtonClick(reason: String?, actiontype: String?) {

                presenter?.changeOrderCurrentState(getActContext(), reason, actiontype, orderId, taskId)
            }

            override fun showSnackMessage(message: String?) {
                showCustomSnackBar(message!!, ll_worklist)
            }
        })

        reasonDialog.show()
    }

    fun changeOrderStateComplete(reason: String, actiontype: String, orderId: String, taskId: String) {
        if (actiontype.equals(AppConstant.ACTIONSUSPEND)) {
            presenter?.changeOrderStatebyId(getActContext(), reason, actiontype, orderId, taskId)
        } else if (actiontype.equals(AppConstant.ACTIONCANCEL)) {
            presenter?.changeOrderStatebyId(getActContext(),reason,actiontype,orderId,taskId)
        }
        else if(actiontype.equals(AppConstant.ACTIONRESUME))
        {
            presenter?.changeOrderStatebyId(getActContext(),reason,actiontype,orderId,taskId)
        }
        else if(actiontype.equals(AppConstant.ACTIONABORT))
        {
            presenter?.changeOrderStatebyId(getActContext(),reason,actiontype,orderId,taskId)
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        if(fromclass!!.equals(AppConstant.QUERYCLASS))
        {
            gotobackScreen()
        }
        else if(fromclass.equals(AppConstant.REPORTCLASS))
        {
            gotoReportScreen()
        }

    }

    private fun gotoReportScreen() {
        val reportIntent=Intent(getActContext(),ReportingActivity::class.java)
        startActivity(reportIntent)
    }

    fun OrderStateChangeComplete()
    {
        showCustomSnackBar("ORDER STATE CHANGE SUCCESSFULLY",ll_worklist)
        gotobackScreen()

    }

    private fun gotobackScreen() {
        val queryIntent=Intent(getActContext(),QueryActivity::class.java)
        queryIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(queryIntent)
    }

    private fun getDataFromIntent() {
        val bundle=intent.extras
        fromclass=bundle.getString(AppConstant.FROM_CLASS)
        queryorderlist=bundle.getParcelableArrayList(AppConstant.QUERYORDERLIST)
    }


}
