package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrdProcessHistInputModel
{
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("orderId")
    @Expose
    private String orderId;
    @SerializedName("publicIP")
    @Expose
    private String publicIP;
    @SerializedName("port")
    @Expose
    private String port;

    public OrdProcessHistInputModel(String username, String password, String orderId, String publicIP, String port) {
        this.username = username;
        this.password = password;
        this.orderId = orderId;
        this.publicIP = publicIP;
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPublicIP() {
        return publicIP;
    }

    public void setPublicIP(String publicIP) {
        this.publicIP = publicIP;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
