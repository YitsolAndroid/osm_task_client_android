package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TaskStatesNStatuses
{
    @SerializedName("Completed")
    @Expose
    private Completed completed;
    @SerializedName("Received")
    @Expose
    private String received;
    @SerializedName("Accepted")
    @Expose
    private String accepted;
    @SerializedName("Assigned")
    @Expose
    private Assigned assigned;
    @SerializedName("NotStarted")
    @Expose
    private String notStarted;


    public Completed getCompleted() {
        return completed;
    }

    public void setCompleted(Completed completed) {
        this.completed = completed;
    }

    public String getReceived() {
        return received;
    }

    public void setReceived(String received) {
        this.received = received;
    }

    public String getAccepted() {
        return accepted;
    }

    public void setAccepted(String accepted) {
        this.accepted = accepted;
    }

    public Assigned getAssigned() {
        return assigned;
    }

    public void setAssigned(Assigned assigned) {
        this.assigned = assigned;
    }

    public String getNotStarted() {
        return notStarted;
    }

    public void setNotStarted(String notStarted) {
        this.notStarted = notStarted;
    }
}
