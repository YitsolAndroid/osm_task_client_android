package com.wdget.yitsol.osm_app.repository;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.ProcessMnemonicModel;

import java.util.List;

public interface IActionEditorRepository
{
    interface OnGetMnemonicByOrderIdCallback
    {
        void onSuccess(List<ProcessMnemonicModel> datalist, List<String> mNemoniclist);
        void onError(String message);
    }

    interface OnGetProcessPriorityCallback
    {
        void onSuccess(List<String> prioritylist);
        void onError(String message);
    }

    void getProcessMnemonic(Context context,String orderId, String taskId,OnGetMnemonicByOrderIdCallback callback);

    void getProcessPriority(Context context, OnGetProcessPriorityCallback callback);
}
