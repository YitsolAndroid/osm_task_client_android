package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Completed
{
    @SerializedName("submit")
    @Expose
    private String submit;
    @SerializedName("delete")
    @Expose
    private String delete;
    @SerializedName("next")
    @Expose
    private String next;
    @SerializedName("cancel")
    @Expose
    private String cancel;
    @SerializedName("back")
    @Expose
    private String back;
    @SerializedName("finish")
    @Expose
    private String finish;

    public String getSubmit() {
        return submit;
    }

    public void setSubmit(String submit) {
        this.submit = submit;
    }

    public String getDelete() {
        return delete;
    }

    public void setDelete(String delete) {
        this.delete = delete;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }
}
