package com.wdget.yitsol.osm_app.view.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wdget.yitsol.osm_app.R;
import com.wdget.yitsol.osm_app.presenter.IReasonDialogListener;

public class ReasonDialog extends AppCompatDialog
{

    private Context context;
    private TextView orderId;
    private EditText edt_reason;
    private Button buttonOk,buttonCancel;
    private String ordId,ACTIONTYPE;
    private IReasonDialogListener listener;


    public ReasonDialog(Context context,String orderId,String ACTIONTYPE) {
        super(context);
        this.context=context;
        this.ordId=orderId;
        this.ACTIONTYPE=ACTIONTYPE;
    }

    public ReasonDialog(Context context, int theme) {
        super(context, theme);
    }

    protected ReasonDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().getAttributes().windowAnimations = R.style.OSFilterDialogTheme;
        setContentView(R.layout.popup_orders);
        initView();
        orderId.setText("Ord Id : "+ordId);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edt_reason.getText().toString().trim().equals("")&&edt_reason.getText().toString().trim().length()==0)
                {
                    listener.showSnackMessage("Please Enter Reason");
                }
                else
                {
                    dismiss();
                    listener.onOkButtonClick(edt_reason.getText().toString().trim(),ACTIONTYPE);
                }


            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void initView() {
        orderId=findViewById(R.id.orderId);
        edt_reason=findViewById(R.id.edt_reason);
        buttonOk=findViewById(R.id.buttonOk);
        buttonCancel=findViewById(R.id.buttonCancel);
    }

    public void registerClickListener(IReasonDialogListener clicklistener)
    {
        this.listener=clicklistener;
    }
}
