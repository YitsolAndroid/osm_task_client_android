package com.wdget.yitsol.osm_app.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.OrderTaskModel
import com.wdget.yitsol.osm_app.presenter.ITaskSpnrItemListener
import com.wdget.yitsol.osm_app.presenter.IWorkListItemClickListener
import kotlinx.android.synthetic.main.txt_change_status.view.*

class OrderStateStatusAdapter(context: Context,orderTaskList: MutableList<OrderTaskModel>): RecyclerView.Adapter<OrderStateStatusAdapter.ViewHolder>()
{
    private var context: Context? = null
    private var orderTaskList: MutableList<OrderTaskModel>?=null
    private var listener: ITaskSpnrItemListener?=null

    init {
        this.context=context
        this.orderTaskList=orderTaskList
    }

    fun registerItemClickListener(clickListener: ITaskSpnrItemListener)
    {
        this.listener=clickListener;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val tasklistview = LayoutInflater.from(context).inflate(R.layout.txt_change_status, parent, false)
        return ViewHolder(tasklistview)
    }

    override fun getItemCount(): Int {
        return orderTaskList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(this.orderTaskList?.get(position), context, position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(taskmodel: OrderTaskModel?, context: Context?, position: Int) {
            itemView.rb_row.text=taskmodel!!.order_taskName
            if(taskmodel.isTask_status)
            {
                itemView.rb_row.isChecked=taskmodel.isTask_status
            }
            else{
                itemView.rb_row.isChecked=taskmodel.isTask_status
            }

            itemView.rb_row.setOnClickListener({
                changeSelectionStatus()
                if(taskmodel.isTask_status)
                {
                    taskmodel.isTask_status=false

                }
                else
                {
                    taskmodel.isTask_status=true
                    if(listener!=null)
                    {
                        listener?.getClickTaskName(taskmodel.order_taskName)
                    }
                }
                notifyItemChanged(position)
            })
        }

    }

    private fun changeSelectionStatus() {
        for(taskmodel:OrderTaskModel in orderTaskList!!)
        {
            taskmodel.isTask_status=false
        }
    }

    fun refreshTaskListAdapter(orderTaskList: MutableList<OrderTaskModel>)
    {
        this.orderTaskList=orderTaskList
        notifyDataSetChanged()
    }
}