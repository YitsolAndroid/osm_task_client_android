package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderStatusModel
{
    @SerializedName("ListStatesNStatuses.Response")
    @Expose
    private ListStatesNStatusesResponse listStatesNStatusesResponse;

    public ListStatesNStatusesResponse getListStatesNStatusesResponse() {
        return listStatesNStatusesResponse;
    }

    public void setListStatesNStatusesResponse(ListStatesNStatusesResponse listStatesNStatusesResponse) {
        this.listStatesNStatusesResponse = listStatesNStatusesResponse;
    }
}
