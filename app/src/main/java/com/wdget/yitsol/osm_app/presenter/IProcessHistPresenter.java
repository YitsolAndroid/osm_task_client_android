package com.wdget.yitsol.osm_app.presenter;

import android.content.Context;

import com.wdget.yitsol.osm_app.presenter.base.IBasePresenter;

public interface IProcessHistPresenter extends IBasePresenter
{
    void getOrderProcessHistory(Context context,String orderId);
}
