package com.wdget.yitsol.osm_app.service;

public class APIUtils
{
    private APIUtils() {}
    //private static final String BASE_URL="http://192.168.0.67:8090/OSMTaskClientRestWS/xml_api/";//TEST URL
  // private static final String BASE_URL="http://192.168.0.170:8082/OSMTaskClientRestWS/";//TEST URL

    //private static final String BASE_URL="http://192.168.0.102:8080/OSMTaskClientRestWS/";//LIVE URL
    private static final String BASE_URL="http://192.168.0.67:8090/OSMTaskClientRestWS/";//TEST URL

    public static APIService getAPIService() {
        return RetrofitClient.
                getClient(BASE_URL).
                create(APIService.class);
    }
}
