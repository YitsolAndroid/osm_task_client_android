package com.wdget.yitsol.osm_app.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ViewGroup
import android.widget.FrameLayout
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.OrdersReportResponse
import com.wdget.yitsol.osm_app.presenter.IReportingPresenter
import com.wdget.yitsol.osm_app.presenter.impl.QueryPresenter
import com.wdget.yitsol.osm_app.presenter.impl.ReportingPresenter
import com.wdget.yitsol.osm_app.repository.IReportingRepository
import com.wdget.yitsol.osm_app.repository.impl.ReportingRepository
import com.wdget.yitsol.osm_app.utils.AppConstant
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity
import kotlinx.android.synthetic.main.query.*
import kotlinx.android.synthetic.main.reporting.*
import java.util.ArrayList

class ReportingActivity : BaseNavigationActivity() {

    private var container: FrameLayout ?= null


    override fun getActContext(): Context {
        return this@ReportingActivity
    }

    override fun initScreen() {

        container = getContainerLayout()

        container?.addView(inflater?.inflate(R.layout.reporting,null),
                ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ll_orders.setOnClickListener{
            var intent = Intent(this,OrdersReportingActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            startActivity(intent)
        }

        ll_statistics.setOnClickListener{
            var intent = Intent(this,StatisticsDummyActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            startActivity(intent)
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        gotoDashboard()
    }

    private fun gotoDashboard() {
        val dashIntent=Intent(getActContext(),DashBoardAct::class.java)
        startActivity(dashIntent)
    }

}