package com.wdget.yitsol.osm_app.presenter.impl;

import com.wdget.yitsol.osm_app.presenter.ISplashPresenter;
import com.wdget.yitsol.osm_app.view.activity.SplashActivity;

public class SplashPresenter implements ISplashPresenter
{
    private SplashActivity view;


    public SplashPresenter(SplashActivity view) {
        this.view = view;
    }

    @Override
    public void checkInternetConnection() {

    }

    @Override
    public void subscribeCallbacks() {

    }

    @Override
    public void unSubscribeCallbacks() {

    }
}
