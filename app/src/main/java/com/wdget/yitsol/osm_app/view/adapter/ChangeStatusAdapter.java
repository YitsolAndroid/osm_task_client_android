/*
 * Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.wdget.yitsol.osm_app.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.wdget.yitsol.osm_app.R;

import java.util.ArrayList;
import java.util.List;

public class ChangeStatusAdapter extends BaseAdapter {

	private Context con;
	private Integer selected=-1;
	public static String checked="No Selection";
	protected String str="";
	protected Integer select;
	private String task;
	private List<String> lst;
	public static List<String> spnSelectLst=new ArrayList<String>();

	public ChangeStatusAdapter(Context con, List<String> headList, String task) {
		this.con = con;
		this.lst = headList;
		this.task=task;
	}
	
	@Override
	public int getCount() {
		return lst.size();

	}

	@Override
	public Object getItem(int position) {
		return lst.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint({"InflateParams", "SetTextI18n"})
	@Override
	public View getView(int position, View v, ViewGroup parent) {
		if (v == null) {
			LayoutInflater in = (LayoutInflater) con
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (in != null) {
				v = in.inflate(R.layout.txt_change_status, null);
			}
		}

		RadioButton rb = null;
		if (v != null) {
			rb = (RadioButton) v.findViewById(R.id.rb_row);
		}
		if(lst.get(position).equalsIgnoreCase(""))
		{
			rb.setText(""+task);
		}
		else
		{
		rb.setText(""+lst.get(position));
		}
		
		
		rb.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				selected = (Integer) v.getTag();
	            notifyDataSetChanged();	
	            
			}
		});
		rb.setTag(position);
		rb.setChecked(position == selected);

		if(rb.isChecked())
		{
			checked = rb.getText().toString();
			//System.out.println("Radio Button Checked is *******"+checked);
		}
		
		return v;
	}
	
	
}
