package com.wdget.yitsol.osm_app.presenter;

public interface IWorkListItemClickListener
{
    void OnClickEditor(String orderId, String taskId, String source, String orderType, String ref_no);
    void OnClickChangeStatus(String orderId, String taskId, String ref_no);
    void OnClickProcessHistory(String orderId);
    void OnSuspendOrder(String orderId,String taskId);
    void OnCancelOrder(String orderId,String taskId);
    void OnResumeOrder(String orderId,String taskId);
    void OnAbortOrder(String orderId,String taskId);
    void showSnackMessage(String message);
    void showWorkList();
    void hideWorkList();
}
