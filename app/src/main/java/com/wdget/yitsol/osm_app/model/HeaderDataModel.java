package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HeaderDataModel
{
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("mnemonic_path")
    @Expose
    private String mnemonicPath;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMnemonicPath() {
        return mnemonicPath;
    }

    public void setMnemonicPath(String mnemonicPath) {
        this.mnemonicPath = mnemonicPath;
    }

}
