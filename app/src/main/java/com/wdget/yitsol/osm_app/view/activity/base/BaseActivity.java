package com.wdget.yitsol.osm_app.view.activity.base;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wdget.yitsol.osm_app.R;
import com.wdget.yitsol.osm_app.presenter.IMessageDialogItemListner;
import com.wdget.yitsol.osm_app.utils.FontType;
import com.wdget.yitsol.osm_app.utils.ShowHideKeyboard;
import com.wdget.yitsol.osm_app.view.dialog.AppMessageDialog;

public abstract class BaseActivity extends AppCompatActivity implements ShowHideKeyboard
{
    private Context context;
    private Dialog dialog;
    private AnimationDrawable animationDrawable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context= BaseActivity.this;
    }

    protected abstract Context getActContext();

    public void setTextLabelFont(Context context,TextView view)
    {
        new FontType(context).settextViewFont(view,new FontType(context).typeface_lato_semibold);
    }

    public void showMessage(String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void showDialogMessage(String message)
    {
        AppMessageDialog appMessageDialog=new AppMessageDialog(context,message, R.style.AppMessageDialogTheme);
        appMessageDialog.setDialogItemClickListener(new IMessageDialogItemListner() {
            @Override
            public void OnOkButtonClick() {
                //onOkButtonClick();
            }
        });
        appMessageDialog.show();
    }

    @Override
    public void showKeyBoard(Context context) {
        ((InputMethodManager)
                (context).getSystemService(Context.INPUT_METHOD_SERVICE))
                .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    @Override
    public void hideKeyBoard(Context context) {
        View view = this.getCurrentFocus();
        if (view != null) {
            view.clearFocus();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Class is used to show the background process to the user.
     */
    class Runloader implements Runnable {
        private String strrMsg;

        public Runloader(String strrMsg) {
            this.strrMsg = strrMsg;
        }

        @SuppressWarnings("ResourceType")
        @Override
        public void run() {
            try
            {
                if (dialog == null) {
                    dialog = new Dialog(context, R.style.Theme_Dialog_Translucent_Dialog);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.getWindow().setBackgroundDrawable(
                            new ColorDrawable(
                                    Color.TRANSPARENT));
                }
                dialog.setContentView(R.layout.loading);
                dialog.setCancelable(false);
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
                if (dialog != null) {
                    dialog.show();
                }
                ImageView imgeView = null;
                if (dialog != null) {
                    imgeView = dialog.findViewById(R.id.imgeView);
                    TextView tvLoading = dialog.findViewById(R.id.tvLoading);
                    if (!strrMsg.equalsIgnoreCase(""))
                        tvLoading.setText(strrMsg);

                    imgeView.setBackgroundResource(R.anim.frame);

                    animationDrawable = (AnimationDrawable) imgeView.getBackground();
                    imgeView.post(new Runnable() {
                        @Override
                        public void run() {
                            if (animationDrawable != null)
                                animationDrawable.start();
                        }
                    });
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public void hideloader() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void showLoaderNew() {
        runOnUiThread(new Runloader(getResources().getString(R.string.loading)));
    }

    public void showLoaderNew(String msg) {
        runOnUiThread(new Runloader(msg));
    }

    public void hideSoftKeyboard(EditText editText) {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    public void showCustomSnackBar(String message, LinearLayout layout)
    {
        Snackbar mSnackBar = Snackbar.make(layout,message,Snackbar.LENGTH_LONG);
        View view = mSnackBar.getView();
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
        params.gravity = Gravity.BOTTOM;
        view.setLayoutParams(params);
        view.setBackgroundColor(Color.parseColor("#0C80FF"));
        TextView mainTextView = (view).findViewById(android.support.design.R.id.snackbar_text);
        mainTextView.setTextColor(Color.WHITE);
        mSnackBar.show();
    }
}
