package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessMnemonicModel
{
    @SerializedName("process_status_mnemonic")
    @Expose
    private String processStatusMnemonic;

    public String getProcessStatusMnemonic() {
        return processStatusMnemonic;
    }

    public void setProcessStatusMnemonic(String processStatusMnemonic) {
        this.processStatusMnemonic = processStatusMnemonic;
    }
}
