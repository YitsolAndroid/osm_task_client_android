package com.wdget.yitsol.osm_app.repository.impl;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.LoginInput;
import com.wdget.yitsol.osm_app.model.LoginOutput;
import com.wdget.yitsol.osm_app.repository.ILoginRepository;
import com.wdget.yitsol.osm_app.service.APIService;
import com.wdget.yitsol.osm_app.service.APIUtils;
import com.wdget.yitsol.osm_app.utils.AppPreference;
import com.wdget.yitsol.osm_app.utils.Encryption;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepository implements ILoginRepository {

    private APIService apiService;

    public LoginRepository() {
        apiService= APIUtils.getAPIService();
    }


    @Override
    public void performUserLogin(Context context, final String username, final String password, final OnUserLoginCallback callback) {
        try {
            final AppPreference appPreference=new AppPreference(context);
            Encryption encryption=new Encryption();
            String encodedInput=encryption.encrypt(username + ":"+ password);
            LoginInput input=new LoginInput(appPreference.getPublicIP(),appPreference.getPort());
            Map<String,String> inputmap=new HashMap<>();
            inputmap.put("Auth",encodedInput);
            inputmap.put("Accept","application/json");
            inputmap.put("Content-type", "application/json");
            inputmap.put("Accept-Encoding", "gzip");
            apiService.performUserLogin(inputmap,input).enqueue(new Callback<LoginOutput>() {
                @Override
                public void onResponse(Call<LoginOutput> call, Response<LoginOutput> response) {
                    if(callback!=null)
                    {
                        if(response.code()== HttpURLConnection.HTTP_OK)
                        {
                            LoginOutput output=response.body();
                            if (output != null) {
                                if(output.getStatus().equals("1"))
                                {
                                    callback.onError("Login Process Failed");
                                }
                                else
                                {
                                    appPreference.createUserLoginSession(username,password,output.getKey());
                                    callback.onSuccess(output);
                                }
                            }

                        }
                    }

                }

                @Override
                public void onFailure(Call<LoginOutput> call, Throwable t) {
                    if(callback!=null)
                    {
                        call.cancel();
                        callback.onError("app login error");
                    }
                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void performUserLogout(Context context,final OnUserLogoutCallback callback) {
        try {
            final AppPreference appPreference = new AppPreference(context);
            HashMap<String,String> logindetails=appPreference.getUserDetails();
            String username=logindetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password=logindetails.get(appPreference.KEY_USERLOGINPASSWORD);
            Encryption encryption=new Encryption();
            String encodedInput=encryption.encrypt(username + ":"+ password);
            LoginInput logOutInput = new LoginInput(appPreference.getPublicIP(), appPreference.getPort());

            Map<String, String> inputmap = new HashMap<>();
            inputmap.put("Auth",encodedInput);
            inputmap.put("Accept", "application/json");
            inputmap.put("Content-type", "application/json");
            inputmap.put("Accept-Encoding", "gzip");

            apiService.performUserLogout(inputmap,logOutInput).enqueue(new Callback<LoginOutput>() {
                @Override
                public void onResponse(Call<LoginOutput> call, Response<LoginOutput> response) {
                    if(callback!=null)
                    {
                        if(response.code()== HttpURLConnection.HTTP_OK)
                        {
                            LoginOutput output=response.body();
                            //appPreference.createUserLoginSession(username,password,output.getKey());
                            appPreference.logoutUser();
                            callback.onSuccess(output);
                        }
                    }

                }

                @Override
                public void onFailure(Call<LoginOutput> call, Throwable t) {
                    if(callback!=null)
                    {
                        call.cancel();
                        callback.onError("server error");
                    }
                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
