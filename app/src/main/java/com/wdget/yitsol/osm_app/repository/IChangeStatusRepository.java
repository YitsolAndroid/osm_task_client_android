package com.wdget.yitsol.osm_app.repository;

import android.content.Context;

import java.util.List;
import java.util.Map;

public interface IChangeStatusRepository
{
    interface OnGetAllOrderStatusCallback
    {
        void onSuccess(List<String> orderStatuslist, Map<String, List<String>> ordstateMap);
        void onError(String message);
    }

    interface OnChangeOrderStatusCallback
    {
        void onSuccess();
        void onError(String message);
    }
    void getAllStatusForOrder(Context context,String orderId, String taskId,OnGetAllOrderStatusCallback callback);

    void changeOrderStatus(Context context, String orderId, String taskId,String select_task,String ord_status,OnChangeOrderStatusCallback callback);
}
