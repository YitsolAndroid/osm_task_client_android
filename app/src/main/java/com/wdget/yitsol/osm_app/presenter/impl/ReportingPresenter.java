package com.wdget.yitsol.osm_app.presenter.impl;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.OrdersReportResponse;
import com.wdget.yitsol.osm_app.model.QueryOutputModel;
import com.wdget.yitsol.osm_app.presenter.IReportingPresenter;
import com.wdget.yitsol.osm_app.repository.IReportingRepository;
import com.wdget.yitsol.osm_app.repository.impl.ReportingRepository;
import com.wdget.yitsol.osm_app.view.activity.OrdersReportingActivity;
import com.wdget.yitsol.osm_app.view.activity.ReportingActivity;
import com.wdget.yitsol.osm_app.view.activity.StatisticsActivity;
import com.wdget.yitsol.osm_app.view.activity.StatisticsDummyActivity;

import java.util.List;

public class ReportingPresenter implements IReportingPresenter {

    private  OrdersReportingActivity view;
    private  StatisticsDummyActivity view2;
    private  StatisticsActivity view3;
    private IReportingRepository.OnOrdersReportingCallBack orderReportingCallBack;
    private IReportingRepository.OnSearchQueryByOrderStateCallback onSearchQueryByOrderStateCallback;
    private IReportingRepository repository;

    public ReportingPresenter(OrdersReportingActivity view)
    {
        this.view = view;
        repository = new ReportingRepository();
    }

    public ReportingPresenter(StatisticsDummyActivity view)
    {
        this.view2 = view;
        repository = new ReportingRepository();
    }

    public ReportingPresenter(StatisticsActivity view)
    {
        this.view3 = view;
        repository = new ReportingRepository();
    }
    @Override
    public void subscribeCallbacks() {

        orderReportingCallBack = new IReportingRepository.OnOrdersReportingCallBack() {
            @Override
            public void onSuccess(List<OrdersReportResponse> ordersReportResponse) {

                if(view!=null){
                    view.hideloader();
                    view.onOrderReportingSuccess(ordersReportResponse);
                }
                if(view2!=null){
                    view2.hideloader();
                    view2.onOrderReportingSuccess(ordersReportResponse);
                }

            }

            @Override
            public void onError(String message) {
                if(view!=null){
                    view.hideloader();
                }
                if(view2!=null){
                    view2.hideloader();
                }
            }
        };

        onSearchQueryByOrderStateCallback = new IReportingRepository.OnSearchQueryByOrderStateCallback() {
            @Override
            public void onSuccess(List<QueryOutputModel> datalist) {
                if(view!=null){
                    view.hideloader();
                    view.showQueryOrderList(datalist);
                }
            }

            @Override
            public void onError(String message) {
                if(view!=null){
                    view.hideloader();
                    view.showMessage(message);
                }
            }
        };
    }

    @Override
    public void unSubscribeCallbacks() {

        orderReportingCallBack = null;
        onSearchQueryByOrderStateCallback = null;
    }

    @Override
    public void getOrdersReport(Context context) {

        try {

            if(view!=null){
                view.showLoaderNew();
            }
            if(view2!=null){
                view2.showLoaderNew();
            }

            repository.getOrdersReport(context, orderReportingCallBack);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void searchQueryByOrderState(Context context, String orderState) {
        try {
            if(view!=null){
                view.showLoaderNew();
                repository.searchQueryByOrderState(context,orderState,onSearchQueryByOrderStateCallback);
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
