package com.wdget.yitsol.osm_app.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by Goutam on 9/9/2015.
 */
public class AppPreference {
    Context context;

    int PRIVATE_MODE = 0;

    private SharedPreferences preferences;
    private SharedPreferences.Editor edit;
    public String KEY_IS_LOGIN = "IsLoggedIn";
    public String KEY_USERLOGINUSERNAME = "loginusername";
    public String KEY_USERLOGINPASSWORD = "loginpassword";
    public String KEY_PUBLICIP="keypublicIP";
    public String KEY_PORT="keyport";
    public String KEY_TOKEN="keytoken";




    /**
     * Constructor (Here used singleton)
     *
     * @param context
     */
    public AppPreference(Context context) {

        this.context = context;
        preferences = context.getSharedPreferences(AppConstant.APP_PREF_NAME, PRIVATE_MODE);
        edit = preferences.edit();

    }

    public void savePublicIPAndPort(String publicIP,String port)
    {
        edit.putString(KEY_PUBLICIP,publicIP);
        edit.putString(KEY_PORT,port);
        edit.commit();
    }

    public String getPublicIP()
    {
        return preferences.getString(KEY_PUBLICIP,"NA");
    }

    public String getPort()
    {
        return preferences.getString(KEY_PORT,"NA");
    }

    public void createUserLoginSession(String username,String password,String token)
    {

        edit.putString(KEY_USERLOGINUSERNAME, username);
        edit.putString(KEY_USERLOGINPASSWORD, password);
        edit.putString(KEY_TOKEN, token);
        edit.putBoolean(KEY_IS_LOGIN, true);
        edit.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_USERLOGINUSERNAME, preferences.getString(KEY_USERLOGINUSERNAME, "NA"));
        user.put(KEY_USERLOGINPASSWORD, preferences.getString(KEY_USERLOGINPASSWORD, "NA"));
        user.put(KEY_TOKEN, preferences.getString(KEY_TOKEN, "NA"));

        // return user
        return user;
    }

    public void saveLoginCredential(String username, String password) {
        edit.putString(KEY_USERLOGINUSERNAME, username);
        edit.putString(KEY_USERLOGINPASSWORD, password);
        edit.commit();
    }

    public HashMap<String, String> getUserLoginCredential() {
        HashMap<String, String> details = new HashMap<>();
        details.put(KEY_USERLOGINUSERNAME, preferences.getString(KEY_USERLOGINUSERNAME, "NA"));
        details.put(KEY_USERLOGINPASSWORD, preferences.getString(KEY_USERLOGINPASSWORD, "NA"));
        return details;
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return preferences.getBoolean(KEY_IS_LOGIN, false);
    }


    /**
     * Logout User method
     */

    public boolean logoutUser() {
        edit.clear();
        edit.commit();
        return true;
    }

    public void saveactiveinternetstate(boolean state)
    {
       /* edit.putBoolean(KEY_ACTIVEINTERNET_STATE,state);
        edit.commit();*/
    }


    public void saveScanStartStatus(boolean status)
    {
        /*edit.putBoolean(SCAN_START_STATE,status);
        edit.commit();*/
    }

    public boolean getScanStartStatus()
    {
        /*return preferences.getBoolean(SCAN_START_STATE,false);*/
        return false;
    }

    public void saveresultOptionLoadStatus(boolean status)
    {
        /*edit.putBoolean(RESULT_OPTION_LOAD_STATE,status);
        edit.commit();*/
    }

    public boolean getResultOptionLoadStatus()
    {
        /*return preferences.getBoolean(RESULT_OPTION_LOAD_STATE,false);*/
        return false;
    }
}

