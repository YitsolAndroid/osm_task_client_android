package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Header_Model
{

    @SerializedName("_process_status")
    @Expose
    private ProcessStatus processStatus;
    @SerializedName("_grace_period_completion_date")
    @Expose
    private GracePeriodCompletionDate gracePeriodCompletionDate;
    @SerializedName("_current_order_state")
    @Expose
    private CurrentOrderState currentOrderState;
    @SerializedName("_target_order_state")
    @Expose
    private TargetOrderState targetOrderState;
    @SerializedName("_reference_number")
    @Expose
    private ReferenceNumber referenceNumber;
    @SerializedName("_date_pos_started")
    @Expose
    private DatePosStarted datePosStarted;
    @SerializedName("_order_state")
    @Expose
    private OrderState orderState;
    @SerializedName("_header")
    @Expose
    private HeaderDataModel header;
    @SerializedName("_execution_mode")
    @Expose
    private ExecutionMode executionMode;
    @SerializedName("_date_pos_created")
    @Expose
    private DatePosCreated datePosCreated;
    @SerializedName("_order_seq_id")
    @Expose
    private OrderSeqId orderSeqId;
    @SerializedName("_priority")
    @Expose
    private Priority priority;
    @SerializedName("_order_hist_seq_id")
    @Expose
    private OrderHistSeqId orderHistSeqId;
    @SerializedName("_namespace")
    @Expose
    private NamespaceModel namespace;
    @SerializedName("_task_id")
    @Expose
    private TaskId taskId;
    @SerializedName("_num_remarks")
    @Expose
    private NumRemarks numRemarks;
    @SerializedName("_user")
    @Expose
    private User user;
    @SerializedName("_compl_date_expected")
    @Expose
    private ComplDateExpected complDateExpected;
    @SerializedName("_version")
    @Expose
    private VersionModel version;
    @SerializedName("_order_type")
    @Expose
    private OrderType orderType;
    @SerializedName("_order_source")
    @Expose
    private OrderSource orderSource;

    public ProcessStatus getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(ProcessStatus processStatus) {
        this.processStatus = processStatus;
    }

    public GracePeriodCompletionDate getGracePeriodCompletionDate() {
        return gracePeriodCompletionDate;
    }

    public void setGracePeriodCompletionDate(GracePeriodCompletionDate gracePeriodCompletionDate) {
        this.gracePeriodCompletionDate = gracePeriodCompletionDate;
    }

    public CurrentOrderState getCurrentOrderState() {
        return currentOrderState;
    }

    public void setCurrentOrderState(CurrentOrderState currentOrderState) {
        this.currentOrderState = currentOrderState;
    }

    public TargetOrderState getTargetOrderState() {
        return targetOrderState;
    }

    public void setTargetOrderState(TargetOrderState targetOrderState) {
        this.targetOrderState = targetOrderState;
    }

    public ReferenceNumber getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(ReferenceNumber referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public DatePosStarted getDatePosStarted() {
        return datePosStarted;
    }

    public void setDatePosStarted(DatePosStarted datePosStarted) {
        this.datePosStarted = datePosStarted;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public HeaderDataModel getHeader() {
        return header;
    }

    public void setHeader(HeaderDataModel header) {
        this.header = header;
    }

    public ExecutionMode getExecutionMode() {
        return executionMode;
    }

    public void setExecutionMode(ExecutionMode executionMode) {
        this.executionMode = executionMode;
    }

    public DatePosCreated getDatePosCreated() {
        return datePosCreated;
    }

    public void setDatePosCreated(DatePosCreated datePosCreated) {
        this.datePosCreated = datePosCreated;
    }

    public OrderSeqId getOrderSeqId() {
        return orderSeqId;
    }

    public void setOrderSeqId(OrderSeqId orderSeqId) {
        this.orderSeqId = orderSeqId;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public OrderHistSeqId getOrderHistSeqId() {
        return orderHistSeqId;
    }

    public void setOrderHistSeqId(OrderHistSeqId orderHistSeqId) {
        this.orderHistSeqId = orderHistSeqId;
    }

    public NamespaceModel getNamespace() {
        return namespace;
    }

    public void setNamespace(NamespaceModel namespace) {
        this.namespace = namespace;
    }

    public TaskId getTaskId() {
        return taskId;
    }

    public void setTaskId(TaskId taskId) {
        this.taskId = taskId;
    }

    public NumRemarks getNumRemarks() {
        return numRemarks;
    }

    public void setNumRemarks(NumRemarks numRemarks) {
        this.numRemarks = numRemarks;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ComplDateExpected getComplDateExpected() {
        return complDateExpected;
    }

    public void setComplDateExpected(ComplDateExpected complDateExpected) {
        this.complDateExpected = complDateExpected;
    }

    public VersionModel getVersion() {
        return version;
    }

    public void setVersion(VersionModel version) {
        this.version = version;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public OrderSource getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(OrderSource orderSource) {
        this.orderSource = orderSource;
    }

    /*@SerializedName("_order_type")
    @Expose
    private OrderType orderType;
    @SerializedName("_date_pos_started")
    @Expose
    private DatePosStarted datePosStarted;
    @SerializedName("_order_source")
    @Expose
    private OrderSource orderSource;
    @SerializedName("_order_state")
    @Expose
    private OrderState orderState;
    @SerializedName("_num_remarks")
    @Expose
    private NumRemarks numRemarks;
    @SerializedName("_grace_period_completion_date")
    @Expose
    private GracePeriodCompletionDate gracePeriodCompletionDate;
    @SerializedName("_date_pos_created")
    @Expose
    private DatePosCreated datePosCreated;
    @SerializedName("_header")
    @Expose
    private HeaderDataModel header;
    @SerializedName("_execution_mode")
    @Expose
    private ExecutionMode executionMode;
    @SerializedName("_version")
    @Expose
    private VersionModel version;
    @SerializedName("_namespace")
    @Expose
    private NamespaceModel namespace;
    @SerializedName("_task_id")
    @Expose
    private TaskId taskId;
    @SerializedName("_reference_number")
    @Expose
    private ReferenceNumber referenceNumber;
    @SerializedName("_current_order_state")
    @Expose
    private CurrentOrderState currentOrderState;
    @SerializedName("_priority")
    @Expose
    private Priority priority;
    @SerializedName("_order_seq_id")
    @Expose
    private OrderSeqId orderSeqId;
    @SerializedName("_target_order_state")
    @Expose
    private TargetOrderState targetOrderState;
    @SerializedName("_process_status")
    @Expose
    private ProcessStatus processStatus;
    @SerializedName("_order_hist_seq_id")
    @Expose
    private OrderHistSeqId orderHistSeqId;
    @SerializedName("_user")
    @Expose
    private User user;
    @SerializedName("_compl_date_expected")
    @Expose
    private ComplDateExpected complDateExpected;

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public DatePosStarted getDatePosStarted() {
        return datePosStarted;
    }

    public void setDatePosStarted(DatePosStarted datePosStarted) {
        this.datePosStarted = datePosStarted;
    }

    public OrderSource getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(OrderSource orderSource) {
        this.orderSource = orderSource;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public NumRemarks getNumRemarks() {
        return numRemarks;
    }

    public void setNumRemarks(NumRemarks numRemarks) {
        this.numRemarks = numRemarks;
    }

    public GracePeriodCompletionDate getGracePeriodCompletionDate() {
        return gracePeriodCompletionDate;
    }

    public void setGracePeriodCompletionDate(GracePeriodCompletionDate gracePeriodCompletionDate) {
        this.gracePeriodCompletionDate = gracePeriodCompletionDate;
    }

    public DatePosCreated getDatePosCreated() {
        return datePosCreated;
    }

    public void setDatePosCreated(DatePosCreated datePosCreated) {
        this.datePosCreated = datePosCreated;
    }

    public HeaderDataModel getHeader() {
        return header;
    }

    public void setHeader(HeaderDataModel header) {
        this.header = header;
    }

    public ExecutionMode getExecutionMode() {
        return executionMode;
    }

    public void setExecutionMode(ExecutionMode executionMode) {
        this.executionMode = executionMode;
    }

    public VersionModel getVersion() {
        return version;
    }

    public void setVersion(VersionModel version) {
        this.version = version;
    }

    public NamespaceModel getNamespace() {
        return namespace;
    }

    public void setNamespace(NamespaceModel namespace) {
        this.namespace = namespace;
    }

    public TaskId getTaskId() {
        return taskId;
    }

    public void setTaskId(TaskId taskId) {
        this.taskId = taskId;
    }

    public ReferenceNumber getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(ReferenceNumber referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public CurrentOrderState getCurrentOrderState() {
        return currentOrderState;
    }

    public void setCurrentOrderState(CurrentOrderState currentOrderState) {
        this.currentOrderState = currentOrderState;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public OrderSeqId getOrderSeqId() {
        return orderSeqId;
    }

    public void setOrderSeqId(OrderSeqId orderSeqId) {
        this.orderSeqId = orderSeqId;
    }

    public TargetOrderState getTargetOrderState() {
        return targetOrderState;
    }

    public void setTargetOrderState(TargetOrderState targetOrderState) {
        this.targetOrderState = targetOrderState;
    }

    public ProcessStatus getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(ProcessStatus processStatus) {
        this.processStatus = processStatus;
    }

    public OrderHistSeqId getOrderHistSeqId() {
        return orderHistSeqId;
    }

    public void setOrderHistSeqId(OrderHistSeqId orderHistSeqId) {
        this.orderHistSeqId = orderHistSeqId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ComplDateExpected getComplDateExpected() {
        return complDateExpected;
    }

    public void setComplDateExpected(ComplDateExpected complDateExpected) {
        this.complDateExpected = complDateExpected;
    }*/

}
