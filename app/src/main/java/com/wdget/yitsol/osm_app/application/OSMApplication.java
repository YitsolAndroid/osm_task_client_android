package com.wdget.yitsol.osm_app.application;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.wdget.yitsol.osm_app.presenter.ConnectivityReceiverListener;
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver;

public class OSMApplication extends MultiDexApplication
{
    private static OSMApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance=this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    public static synchronized OSMApplication getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}
