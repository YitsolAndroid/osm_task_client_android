package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SummaryModel
{
    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("ExpectedDuration")
    @Expose
    private String expectedDuration;
    @SerializedName("ActualDuration")
    @Expose
    private long actualDuration;
    @SerializedName("ExpectedCompletionDate")
    @Expose
    private String expectedCompletionDate;
    @SerializedName("CompleteDate")
    @Expose
    private String completeDate;
    @SerializedName("ExpectedStartDate")
    @Expose
    private String expectedStartDate;
    @SerializedName("RequestedDeliveryDate")
    @Expose
    private String requestedDeliveryDate;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getExpectedDuration() {
        return expectedDuration;
    }

    public void setExpectedDuration(String expectedDuration) {
        this.expectedDuration = expectedDuration;
    }

    public long getActualDuration() {
        return actualDuration;
    }

    public void setActualDuration(long actualDuration) {
        this.actualDuration = actualDuration;
    }

    public String getExpectedCompletionDate() {
        return expectedCompletionDate;
    }

    public void setExpectedCompletionDate(String expectedCompletionDate) {
        this.expectedCompletionDate = expectedCompletionDate;
    }

    public String getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(String completeDate) {
        this.completeDate = completeDate;
    }

    public String getExpectedStartDate() {
        return expectedStartDate;
    }

    public void setExpectedStartDate(String expectedStartDate) {
        this.expectedStartDate = expectedStartDate;
    }

    public String getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(String requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }
}
