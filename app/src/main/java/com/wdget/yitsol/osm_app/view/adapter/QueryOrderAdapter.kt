package com.wdget.yitsol.osm_app.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.QueryOutputModel
import com.wdget.yitsol.osm_app.presenter.IWorkListItemClickListener
import com.wdget.yitsol.osm_app.utils.AppConstant
import kotlinx.android.synthetic.main.row_worklist_header.view.*
import kotlinx.android.synthetic.main.row_worklistfooteritem.view.*

@Suppress("UNCHECKED_CAST")
class QueryOrderAdapter(context: Context, queryorderlist:MutableList<QueryOutputModel>): RecyclerView.Adapter<QueryOrderAdapter.ViewHolder>(), Filterable
{
    override fun getFilter(): Filter {
        return object:Filter(){
            val filterResults = Filter.FilterResults()
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                if (charString.isEmpty()) {
                    filterResults.values = queryorderlist
                    filterResults.count = queryorderlist!!.size
                }
                else
                {
                    val filterlist: MutableList<QueryOutputModel> = ArrayList()
                    for (ordermodel: QueryOutputModel in queryorderlist!!) {
                        if (ordermodel.orderSeqId.toUpperCase().startsWith(charString.toUpperCase())) {
                            filterlist.add(ordermodel)
                        } else if (ordermodel.orderSeqId.toUpperCase().contains(charString.toUpperCase())) {
                            filterlist.add(ordermodel)
                        }
                    }

                    filterResults.values = filterlist
                    filterResults.count = filterlist.size
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filterlist = results?.values as MutableList<QueryOutputModel>?
                if(filterlist!!.count()==0)
                {
                    listener!!.hideWorkList()
                }
                else
                {
                    listener!!.showWorkList()
                }
                notifyDataSetChanged()
            }

        }
    }

    var context:Context?=null
    var queryorderlist:MutableList<QueryOutputModel>?=null
    private var filterlist: MutableList<QueryOutputModel>? = null
    private var slideUpAnim: Animation? = null
    private var slideDownAnim: Animation? = null
    private var listener: IWorkListItemClickListener?=null

    init {
        this.context=context
        this.queryorderlist=queryorderlist
        this.filterlist=queryorderlist
        slideDownAnim = AnimationUtils.loadAnimation(context, R.anim.slide_down)
        slideUpAnim = AnimationUtils.loadAnimation(context, R.anim.slide_up)
    }

    fun registerItemClickListener(clicklistener:IWorkListItemClickListener)
    {
        this.listener=clicklistener;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val worklistview = LayoutInflater.from(context).inflate(R.layout.row_worklist_header, parent, false)
        return ViewHolder(worklistview)
    }

    override fun getItemCount(): Int {
        return this.filterlist!!.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(this.filterlist?.get(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindItems(queryOrderModel: QueryOutputModel?) {
            itemView.ll_worklistoptionlayout.visibility=View.GONE
            itemView.ll_worklistoption.visibility=View.GONE
            /*if(queryOrderModel!!.optionStatus==0)
            {
                itemView.ll_worklistoptionlayout.visibility=View.VISIBLE
                itemView.ll_worklistoptionlayout.startAnimation(slideDownAnim)
                itemView.txt_option.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.up_arrow, 0)
            }
            else
            {
                itemView.ll_worklistoptionlayout.startAnimation(slideUpAnim)
                itemView.ll_worklistoptionlayout.visibility=View.GONE
                itemView.txt_option.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.drop_down, 0)
            }*/

            itemView.txt_orderId.text="Order Number : "+queryOrderModel!!.orderSeqId
            itemView.txt_orderNameSpace.text="Name Space :"+queryOrderModel.namespace
            itemView.txt_orderProcess.text="Process State : "+queryOrderModel.processStatus
            itemView.txt_orderType.text="Order Type : "+queryOrderModel.orderType
            itemView.txt_orderStatus.text="Status : "+nameCaptilised(queryOrderModel.orderState)
            changeOrderStateColor(nameCaptilised(getCurrentOrderState(queryOrderModel.currentOrderState)),itemView.txt_currOrdstate)

            itemView.txt_changestatus.setOnClickListener {
                listener?.OnClickChangeStatus(queryOrderModel.orderSeqId,queryOrderModel.taskId,queryOrderModel.referenceNumber)
            }

            itemView.txt_processhist.setOnClickListener{
                listener!!.OnClickProcessHistory(queryOrderModel.orderSeqId)
            }

            itemView.txt_suspendorder.setOnClickListener{
                val current_state=nameCaptilised(getCurrentOrderState(queryOrderModel.currentOrderState))
                if(current_state.equals(AppConstant.SUSPENDORDER))
                {
                    listener!!.showSnackMessage("Transaction Not Allowed.Order State "+nameCaptilised(getCurrentOrderState(queryOrderModel.currentOrderState)))
                }
                else
                {
                    listener!!.OnSuspendOrder(queryOrderModel.orderSeqId,queryOrderModel.taskId)
                }

            }

            itemView.txt_cancelOrder.setOnClickListener {
                val current_state=nameCaptilised(getCurrentOrderState(queryOrderModel.currentOrderState))
                if(current_state.equals(AppConstant.NOTSTARTED)||current_state.equals(AppConstant.CANCELLEDORD)) {
                    listener!!.showSnackMessage("Transaction Not Allowed.Order State "+nameCaptilised(getCurrentOrderState(queryOrderModel.currentOrderState)))
                } else {
                    listener!!.OnCancelOrder(queryOrderModel.orderSeqId,queryOrderModel.taskId)
                }

            }
            itemView.txt_resumeOrder.setOnClickListener {
                val current_state=nameCaptilised(getCurrentOrderState(queryOrderModel.currentOrderState))
                if(current_state.equals(AppConstant.NOTSTARTED)) {
                    listener!!.showSnackMessage("Transaction Not Allowed.Order State "+nameCaptilised(getCurrentOrderState(queryOrderModel.currentOrderState)))
                } else {
                    listener!!.OnResumeOrder(queryOrderModel.orderSeqId,queryOrderModel.taskId)
                }

            }

            itemView.txt_abortOrder.setOnClickListener {
                listener!!.OnAbortOrder(queryOrderModel.orderSeqId,queryOrderModel.taskId)
            }
        }

    }

    private fun getCurrentOrderState(current_order_state: String): String {
        val ord_state_arry = current_order_state.split(".")

        val ord_state: String = ord_state_arry.get(ord_state_arry.size - 1)

        return ord_state

    }

    fun changeOrderStateColor(state: String, txt_currOrdstate: TextView)
    {
        if(state.equals("Suspended"))
        {
            val spannableString= SpannableString("Current State : "+state)
            val foregroundStateColorSpan= ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.md_red_600))
            val foregroundLabelColorSpan= ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            spannableString.setSpan(foregroundStateColorSpan,16,spannableString.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannableString.setSpan(foregroundLabelColorSpan,0,15, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            //txt_currOrdstate.setTextColor(ContextCompat.getColor(context!!,R.color.md_red_600))
            txt_currOrdstate.text=spannableString
        }
        else if(state.equals("Cancelled"))
        {
            val spannableString= SpannableString("Current State : "+state)
            val foregroundStateColorSpan= ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.md_red_600))
            val foregroundLabelColorSpan= ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            spannableString.setSpan(foregroundStateColorSpan,16,spannableString.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannableString.setSpan(foregroundLabelColorSpan,0,15, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            //txt_currOrdstate.setTextColor(ContextCompat.getColor(context!!,R.color.md_red_600))
            txt_currOrdstate.text=spannableString
        }
        else if(state.equals("Not_started"))
        {
            val spannableString= SpannableString("Current State : "+state)
            val foregroundStateColorSpan= ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.md_yellow_800))
            val foregroundLabelColorSpan= ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            spannableString.setSpan(foregroundStateColorSpan,16,spannableString.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannableString.setSpan(foregroundLabelColorSpan,0,15, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            //txt_currOrdstate.setTextColor(ContextCompat.getColor(context!!,R.color.md_red_600))
            txt_currOrdstate.text=spannableString
        }
        else if(state.equals("In_progress"))
        {
            val spannableString= SpannableString("Current State : "+state)
            val foregroundStateColorSpan= ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.md_green_500))
            val foregroundLabelColorSpan= ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            spannableString.setSpan(foregroundStateColorSpan,16,spannableString.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannableString.setSpan(foregroundLabelColorSpan,0,15, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            //txt_currOrdstate.setTextColor(ContextCompat.getColor(context!!,R.color.md_red_600))
            txt_currOrdstate.text=spannableString
        }
        else
        {
            val spannableString= SpannableString("Current State : "+state)
            val foregroundStateColorSpan= ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            val foregroundLabelColorSpan= ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            spannableString.setSpan(foregroundStateColorSpan,16,spannableString.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannableString.setSpan(foregroundLabelColorSpan,0,15, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            //txt_currOrdstate.setTextColor(ContextCompat.getColor(context!!,R.color.md_red_600))
            txt_currOrdstate.text=spannableString
        }
    }

    private fun nameCaptilised(nametoCaps:String):String{
        val name=nametoCaps.substring(0,1).toUpperCase() + nametoCaps.substring(1).toLowerCase()
        return name
    }
}