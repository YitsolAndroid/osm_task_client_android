package com.wdget.yitsol.osm_app.utils;

import com.wdget.yitsol.osm_app.model.WorkListDataModel;

import java.util.Comparator;

public class OrderSortById implements Comparator<WorkListDataModel>
{

    @Override
    public int compare(WorkListDataModel orderId1, WorkListDataModel orderId2) {
        return Integer.parseInt(orderId1.getOrderId())- Integer.parseInt(orderId2.getOrderId());
    }
}
