package com.wdget.yitsol.osm_app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HeaderModel implements Parcelable
{
    @SerializedName("mnemonic_path")
    @Expose
    private String mnemonicPath;
    @SerializedName("content")
    @Expose
    private String content;

    protected HeaderModel(Parcel in) {
        mnemonicPath = in.readString();
        content = in.readString();
    }

    public static final Creator<HeaderModel> CREATOR = new Creator<HeaderModel>() {
        @Override
        public HeaderModel createFromParcel(Parcel in) {
            return new HeaderModel(in);
        }

        @Override
        public HeaderModel[] newArray(int size) {
            return new HeaderModel[size];
        }
    };

    public String getMnemonicPath() {
        return mnemonicPath;
    }

    public void setMnemonicPath(String mnemonicPath) {
        this.mnemonicPath = mnemonicPath;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mnemonicPath);
        dest.writeString(content);
    }
}
