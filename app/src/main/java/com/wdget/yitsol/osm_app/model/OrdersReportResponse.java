package com.wdget.yitsol.osm_app.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrdersReportResponse implements Parcelable{

    @SerializedName("total")
    @Expose
    private String total;

    @SerializedName("mnemonic")
    @Expose
    private String mnemonic;

    protected OrdersReportResponse(Parcel in) {
        total = in.readString();
        mnemonic = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(total);
        dest.writeString(mnemonic);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OrdersReportResponse> CREATOR = new Creator<OrdersReportResponse>() {
        @Override
        public OrdersReportResponse createFromParcel(Parcel in) {
            return new OrdersReportResponse(in);
        }

        @Override
        public OrdersReportResponse[] newArray(int size) {
            return new OrdersReportResponse[size];
        }
    };

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getMnemonic() {
        return mnemonic;
    }

    public void setMnemonic(String mnemonic) {
        this.mnemonic = mnemonic;
    }
}
