package com.wdget.yitsol.osm_app.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import com.jaredrummler.materialspinner.MaterialSpinner
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.*
import com.wdget.yitsol.osm_app.presenter.IQueryPresenter
import com.wdget.yitsol.osm_app.presenter.IWorkListPresenter
import com.wdget.yitsol.osm_app.presenter.impl.QueryPresenter
import com.wdget.yitsol.osm_app.utils.AppConstant
import com.wdget.yitsol.osm_app.utils.CommonPopUp
import com.wdget.yitsol.osm_app.utils.ConnectionDetector
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity
import kotlinx.android.synthetic.main.query.*
import kotlinx.android.synthetic.main.worklist.*

class QueryActivity : BaseNavigationActivity(),AdapterView.OnItemSelectedListener{


    private var container: FrameLayout?=null
    private var presenter: IQueryPresenter?=null
    private var orderState = ""
    private var nameSpace = ""
    private var processStatus = ""
    private var source = ""
    private var state = ""
    private var executionMode = ""
    private var task = ""
    private var type = ""
    private var user = ""

    private var orderId =""
    private var ref = ""
    private val createDateFrom = ""
    private val CreateDateTo = ""

    private var executionModeList: MutableList<String> = mutableListOf<String>()

    override fun getActContext(): Context {
      return this@QueryActivity
    }

    override fun initScreen() {
        container=getContainerLayout()
        container?.addView(inflater?.inflate(R.layout.query, null),
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        presenter = QueryPresenter(this);

        executionModeList.add("Select Execution Mode")
        executionModeList.add("Do (In Amending)")
        executionModeList.add("Do")
        executionModeList.add("Redo")
        executionModeList.add("Undo")

        btnSearch.setOnClickListener {
            orderId = edtId.text.toString()
            val connectStatus= ConnectivityReceiver.isConnected()
            if(connectStatus)
            {
                presenter?.searchQuery(getActContext(),orderId,orderState,nameSpace,state)
            }
            else
            {
                showCustomSnackBar(AppConstant.NO_CONNECTION,ll_query)
            }
        }

        btnReset.setOnClickListener({
            resetAllInputData()
            getAllQueryInputData()
        })
    }

    private fun resetAllInputData() {
        orderId=""
        orderState=""
        nameSpace=""
        state=""
    }


    override fun onStart() {
        super.onStart()
        presenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        presenter?.unSubscribeCallbacks()
    }

    override fun onResume() {
        super.onResume()
        getAllQueryInputData()

    }

    private fun getAllQueryInputData() {
        val connectStatus= ConnectivityReceiver.isConnected()
        if(connectStatus)
        {
            presenter?.getQueryDataCall(getActContext())
        }
        else
        {
            showCustomSnackBar(AppConstant.NO_CONNECTION,ll_query)
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val dashboardIntent= Intent(getActContext(),DashBoardAct::class.java)
        startActivity(dashboardIntent)
    }

    fun onQueryDataSuccess(queryDataModel: QueryDataModel){

        var orderStae: MutableList<String> = mutableListOf<String>()
        orderStae.add("Select Order Status")
        for (item : OrderStateResponse in queryDataModel.getOrderStatus()){
            orderStae.add(item.getOrderState())
        }

        var processes: MutableList<String> = mutableListOf<String>()
        processes.add("Select Process")
        for (item : ProcessStatusResponse in queryDataModel.getProcesses()){
            processes.add(item.getProcessIdDesc())
        }

        var nameSpaces: MutableList<String> = mutableListOf<String>()
        nameSpaces.add("Select NameSpace")
        for (item : CatridgeResponse in queryDataModel.getCatridges()){
            nameSpaces.add(item.getNamespace())
        }

        var sources: MutableList<String> = mutableListOf<String>()
        sources.add("Select Source")
        for (item : OrderSourceResponse in queryDataModel.getSource()){
            sources.add(item.getSource())
        }

        var tasks: MutableList<String> = mutableListOf<String>()
        tasks.add("Select Task")
        for (item : TaskResponse in queryDataModel.getTask()){
            tasks.add(item.getTask())
        }

        var types: MutableList<String> = mutableListOf<String>()
        types.add("Select Order Type")
        for (item : OrderTypeRespnse in queryDataModel.getType()){
            types.add(item.getOrderType())
        }

        var users: MutableList<String> = mutableListOf<String>()
        users.add("Select User")
        for (item : UserResponse in queryDataModel.getUsers()){
            users.add(item.getUsername())
        }

        var allOrderStates: MutableList<String> = mutableListOf<String>()
        allOrderStates.add("Select Order State")
        for (item : AllOrderStatesResponse in queryDataModel.getAllOrderStates()){
            allOrderStates.add(item.getStatedesc())
        }



        //spnOrderState.attachDataSource(orderStae)
        spnProcessStatus.attachDataSource(processes)
        //spnNamedpace.attachDataSource(nameSpaces)
        spnSource.attachDataSource(sources)
        spnTask.attachDataSource(tasks)
        spnType.attachDataSource(types)
        spnUser.attachDataSource(users)
        //spnState.attachDataSource(allOrderStates)
        spnExecutionMode.attachDataSource(executionModeList)

        setDatatoSpinner(orderStae,nameSpaces,allOrderStates)




    }

    private fun setDatatoSpinner(orderStae: MutableList<String>, nameSpaces: MutableList<String>, allOrderStates: MutableList<String>) {
        spnOrderState.setItems(orderStae)
        spnNamedpace.setItems(nameSpaces)
        spnState.setItems(allOrderStates)

        spnOrderState.selectedIndex=0
        spnNamedpace.selectedIndex=0
        spnState.selectedIndex=0

        spnOrderState.setOnItemSelectedListener(object :MaterialSpinner.OnItemSelectedListener<String>
        {
            override fun onItemSelected(view: MaterialSpinner?, position: Int, id: Long, item: String?) {
                if(!item.equals("Select Order Status"))
                {
                    if(item.equals("no_state")||item.equals("not_started")||item.equals("suspended")||
                            item.equals("cancelled")||item.equals("cancelling")||item.equals("waiting_for_revision")||
                            item.equals("aborted")||item.equals("failed"))
                    {
                        orderState=StringBuilder().append("open.not_running.").append(item).toString()
                    }
                    else
                    {
                        orderState=StringBuilder().append("open.running.").append(item).toString()
                    }
                }
                else
                {
                    orderState=""
                }
            }
        })

        spnOrderState.setOnNothingSelectedListener {
            orderState=""
        }

        spnNamedpace.setOnItemSelectedListener(object:MaterialSpinner.OnItemSelectedListener<String>{
            override fun onItemSelected(view: MaterialSpinner?, position: Int, id: Long, item: String?) {
                if(!item.equals("Select NameSpace"))
                {
                    nameSpace=item!!
                }
                else
                {
                    nameSpace=""
                }
            }
        })

        spnNamedpace.setOnNothingSelectedListener {
            nameSpace=""
        }

        spnState.setOnItemSelectedListener(object:MaterialSpinner.OnItemSelectedListener<String>{
            override fun onItemSelected(view: MaterialSpinner?, position: Int, id: Long, item: String?) {
                if(!item.equals("Select Order State"))
                {
                    state=item!!
                }
                else
                {
                    state=""
                }
            }

        })

        spnState.setOnNothingSelectedListener {
            state=""
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(adapterView: AdapterView<*>?, view: View?, position: Int, id: Long) {

        when(adapterView!!.id){
            R.id.spnOrderState -> orderState = adapterView.adapter.getItem(position).toString()
            R.id.spnExecutionMode -> executionMode = adapterView.adapter.getItem(position).toString()
            R.id.spnNamedpace -> nameSpace = adapterView.adapter.getItem(position).toString()
            R.id.spnProcessStatus -> processStatus = adapterView.adapter.getItem(position).toString()
            R.id.spnSource -> source = adapterView.adapter.getItem(position).toString()
            R.id.spnTask -> task = adapterView.adapter.getItem(position).toString()
            R.id.spnType -> type = adapterView.adapter.getItem(position).toString()
            R.id.spnUser -> user = adapterView.adapter.getItem(position).toString()
            R.id.spnState -> state = adapterView.adapter.getItem(position).toString()
        }
    }

    fun showSnackMessage(message:String)
    {
        showCustomSnackBar(message,ll_query)
    }

    fun showQueryOrderList(datalist: MutableList<QueryOutputModel>)
    {
        var querydatalist=ArrayList<QueryOutputModel>()
        for(outputmodel:QueryOutputModel in datalist)
        {
            querydatalist.add(outputmodel)
        }
        val queryorderIntent=Intent(getActContext(),QueryOrderListActivity::class.java)
        val bundle=Bundle()
        bundle.putString(AppConstant.FROM_CLASS,AppConstant.QUERYCLASS)
        bundle.putParcelableArrayList(AppConstant.QUERYORDERLIST,querydatalist)
        queryorderIntent.putExtras(bundle)
        queryorderIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(queryorderIntent)
    }
}