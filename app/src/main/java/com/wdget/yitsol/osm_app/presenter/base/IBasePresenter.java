package com.wdget.yitsol.osm_app.presenter.base;

/**
 * Created by user on 4/5/2017.
 */

public interface IBasePresenter {
    void subscribeCallbacks();
    void unSubscribeCallbacks();
}
