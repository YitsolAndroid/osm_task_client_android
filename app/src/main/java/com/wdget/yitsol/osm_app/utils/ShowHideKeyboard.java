package com.wdget.yitsol.osm_app.utils;

import android.content.Context;

public interface ShowHideKeyboard
{
    void showKeyBoard(Context context);
    void hideKeyBoard(Context context);
}
