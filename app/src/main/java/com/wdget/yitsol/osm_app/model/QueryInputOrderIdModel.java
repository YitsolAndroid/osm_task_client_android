package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QueryInputOrderIdModel {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("publicIP")
    @Expose
    private String publicIP;
    @SerializedName("port")
    @Expose
    private String port;
    @SerializedName("orderId")
    @Expose
    private String orderId;

    public QueryInputOrderIdModel(String username, String password, String publicIP, String port, String orderId) {
        this.username = username;
        this.password = password;
        this.publicIP = publicIP;
        this.port = port;
        this.orderId = orderId;
    }
}
