package com.wdget.yitsol.osm_app.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.FrameLayout
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.presenter.IActionEditorPresenter
import com.wdget.yitsol.osm_app.presenter.impl.ActionEditorPresenter
import com.wdget.yitsol.osm_app.utils.AppConstant
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity
import kotlinx.android.synthetic.main.editor_actionlayout.*

class EditorActionActivity : BaseNavigationActivity() {

    private var container: FrameLayout?=null
    private var presenter:IActionEditorPresenter?=null
    private var orderId:String?=null
    private var taskId:String?=null
    private var source:String?=null
    private var ord_type:String?=null
    private var ref_num:String?=null

    override fun getActContext(): Context {
        return this@EditorActionActivity
    }

    override fun initScreen() {
        container=getContainerLayout()
        container?.addView(inflater?.inflate(R.layout.editor_actionlayout, null),
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        getDataFromIntent()
        presenter=ActionEditorPresenter(this)
        setDataForView()
        btn_Cancel.setOnClickListener({
            goBack()
        })
    }

    private fun goBack() {
        val dashboardIntent= Intent(getActContext(),WorkListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(dashboardIntent)
    }

    @SuppressLint("SetTextI18n")
    private fun setDataForView() {
        txt_OrderId.text="Order Id : "+orderId
        edt_refNo.text="Ref Num : "+ref_num
        txt_OrderType.text="Ord Type : "+ord_type
        txt_OrderSource.text="Ord Source : "+source
        txt_task.text="Task : "+taskId
    }

    private fun getDataFromIntent() {
        orderId=intent.getStringExtra(AppConstant.ORDER_ID)
        taskId=intent.getStringExtra(AppConstant.TASK_ID)
        source=intent.getStringExtra(AppConstant.SOURCE)
        ord_type=intent.getStringExtra(AppConstant.ORD_TYPE)
        ref_num=intent.getStringExtra(AppConstant.REFERNCE)
    }

    override fun onStart() {
        super.onStart()
        presenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        presenter?.unSubscribeCallbacks()
    }

    override fun onResume() {
        super.onResume()
        val connectStatus= ConnectivityReceiver.isConnected()
        if(connectStatus)
        {
            presenter?.getProcessMnemonic(getActContext(),orderId,taskId)
        }
        else
        {
            showCustomSnackBar(AppConstant.NO_CONNECTION,ll_editor)
        }
    }

    fun getProcessMnemonicComplete(mNemoniclist: List<String>)
    {
        dpnr_Status.attachDataSource(mNemoniclist)
        dpnr_Status.setOnItemSelectedListener(object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

        })
        presenter?.getProcessPriority(getActContext())
    }

    fun showAllProcessPriority(prioritylist: List<String>)
    {
        dpnr_priority.attachDataSource(prioritylist)
        dpnr_priority.setOnItemSelectedListener(object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

            }

        })
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        goBack()
    }
}

