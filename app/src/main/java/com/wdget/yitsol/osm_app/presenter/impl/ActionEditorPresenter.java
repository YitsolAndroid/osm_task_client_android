package com.wdget.yitsol.osm_app.presenter.impl;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.ProcessMnemonicModel;
import com.wdget.yitsol.osm_app.presenter.IActionEditorPresenter;
import com.wdget.yitsol.osm_app.repository.IActionEditorRepository;
import com.wdget.yitsol.osm_app.repository.impl.ActionEditorRepository;
import com.wdget.yitsol.osm_app.view.activity.EditorActionActivity;

import java.util.List;

public class ActionEditorPresenter implements IActionEditorPresenter
{

    private EditorActionActivity view;
    private IActionEditorRepository repository;
    private IActionEditorRepository.OnGetMnemonicByOrderIdCallback mnemonicByOrderIdCallback;
    private IActionEditorRepository.OnGetProcessPriorityCallback priorityCallback;

    public ActionEditorPresenter(EditorActionActivity view) {
        this.view = view;
        repository= new ActionEditorRepository();
    }

    @Override
    public void getProcessMnemonic(Context context,String orderId, String taskId) {
        try {
            view.showLoaderNew();
            repository.getProcessMnemonic(context,orderId,taskId,mnemonicByOrderIdCallback);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void getProcessPriority(Context context) {
        try {
            view.showLoaderNew();
            repository.getProcessPriority(context,priorityCallback);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void subscribeCallbacks() {
        mnemonicByOrderIdCallback=new IActionEditorRepository.OnGetMnemonicByOrderIdCallback() {
            @Override
            public void onSuccess(List<ProcessMnemonicModel> datalist, List<String> mNemoniclist) {
                view.hideloader();
                view.getProcessMnemonicComplete(mNemoniclist);
            }

            @Override
            public void onError(String message) {
                view.hideloader();
                view.showMessage(message);
            }
        };

        priorityCallback=new IActionEditorRepository.OnGetProcessPriorityCallback() {
            @Override
            public void onSuccess(List<String> prioritylist) {
                view.hideloader();
                view.showAllProcessPriority(prioritylist);
            }

            @Override
            public void onError(String message) {
                view.hideloader();
                view.showMessage(message);
            }
        };
    }

    @Override
    public void unSubscribeCallbacks() {
        mnemonicByOrderIdCallback=null;
        priorityCallback=null;
    }
}
