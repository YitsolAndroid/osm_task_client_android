package com.wdget.yitsol.osm_app.repository;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.OrdProcessHistOutputModel;

public interface IProcessHistRepository
{
    interface OnProcessHistCallback
    {
        void onSuccess(OrdProcessHistOutputModel outputModel);
        void onError(String message);
    }

    void getOrderProcessHistory(Context context, String orderId,OnProcessHistCallback callback);
}
