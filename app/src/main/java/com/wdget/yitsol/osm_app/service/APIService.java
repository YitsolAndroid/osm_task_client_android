package com.wdget.yitsol.osm_app.service;

import com.wdget.yitsol.osm_app.model.AssignOrdInputModel;
import com.wdget.yitsol.osm_app.model.AssignOrdOutputModel;
import com.wdget.yitsol.osm_app.model.CompleteOrdInputModel;
import com.wdget.yitsol.osm_app.model.CompleteOrdOutputModel;
import com.wdget.yitsol.osm_app.model.GetOrderByIdModel;
import com.wdget.yitsol.osm_app.model.ChangeOrderInputModel;
import com.wdget.yitsol.osm_app.model.LoginInput;
import com.wdget.yitsol.osm_app.model.LoginOutput;
import com.wdget.yitsol.osm_app.model.OrdProcessHistInputModel;
import com.wdget.yitsol.osm_app.model.OrdProcessHistOutputModel;
import com.wdget.yitsol.osm_app.model.OrderStatusInputModel;
import com.wdget.yitsol.osm_app.model.OrderStatusModel;
import com.wdget.yitsol.osm_app.model.OrdersReportResponse;
import com.wdget.yitsol.osm_app.model.ProcessMnemonicModel;
import com.wdget.yitsol.osm_app.model.ProcessOrderModel;
import com.wdget.yitsol.osm_app.model.QueryDataModel;
import com.wdget.yitsol.osm_app.model.QueryInputModel;
import com.wdget.yitsol.osm_app.model.QueryInputOrderIdModel;
import com.wdget.yitsol.osm_app.model.QueryOutputModel;
import com.wdget.yitsol.osm_app.model.ReceivedOrdInputModel;
import com.wdget.yitsol.osm_app.model.ReceivedOrdOutputModel;
import com.wdget.yitsol.osm_app.model.ChangeOrdStateInputModel;
import com.wdget.yitsol.osm_app.model.ChangeOrdStateOutputModel;
import com.wdget.yitsol.osm_app.model.SearchQueryByOrderStateModel;
import com.wdget.yitsol.osm_app.model.WorkListInputModel;
import com.wdget.yitsol.osm_app.model.WorkListModel;
import com.wdget.yitsol.osm_app.model.WorkListResultModel;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {


    @POST("xml_api/login")
    Call<LoginOutput> performUserLogin(@HeaderMap Map<String, String> headers, @Body LoginInput input);

    @POST("xml_api/logout")
    Call<LoginOutput> performUserLogout(@HeaderMap Map<String, String> headers,@Body LoginInput input);

    @GET("orderServices/getProcessofOrders")
    Call<List<ProcessOrderModel>> getProcessOfOrderCall(@HeaderMap Map<String,String> headers);

    @POST("xml_api/getWorklistOrder")
    Call<WorkListResultModel> getAllWorkListData(@HeaderMap Map<String,String> headers, @Body WorkListInputModel inputModel);

    @GET("orderServices/services")
    Call<QueryDataModel> getQueryDataCall(@HeaderMap Map<String,String> headers);

    @GET("orderServices/getProcess")
    Call<List<ProcessMnemonicModel>> getProcessMnemonicForOrder(@HeaderMap Map<String,String> headers,
                                                                @Query("orderId") String orderId, @Query("task") String task);

    @POST("xml_api/listStatesNStatuses")
    Call<OrderStatusModel> getAllStatusForOrder(@HeaderMap Map<String,String> headers, @Body OrderStatusInputModel inputModel);

    @GET("orderServices/reportForOrderState")
    Call<List<OrdersReportResponse>> getOrdersReport(@HeaderMap Map<String,String> headers);

    @POST("xml_api/getOrder")
    Call<GetOrderByIdModel> changeOrderStatusById(@HeaderMap Map<String,String> headers, @Body ChangeOrderInputModel inputModel);

    @POST("xml_api/getCompleteOrder")
    Call<CompleteOrdOutputModel> completeOrdStatusChange(@HeaderMap Map<String,String> headers, @Body CompleteOrdInputModel inputModel);

    @POST("xml_api/getReceiveOrder")
    Call<ReceivedOrdOutputModel> receivedOrdStatusChange(@HeaderMap Map<String,String> headers, @Body ReceivedOrdInputModel inputModel);

    @POST("xml_api/getAssignOrder")
    Call<AssignOrdOutputModel> assignOrdStatusChange(@HeaderMap Map<String,String> headers, @Body AssignOrdInputModel inputModel);

    @POST("xml_api/getOrderProcessHistory")
    Call<OrdProcessHistOutputModel> getOrderProcessHistory(@HeaderMap Map<String,String> headers, @Body OrdProcessHistInputModel inputModel);

    @POST("orderServices/{actiontype}")
    Call<ChangeOrdStateOutputModel> changeOrderById(@Path("actiontype")String actiontype, @HeaderMap Map<String,String> headers, @Body ChangeOrdStateInputModel inputModel);

    @POST("xml_api/queryStrings")
    Call<List<QueryOutputModel>> performOrderQuery(@HeaderMap Map<String,String> headers, @Body QueryInputModel inputModel);

    @POST("xml_api/queryStrings")
    Call<List<QueryOutputModel>> searchQueryByOrderState(@HeaderMap Map<String,String> headers, @Body SearchQueryByOrderStateModel inputModel);

}
