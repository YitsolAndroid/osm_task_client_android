package com.wdget.yitsol.osm_app.presenter.impl;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.OrdProcessHistOutputModel;
import com.wdget.yitsol.osm_app.presenter.IProcessHistPresenter;
import com.wdget.yitsol.osm_app.repository.IProcessHistRepository;
import com.wdget.yitsol.osm_app.repository.impl.ProcessHistRepository;
import com.wdget.yitsol.osm_app.view.activity.ProcessHistActivity;

public class ProcessHistPresenter implements IProcessHistPresenter
{
    private ProcessHistActivity view;
    private IProcessHistRepository repository;
    private IProcessHistRepository.OnProcessHistCallback processHistCallback;

    public ProcessHistPresenter(ProcessHistActivity view) {
        this.view = view;
        repository=new ProcessHistRepository();
    }

    @Override
    public void getOrderProcessHistory(Context context, String orderId) {
        try {
            view.showLoaderNew();
            repository.getOrderProcessHistory(context,orderId,processHistCallback);

        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void subscribeCallbacks() {
        processHistCallback=new IProcessHistRepository.OnProcessHistCallback() {
            @Override
            public void onSuccess(OrdProcessHistOutputModel outputModel) {
                view.hideloader();
                view.showordProcessHistory(outputModel);
            }

            @Override
            public void onError(String message) {
                view.hideloader();
                view.showMessage(message);
            }
        };
    }

    @Override
    public void unSubscribeCallbacks() {
        processHistCallback=null;
    }
}
