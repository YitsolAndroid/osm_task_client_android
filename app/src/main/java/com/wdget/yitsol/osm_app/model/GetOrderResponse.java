package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

class GetOrderResponse {
    @SerializedName("AcceptedUserName")
    @Expose
    private List<String> acceptedUserName = null;
    @SerializedName("ExpectedDuration")
    @Expose
    private String expectedDuration;
    @SerializedName("Task")
    @Expose
    private String task;
    @SerializedName("Workgroups")
    @Expose
    private Workgroups workgroups;
    @SerializedName("ProcessStatus")
    @Expose
    private String processStatus;
    @SerializedName("Reference")
    @Expose
    private long reference;
    @SerializedName("Priority")
    @Expose
    private long priority;
    @SerializedName("ExecutionMode")
    @Expose
    private String executionMode;
    @SerializedName("OrderID")
    @Expose
    private long orderID;
    @SerializedName("ExpectedOrderCompletionDate")
    @Expose
    private String expectedOrderCompletionDate;
    @SerializedName("Namespace")
    @Expose
    private String namespace;
    @SerializedName("OrderHistID")
    @Expose
    private long orderHistID;
    @SerializedName("_root")
    @Expose
    private RootModel root;
    @SerializedName("xmlns")
    @Expose
    private String xmlns;
    @SerializedName("OrderType")
    @Expose
    private String orderType;
    @SerializedName("Version")
    @Expose
    private String version;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("OrderSource")
    @Expose
    private String orderSource;
    @SerializedName("OrderState")
    @Expose
    private String orderState;

    public List<String> getAcceptedUserName() {
        return acceptedUserName;
    }

    public void setAcceptedUserName(List<String> acceptedUserName) {
        this.acceptedUserName = acceptedUserName;
    }

    public String getExpectedDuration() {
        return expectedDuration;
    }

    public void setExpectedDuration(String expectedDuration) {
        this.expectedDuration = expectedDuration;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Workgroups getWorkgroups() {
        return workgroups;
    }

    public void setWorkgroups(Workgroups workgroups) {
        this.workgroups = workgroups;
    }

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public long getReference() {
        return reference;
    }

    public void setReference(long reference) {
        this.reference = reference;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public String getExecutionMode() {
        return executionMode;
    }

    public void setExecutionMode(String executionMode) {
        this.executionMode = executionMode;
    }

    public long getOrderID() {
        return orderID;
    }

    public void setOrderID(long orderID) {
        this.orderID = orderID;
    }

    public String getExpectedOrderCompletionDate() {
        return expectedOrderCompletionDate;
    }

    public void setExpectedOrderCompletionDate(String expectedOrderCompletionDate) {
        this.expectedOrderCompletionDate = expectedOrderCompletionDate;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public long getOrderHistID() {
        return orderHistID;
    }

    public void setOrderHistID(long orderHistID) {
        this.orderHistID = orderHistID;
    }

    public RootModel getRoot() {
        return root;
    }

    public void setRoot(RootModel root) {
        this.root = root;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

}
