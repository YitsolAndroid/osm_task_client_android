package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Warnings
{
    @SerializedName("Warning")
    @Expose
    private WarningModel warning;

    public WarningModel getWarning() {
        return warning;
    }

    public void setWarning(WarningModel warning) {
        this.warning = warning;
    }
}
