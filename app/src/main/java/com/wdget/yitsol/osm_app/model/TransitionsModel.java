package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TransitionsModel
{
    @SerializedName("Transition")
    @Expose
    private List<TransitionData> transition = null;

    public List<TransitionData> getTransition() {
        return transition;
    }

    public void setTransition(List<TransitionData> transition) {
        this.transition = transition;
    }

}
