package com.wdget.yitsol.osm_app.presenter.impl;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.QueryDataModel;
import com.wdget.yitsol.osm_app.model.QueryOutputModel;
import com.wdget.yitsol.osm_app.presenter.IQueryPresenter;
import com.wdget.yitsol.osm_app.repository.IQueryRepository;
import com.wdget.yitsol.osm_app.repository.impl.QueryRepository;
import com.wdget.yitsol.osm_app.view.activity.QueryActivity;

import java.util.List;

public class QueryPresenter implements IQueryPresenter{

    private QueryActivity view;
    private IQueryRepository repository;
    private IQueryRepository.OnQueryDataCallback queryDataCallback;
    private IQueryRepository.OnOrderQueryCallback orderQueryCallback;

    public QueryPresenter(QueryActivity view){
        this.view = view;
        repository = new QueryRepository();
    }

    @Override
    public void getQueryDataCall(Context context) {
        try {
            view.showLoaderNew();
            repository.getQueryDataCall(context,queryDataCallback);
        }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    @Override
    public void searchQuery(Context context, String orderId,String orderState,String nameSpace,String state) {
        try {
            view.showLoaderNew();
            repository.searchQuery(context,orderId,orderState,nameSpace,state,orderQueryCallback);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void subscribeCallbacks() {
        queryDataCallback = new IQueryRepository.OnQueryDataCallback() {
            @Override
            public void onSuccess(QueryDataModel queryDataModel) {
                view.hideloader();
                view.onQueryDataSuccess(queryDataModel);
            }
            @Override
            public void onError(String message) {
                view.hideloader();
                view.showMessage(message);

            }
        };

        orderQueryCallback=new IQueryRepository.OnOrderQueryCallback() {
            @Override
            public void onSuccess(List<QueryOutputModel> datalist) {
                view.hideloader();
                view.showQueryOrderList(datalist);
            }

            @Override
            public void onError(String message) {
                view.hideloader();
                view.showMessage(message);
            }
        };
    }

    @Override
    public void unSubscribeCallbacks() {
        queryDataCallback = null;
        orderQueryCallback=null;
    }
}
