package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WorklistResponse
{
    @SerializedName("xmlns")
    @Expose
    private String xmlns;
    @SerializedName("Header")
    @Expose
    private WorkListHeader header;
    @SerializedName("Orderdata")
    @Expose
    private List<WorkListOrderdata> orderdata = null;
    @SerializedName("Warnings")
    @Expose
    private Warnings warnings;

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public WorkListHeader getHeader() {
        return header;
    }

    public void setHeader(WorkListHeader header) {
        this.header = header;
    }

    public List<WorkListOrderdata> getOrderdata() {
        return orderdata;
    }

    public void setOrderdata(List<WorkListOrderdata> orderdata) {
        this.orderdata = orderdata;
    }

    public Warnings getWarnings() {
        return warnings;
    }

    public void setWarnings(Warnings warnings) {
        this.warnings = warnings;
    }
}
