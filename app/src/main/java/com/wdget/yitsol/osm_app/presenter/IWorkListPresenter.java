package com.wdget.yitsol.osm_app.presenter;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.ProcessOrderModel;
import com.wdget.yitsol.osm_app.presenter.base.IBasePresenter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IWorkListPresenter extends IBasePresenter
{
    void getProcessOfOrderCall(Context context);

    void getAllWorkListDataCall(Context context, List<ProcessOrderModel> processOrderdatalist);

    void changeOrderCurrentState(Context context,@Nullable String reason, @Nullable String actiontype,String orderId,String taskId);

    void changeOrderStatebyId(Context context,@NotNull String reason, @NotNull String actiontype, @NotNull String orderId, @NotNull String taskId);
}
