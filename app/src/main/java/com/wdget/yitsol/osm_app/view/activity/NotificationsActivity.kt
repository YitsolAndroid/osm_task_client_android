package com.wdget.yitsol.osm_app.view.activity

import android.content.Context
import android.content.Intent
import android.view.ViewGroup
import android.widget.FrameLayout
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity

class NotificationsActivity : BaseNavigationActivity() {

    private var container: FrameLayout? = null

    override fun getActContext(): Context {
        return this@NotificationsActivity
    }

    override fun initScreen() {

        container = getContainerLayout()
         container?.addView(inflater?.inflate(R.layout.notifications,null),
                 ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT)


    }

    override fun onBackPressed() {
        //super.onBackPressed()
        val dashboardIntent= Intent(getActContext(),DashBoardAct::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(dashboardIntent)
    }
}