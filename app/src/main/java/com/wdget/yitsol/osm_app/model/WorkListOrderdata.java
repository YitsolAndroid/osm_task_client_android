package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkListOrderdata
{
    @SerializedName("_process_status")
    @Expose
    private String processStatus;
    @SerializedName("_grace_period_completion_date")
    @Expose
    private String gracePeriodCompletionDate;
    @SerializedName("_current_order_state")
    @Expose
    private String currentOrderState;
    @SerializedName("_target_order_state")
    @Expose
    private String targetOrderState;
    @SerializedName("_reference_number")
    @Expose
    private String referenceNumber;
    @SerializedName("_date_pos_started")
    @Expose
    private String datePosStarted;
    @SerializedName("_order_state")
    @Expose
    private String orderState;
    @SerializedName("_header")
    @Expose
    private DataHeader header;
    @SerializedName("_execution_mode")
    @Expose
    private String executionMode;
    @SerializedName("_date_pos_created")
    @Expose
    private String datePosCreated;
    @SerializedName("_order_seq_id")
    @Expose
    private long orderSeqId;
    @SerializedName("_priority")
    @Expose
    private long priority;
    @SerializedName("_order_hist_seq_id")
    @Expose
    private long orderHistSeqId;
    @SerializedName("_namespace")
    @Expose
    private String namespace;
    @SerializedName("_task_id")
    @Expose
    private String taskId;
    @SerializedName("_num_remarks")
    @Expose
    private long numRemarks;
    @SerializedName("_user")
    @Expose
    private String user;
    @SerializedName("_compl_date_expected")
    @Expose
    private String complDateExpected;
    @SerializedName("_version")
    @Expose
    private String version;
    @SerializedName("_order_type")
    @Expose
    private String orderType;
    @SerializedName("_order_source")
    @Expose
    private String orderSource;

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public String getGracePeriodCompletionDate() {
        return gracePeriodCompletionDate;
    }

    public void setGracePeriodCompletionDate(String gracePeriodCompletionDate) {
        this.gracePeriodCompletionDate = gracePeriodCompletionDate;
    }

    public String getCurrentOrderState() {
        return currentOrderState;
    }

    public void setCurrentOrderState(String currentOrderState) {
        this.currentOrderState = currentOrderState;
    }

    public String getTargetOrderState() {
        return targetOrderState;
    }

    public void setTargetOrderState(String targetOrderState) {
        this.targetOrderState = targetOrderState;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getDatePosStarted() {
        return datePosStarted;
    }

    public void setDatePosStarted(String datePosStarted) {
        this.datePosStarted = datePosStarted;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public DataHeader getHeader() {
        return header;
    }

    public void setHeader(DataHeader header) {
        this.header = header;
    }

    public String getExecutionMode() {
        return executionMode;
    }

    public void setExecutionMode(String executionMode) {
        this.executionMode = executionMode;
    }

    public String getDatePosCreated() {
        return datePosCreated;
    }

    public void setDatePosCreated(String datePosCreated) {
        this.datePosCreated = datePosCreated;
    }

    public long getOrderSeqId() {
        return orderSeqId;
    }

    public void setOrderSeqId(long orderSeqId) {
        this.orderSeqId = orderSeqId;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public long getOrderHistSeqId() {
        return orderHistSeqId;
    }

    public void setOrderHistSeqId(long orderHistSeqId) {
        this.orderHistSeqId = orderHistSeqId;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public long getNumRemarks() {
        return numRemarks;
    }

    public void setNumRemarks(long numRemarks) {
        this.numRemarks = numRemarks;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getComplDateExpected() {
        return complDateExpected;
    }

    public void setComplDateExpected(String complDateExpected) {
        this.complDateExpected = complDateExpected;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderSource() {
        return orderSource;
    }

    public void setOrderSource(String orderSource) {
        this.orderSource = orderSource;
    }
}
