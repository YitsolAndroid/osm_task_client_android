package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListStatesNStatusesResponse
{
    @SerializedName("OrderHistID")
    @Expose
    private long orderHistID;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("OrderID")
    @Expose
    private long orderID;
    @SerializedName("xmlns")
    @Expose
    private String xmlns;
    @SerializedName("TaskStatesNStatuses")
    @Expose
    private TaskStatesNStatuses taskStatesNStatuses;

    public long getOrderHistID() {
        return orderHistID;
    }

    public void setOrderHistID(long orderHistID) {
        this.orderHistID = orderHistID;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getOrderID() {
        return orderID;
    }

    public void setOrderID(long orderID) {
        this.orderID = orderID;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public TaskStatesNStatuses getTaskStatesNStatuses() {
        return taskStatesNStatuses;
    }

    public void setTaskStatesNStatuses(TaskStatesNStatuses taskStatesNStatuses) {
        this.taskStatesNStatuses = taskStatesNStatuses;
    }
}
