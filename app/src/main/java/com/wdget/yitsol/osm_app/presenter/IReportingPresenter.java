package com.wdget.yitsol.osm_app.presenter;

import android.content.Context;

import com.wdget.yitsol.osm_app.presenter.base.IBasePresenter;

public interface IReportingPresenter extends IBasePresenter{

    void getOrdersReport(Context context);

    void searchQueryByOrderState(Context context,String orderState);

}
