package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompleteOrderResponse
{
    @SerializedName("OrderHistID")
    @Expose
    private String orderHistID;
    @SerializedName("xmlns")
    @Expose
    private String xmlns;
    @SerializedName("OrderID")
    @Expose
    private long orderID;

    public String getOrderHistID() {
        return orderHistID;
    }

    public void setOrderHistID(String orderHistID) {
        this.orderHistID = orderHistID;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public long getOrderID() {
        return orderID;
    }

    public void setOrderID(long orderID) {
        this.orderID = orderID;
    }

}
