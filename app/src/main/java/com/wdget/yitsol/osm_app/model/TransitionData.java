package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransitionData
{
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("TaskID")
    @Expose
    private String taskID;
    @SerializedName("ExpectedCompletionDate")
    @Expose
    private String expectedCompletionDate;
    @SerializedName("User")
    @Expose
    private String user;
    @SerializedName("DataNodeValue")
    @Expose
    private String dataNodeValue;
    @SerializedName("DataNodeIndex")
    @Expose
    private String dataNodeIndex;
    @SerializedName("ExecutionMode")
    @Expose
    private String executionMode;
    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("OrderHistID")
    @Expose
    private long orderHistID;
    @SerializedName("FromOrderHistID")
    @Expose
    private String fromOrderHistID;
    @SerializedName("ActualDuration")
    @Expose
    private long actualDuration;
    @SerializedName("TaskDescription")
    @Expose
    private String taskDescription;
    @SerializedName("CompleteDate")
    @Expose
    private String completeDate;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("TransitionType")
    @Expose
    private String transitionType;
    @SerializedName("TaskType")
    @Expose
    private String taskType;
    @SerializedName("DataNodeMnemonic")
    @Expose
    private String dataNodeMnemonic;
    @SerializedName("ParentTaskOrderHistID")
    @Expose
    private String parentTaskOrderHistID;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTaskID() {
        return taskID;
    }

    public void setTaskID(String taskID) {
        this.taskID = taskID;
    }

    public String getExpectedCompletionDate() {
        return expectedCompletionDate;
    }

    public void setExpectedCompletionDate(String expectedCompletionDate) {
        this.expectedCompletionDate = expectedCompletionDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDataNodeValue() {
        return dataNodeValue;
    }

    public void setDataNodeValue(String dataNodeValue) {
        this.dataNodeValue = dataNodeValue;
    }

    public String getDataNodeIndex() {
        return dataNodeIndex;
    }

    public void setDataNodeIndex(String dataNodeIndex) {
        this.dataNodeIndex = dataNodeIndex;
    }

    public String getExecutionMode() {
        return executionMode;
    }

    public void setExecutionMode(String executionMode) {
        this.executionMode = executionMode;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public long getOrderHistID() {
        return orderHistID;
    }

    public void setOrderHistID(long orderHistID) {
        this.orderHistID = orderHistID;
    }

    public String getFromOrderHistID() {
        return fromOrderHistID;
    }

    public void setFromOrderHistID(String fromOrderHistID) {
        this.fromOrderHistID = fromOrderHistID;
    }

    public long getActualDuration() {
        return actualDuration;
    }

    public void setActualDuration(long actualDuration) {
        this.actualDuration = actualDuration;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(String completeDate) {
        this.completeDate = completeDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTransitionType() {
        return transitionType;
    }

    public void setTransitionType(String transitionType) {
        this.transitionType = transitionType;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getDataNodeMnemonic() {
        return dataNodeMnemonic;
    }

    public void setDataNodeMnemonic(String dataNodeMnemonic) {
        this.dataNodeMnemonic = dataNodeMnemonic;
    }

    public String getParentTaskOrderHistID() {
        return parentTaskOrderHistID;
    }

    public void setParentTaskOrderHistID(String parentTaskOrderHistID) {
        this.parentTaskOrderHistID = parentTaskOrderHistID;
    }

}
