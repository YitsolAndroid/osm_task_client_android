package com.wdget.yitsol.osm_app.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.WorkListDataModel
import com.wdget.yitsol.osm_app.presenter.IWorkListItemClickListener
import com.wdget.yitsol.osm_app.utils.AppConstant
import kotlinx.android.synthetic.main.row_worklist_header.view.*
import kotlinx.android.synthetic.main.row_worklistfooteritem.view.*

class WorkListAdapter(context: Context, worklistdata: List<WorkListDataModel>) : RecyclerView.Adapter<WorkListAdapter.ViewHolder>(), Filterable {
    override fun getFilter(): Filter {
        return object : Filter() {
            val filterResults = Filter.FilterResults()
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                if (charString.isEmpty()) {
                    filterResults.values = worklistdata
                    filterResults.count = worklistdata!!.size

                } else {
                    val filterlist: MutableList<WorkListDataModel> = ArrayList()
                    for (ordermodel: WorkListDataModel in worklistdata!!) {
                        if (ordermodel.orderId.toUpperCase().startsWith(charString.toUpperCase())) {
                            filterlist.add(ordermodel)
                        } else if (ordermodel.orderId.toUpperCase().contains(charString.toUpperCase())) {
                            filterlist.add(ordermodel)
                        }
                    }

                    filterResults.values = filterlist
                    filterResults.count = filterlist.size
                }
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filterlist = results?.values as List<WorkListDataModel>?
                if(filterlist!!.count()==0)
                {
                    listener!!.hideWorkList()
                }
                else
                {
                    listener!!.showWorkList()
                }
                notifyDataSetChanged()
            }

        }
    }

    private var context: Context? = null
    private var worklistdata: List<WorkListDataModel>? = null
    private var filterlist: List<WorkListDataModel>? = null
    private var slideUpAnim: Animation? = null
    private var slideDownAnim: Animation? = null
    private var listener:IWorkListItemClickListener?=null

    init {
        this.context = context
        this.worklistdata = worklistdata
        this.filterlist = worklistdata
        slideDownAnim = AnimationUtils.loadAnimation(context, R.anim.slide_down)
        slideUpAnim = AnimationUtils.loadAnimation(context, R.anim.slide_up)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val worklistview = LayoutInflater.from(context).inflate(R.layout.row_worklist_header, parent, false)
        return ViewHolder(worklistview)
    }

    override fun getItemCount(): Int {

        return this.filterlist!!.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(this.filterlist?.get(position), position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindItems(dataModel: WorkListDataModel?, position: Int) {
            if (dataModel!!.isOptionStatus) {
                itemView.ll_worklistoptionlayout.visibility = View.VISIBLE
                itemView.ll_worklistoptionlayout.startAnimation(slideDownAnim)
                itemView.txt_option.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.up_arrow, 0)
            } else {
                itemView.ll_worklistoptionlayout.startAnimation(slideUpAnim)
                itemView.ll_worklistoptionlayout.visibility = View.GONE
                itemView.txt_option.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.drop_down, 0)
            }

            itemView.txt_orderId.text = "Order Number : " + dataModel.orderId
            itemView.txt_orderNameSpace.text = "Cartridge Name : " + dataModel.nameSpace
            itemView.txt_orderProcess.text = "Process State : " + dataModel.process
            itemView.txt_orderType.text = "Order Type : " + dataModel.orderType
            itemView.txt_orderStatus.text = "Order Status : " + nameCaptilised(dataModel.orderState)
            itemView.txt_option.setOnClickListener({
                if (dataModel.isOptionStatus) {
                    dataModel.isOptionStatus = false
                } else {
                    dataModel.isOptionStatus = true
                }
                notifyItemChanged(position)
            })
            itemView.txt_editor.setOnClickListener({
                listener!!.OnClickEditor(dataModel.orderId,dataModel.taskId
                ,dataModel.source,dataModel.orderType,dataModel.ref_no)
            })
            itemView.txt_changestatus.setOnClickListener({
                listener!!.OnClickChangeStatus(dataModel.orderId,dataModel.taskId,dataModel.ref_no)
            })

            itemView.txt_processhist.setOnClickListener({
                listener!!.OnClickProcessHistory(dataModel.orderId)
            })

            itemView.txt_suspendorder.setOnClickListener({
                val current_state=nameCaptilised(getCurrentOrderState(dataModel.current_order_state))
                if(current_state.equals(AppConstant.SUSPENDORDER))
                {
                    listener!!.showSnackMessage("Transaction Not Allowed.Order State "+nameCaptilised(getCurrentOrderState(dataModel.current_order_state)))
                }
                else
                {
                    listener!!.OnSuspendOrder(dataModel.orderId,dataModel.taskId)
                }

            })

            itemView.txt_cancelOrder.setOnClickListener({
                val current_state=nameCaptilised(getCurrentOrderState(dataModel.current_order_state))
                if(current_state.equals(AppConstant.NOTSTARTED)||current_state.equals(AppConstant.CANCELLEDORD))
                {
                    listener!!.showSnackMessage("Transaction Not Allowed.Order State "+nameCaptilised(getCurrentOrderState(dataModel.current_order_state)))
                }
                else
                {
                    listener!!.OnCancelOrder(dataModel.orderId,dataModel.taskId)
                }
            })

            itemView.txt_resumeOrder.setOnClickListener({
                val current_state=nameCaptilised(getCurrentOrderState(dataModel.current_order_state))
                if(current_state.equals(AppConstant.NOTSTARTED))
                {
                    listener!!.showSnackMessage("Transaction Not Allowed.Order State "+nameCaptilised(getCurrentOrderState(dataModel.current_order_state)))
                }
                else
                {
                    listener!!.OnResumeOrder(dataModel.orderId,dataModel.taskId)
                }

            })

            itemView.txt_abortOrder.setOnClickListener({
                listener!!.OnAbortOrder(dataModel.orderId,dataModel.taskId)

            })
            changeOrderStateColor(nameCaptilised(getCurrentOrderState(dataModel.current_order_state)),itemView.txt_currOrdstate)
        }

    }

    fun changeOrderStateColor(state: String, txt_currOrdstate: TextView)
    {
        if(state.equals("Suspended"))
        {
            val spannableString=SpannableString("Order Current State : "+state)
            val foregroundStateColorSpan=ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.md_red_600))
            val foregroundLabelColorSpan=ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            spannableString.setSpan(foregroundStateColorSpan,21,spannableString.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannableString.setSpan(foregroundLabelColorSpan,0,20, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            //txt_currOrdstate.setTextColor(ContextCompat.getColor(context!!,R.color.md_red_600))
            txt_currOrdstate.text=spannableString
        }
        else if(state.equals("Cancelled"))
        {
            val spannableString=SpannableString("Order Current State : "+state)
            val foregroundStateColorSpan=ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.md_red_600))
            val foregroundLabelColorSpan=ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            spannableString.setSpan(foregroundStateColorSpan,21,spannableString.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannableString.setSpan(foregroundLabelColorSpan,0,20, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            //txt_currOrdstate.setTextColor(ContextCompat.getColor(context!!,R.color.md_red_600))
            txt_currOrdstate.text=spannableString
        }
        else if(state.equals("Not_started"))
        {
            val spannableString=SpannableString("Order Current State : "+state)
            val foregroundStateColorSpan=ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.md_yellow_800))
            val foregroundLabelColorSpan=ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            spannableString.setSpan(foregroundStateColorSpan,21,spannableString.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannableString.setSpan(foregroundLabelColorSpan,0,20, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            //txt_currOrdstate.setTextColor(ContextCompat.getColor(context!!,R.color.md_red_600))
            txt_currOrdstate.text=spannableString
        }
        else if(state.equals("In_progress"))
        {
            val spannableString=SpannableString("Order Current State : "+state)
            val foregroundStateColorSpan=ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.md_green_500))
            val foregroundLabelColorSpan=ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            spannableString.setSpan(foregroundStateColorSpan,21,spannableString.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannableString.setSpan(foregroundLabelColorSpan,0,21, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            //txt_currOrdstate.setTextColor(ContextCompat.getColor(context!!,R.color.md_red_600))
            txt_currOrdstate.text=spannableString
        }
        else
        {
            val spannableString=SpannableString("Order Current State : "+state)
            val foregroundStateColorSpan=ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            val foregroundLabelColorSpan=ForegroundColorSpan(ContextCompat.getColor(context!!,R.color.black_color))
            spannableString.setSpan(foregroundStateColorSpan,21,spannableString.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannableString.setSpan(foregroundLabelColorSpan,0,20, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            //txt_currOrdstate.setTextColor(ContextCompat.getColor(context!!,R.color.md_red_600))
            txt_currOrdstate.text=spannableString
        }
    }

    fun registerItemClickListener(clicklistener:IWorkListItemClickListener)
    {
        this.listener=clicklistener;
    }

    private fun getCurrentOrderState(current_order_state: String): String {
        val ord_state_arry = current_order_state.split(".")

        val ord_state: String = ord_state_arry.get(ord_state_arry.size - 1)

        return ord_state

    }

    private fun nameCaptilised(nametoCaps:String):String{
        val name=nametoCaps.substring(0,1).toUpperCase() + nametoCaps.substring(1).toLowerCase()
        return name
    }

}