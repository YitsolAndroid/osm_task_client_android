package com.wdget.yitsol.osm_app.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class FontType{
	
public Typeface typeFace_AmTypeWriter,typeface_lato_semibold;

	public FontType(Context con) {
		typeFace_AmTypeWriter=Typeface.createFromAsset(con.getAssets(),"fonts/Lato-Italic.ttf");
		typeface_lato_semibold=Typeface.createFromAsset(con.getAssets(),"fonts/Lato-Semibold.ttf");
	}

	public FontType(Context con, ViewGroup root)
	{
		typeFace_AmTypeWriter=Typeface.createFromAsset(con.getAssets(),"fonts/Lato-Italic.ttf");
		setFont(root, typeFace_AmTypeWriter);
	}

	public void settextViewFont(TextView textView, Typeface font) {

		textView.setTypeface(font);
	}
	
	public void setFont(ViewGroup group, Typeface font) {
		int count = group.getChildCount();
		View v;
		for (int i = 0; i < count; i++) {
			v = group.getChildAt(i);
			if (v instanceof TextView || v instanceof Button) {
				((TextView) v).setTypeface(font);
			} 
			else if (v instanceof ViewGroup)
				setFont((ViewGroup) v, font);
		}
	}
	
}
