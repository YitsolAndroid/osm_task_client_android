package com.wdget.yitsol.osm_app.repository;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.LoginInput;
import com.wdget.yitsol.osm_app.model.LoginOutput;

public interface ILoginRepository
{
    interface OnUserLoginCallback
    {
        void onSuccess(LoginOutput output);
        void onError(String message);
    }

    interface OnUserLogoutCallback
    {
        void onSuccess(LoginOutput output);
        void onError(String message);
    }

    void performUserLogin(Context context, String username, String password, OnUserLoginCallback callback);

    void performUserLogout(Context context,final OnUserLogoutCallback callback);
}
