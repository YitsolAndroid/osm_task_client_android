/*
* Copyright (C) 2015, 2016, Blue Matrix and/or its affiliates. All rights reserved. 
 */
package com.wdget.yitsol.osm_app.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;


public class CommonPopUp extends Dialog {
	private TextView tv_Msg;
	
	public CommonPopUp(Context context,String msg) {
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(com.wdget.yitsol.osm_app.R.layout.common_popup);

		
		tv_Msg = (TextView) findViewById(com.wdget.yitsol.osm_app.R.id.tv_Msg);
		tv_Msg.setText(""+msg);
	}
	
	
}
