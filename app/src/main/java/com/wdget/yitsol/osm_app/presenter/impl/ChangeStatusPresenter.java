package com.wdget.yitsol.osm_app.presenter.impl;

import android.content.Context;

import com.wdget.yitsol.osm_app.presenter.IChangeStatusPresenter;
import com.wdget.yitsol.osm_app.repository.IChangeStatusRepository;
import com.wdget.yitsol.osm_app.repository.impl.ChangeStatusRepository;
import com.wdget.yitsol.osm_app.view.activity.ChangeStatusActivity;

import java.util.List;
import java.util.Map;

public class ChangeStatusPresenter implements IChangeStatusPresenter
{
    private ChangeStatusActivity view;
    private IChangeStatusRepository repository;
    private IChangeStatusRepository.OnGetAllOrderStatusCallback orderStatusCallback;
    private IChangeStatusRepository.OnChangeOrderStatusCallback changeOrderStatusCallback;


    public ChangeStatusPresenter(ChangeStatusActivity view) {
        this.view = view;
        repository=new ChangeStatusRepository();
    }

    @Override
    public void getAllStatusForOrder(Context context,String orderId, String taskId) {
        try {
            view.showLoaderNew();
            repository.getAllStatusForOrder(context,orderId,taskId,orderStatusCallback);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void changeOrderStatus(Context context, String orderId, String taskId,String select_task,String ord_status) {
        try {
            if(validateTaskOptionSelection(select_task))
            {
                view.showLoaderNew();
                repository.changeOrderStatus(context,orderId,taskId,select_task,ord_status,changeOrderStatusCallback);
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private boolean validateTaskOptionSelection(String select_task) {
        if(select_task==null)
        {
            view.showSnackMessage("Please Choose Task Option");
            return false;
        }
        else
        {
            return true;
        }
    }

    @Override
    public void subscribeCallbacks() {
        orderStatusCallback=new IChangeStatusRepository.OnGetAllOrderStatusCallback() {
            @Override
            public void onSuccess(List<String> orderStatuslist, Map<String, List<String>> ordstateMap) {
                view.hideloader();
                view.showAllOrderTaskStatus(orderStatuslist,ordstateMap);
            }

            @Override
            public void onError(String message) {
                view.hideloader();
                view.showMessage(message);
                view.hideViewForOrderStatus();
            }
        };

        changeOrderStatusCallback=new IChangeStatusRepository.OnChangeOrderStatusCallback() {
            @Override
            public void onSuccess() {
                view.hideloader();
                view.orderStatusChangeComplete();
            }

            @Override
            public void onError(String message) {
                view.hideloader();
                view.showMessage(message);
            }
        };
    }

    @Override
    public void unSubscribeCallbacks() {
        orderStatusCallback=null;
        changeOrderStatusCallback=null;
    }
}
