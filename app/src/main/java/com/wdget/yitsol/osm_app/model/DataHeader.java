package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataHeader
{
    @SerializedName("mnemonic_path")
    @Expose
    private String mnemonicPath;
    public String getMnemonicPath() {
        return mnemonicPath;
    }

    public void setMnemonicPath(String mnemonicPath) {
        this.mnemonicPath = mnemonicPath;
    }

}
