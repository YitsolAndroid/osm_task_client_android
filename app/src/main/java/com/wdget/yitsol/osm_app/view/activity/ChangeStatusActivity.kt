package com.wdget.yitsol.osm_app.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.FrameLayout
import com.jaredrummler.materialspinner.MaterialSpinner
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.R.string.task
import com.wdget.yitsol.osm_app.model.OrderTaskModel
import com.wdget.yitsol.osm_app.presenter.IChangeStatusPresenter
import com.wdget.yitsol.osm_app.presenter.ITaskSpnrItemListener
import com.wdget.yitsol.osm_app.presenter.impl.ChangeStatusPresenter
import com.wdget.yitsol.osm_app.utils.AppConstant
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver
import com.wdget.yitsol.osm_app.utils.FontType
import com.wdget.yitsol.osm_app.utils.LinearLayoutSpaceItemDecoration
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity
import com.wdget.yitsol.osm_app.view.adapter.OrderStateStatusAdapter
import kotlinx.android.synthetic.main.change_status.*
import java.util.*

class ChangeStatusActivity : BaseNavigationActivity() {

    private var container: FrameLayout?=null
    private var orderId:String?=null
    private var refer_no:String?=null
    private var taskId:String?=null
    private var ord_status:String?=null
    private var select_task:String?=null
    private var presenter:IChangeStatusPresenter?=null
    private var ord_tasklistdata:MutableList<OrderTaskModel>?=null
    private var ord_stateStatusAdapter:OrderStateStatusAdapter?=null
    private var root: ViewGroup? = null

    override fun getActContext(): Context {
        return this@ChangeStatusActivity
    }

    override fun initScreen() {
        container=getContainerLayout()
        container?.addView(inflater?.inflate(R.layout.change_status, null),
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        ord_tasklistdata=ArrayList()
        presenter=ChangeStatusPresenter(this)
        getDataFromIntent()
        btn_update.setOnClickListener({
            if(ord_status!=null)
            {
                if(ord_status.equals(AppConstant.STATEACCEPTED))
                {
                    presenter?.changeOrderStatus(getActContext(),orderId,taskId,select_task,ord_status)
                }
                else if(ord_status.equals(AppConstant.STATECOMPLETED))
                {
                    presenter?.changeOrderStatus(getActContext(),orderId,taskId,select_task,ord_status)
                }
                else if(ord_status.equals(AppConstant.STATERECEIVED))
                {
                    presenter?.changeOrderStatus(getActContext(),orderId,taskId,select_task,ord_status)
                }
                else if(ord_status.equals(AppConstant.STATEASSIGNED))
                {
                    presenter?.changeOrderStatus(getActContext(),orderId,taskId,select_task,ord_status)
                }
            }
            else
            {
                showSnackMessage("Please Select Order Status")
            }

        })
    }

    private fun getDataFromIntent() {
        orderId=intent.getStringExtra(AppConstant.ORDER_ID)
        taskId=intent.getStringExtra(AppConstant.TASK_ID)
        refer_no=intent.getStringExtra(AppConstant.REFERNCE)
    }

    override fun onStart() {
        super.onStart()
        presenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        presenter?.unSubscribeCallbacks()
    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()
        txt_OrderId.text="Order Id # "+orderId
        txt_Ref.text="Ref No # "+refer_no
        txt_Task.text="Order Task # "+taskId
        val connectStatus= ConnectivityReceiver.isConnected()
        setOrderTaskAdapter(ord_tasklistdata)
        if(connectStatus)
        {
            presenter?.getAllStatusForOrder(getActContext(),orderId,taskId)
        }
        else
        {
            showCustomSnackBar(AppConstant.NO_CONNECTION,ll_changeStatus)
        }
    }

    fun showSnackMessage(message:String)
    {
        showCustomSnackBar(message,ll_changeStatus)
    }

    private fun setOrderTaskAdapter(ord_tasklistdata: MutableList<OrderTaskModel>?) {
        list_status.layoutManager = LinearLayoutManager(getActContext(), LinearLayoutManager.VERTICAL, false)
        list_status.addItemDecoration(LinearLayoutSpaceItemDecoration(5))
        ord_stateStatusAdapter= OrderStateStatusAdapter(getActContext(),ord_tasklistdata!!)
        ord_stateStatusAdapter?.registerItemClickListener(object:ITaskSpnrItemListener{
            override fun getClickTaskName(name: String?) {
                select_task=name
            }

        })
        list_status.adapter=ord_stateStatusAdapter
    }

    override fun onPause() {
        super.onPause()
    }

    fun hideViewForOrderStatus()
    {
        spnr_task.visibility=View.GONE
        list_status.visibility=View.GONE
    }

    fun showAllOrderTaskStatus(orderStatuslist: MutableList<String>, ordstateMap: MutableMap<String, MutableList<String>>)
    {

        spnr_task.setItems(orderStatuslist)

        spnr_task.setOnItemSelectedListener(object :MaterialSpinner.OnItemSelectedListener<String>{
            override fun onItemSelected(view: MaterialSpinner?, position: Int, id: Long, item: String?) {
                ord_status=item
                if(!item.equals("Select Order Status")&&ordstateMap.size>0)
                {
                    var ord_TaskList=ordstateMap.get(ord_status!!)
                    if(ord_TaskList!=null&&ord_TaskList.size>0)
                    {
                        list_status.visibility=View.VISIBLE
                        var ord_Tasklistdata:MutableList<OrderTaskModel>?=ArrayList()
                        for(taskName:String in ord_TaskList)
                        {
                            ord_Tasklistdata?.add(OrderTaskModel(taskName,false))
                        }
                        setListData(ord_Tasklistdata)
                    }
                    else
                    {
                        list_status.visibility=View.VISIBLE
                        var ord_Tasklistdata:MutableList<OrderTaskModel>?=ArrayList()
                        ord_Tasklistdata?.add(OrderTaskModel(ord_status,false))
                        setListData(ord_Tasklistdata)
                    }

                }
                else
                {
                    if(!item.equals("Select Order Status"))
                    {
                        list_status.visibility=View.VISIBLE
                        var ord_Tasklistdata:MutableList<OrderTaskModel>?=ArrayList()
                        ord_Tasklistdata?.add(OrderTaskModel(ord_status,false))
                        setListData(ord_Tasklistdata)
                    }
                    else
                    {
                        list_status.visibility=View.GONE
                        ord_status = AppConstant.NOT_AVAILABLE
                    }

                }
            }

        })

        spnr_task.setOnNothingSelectedListener {

        }


        /*spnr_task.attachDataSource(orderStatuslist)
        spnr_task.setOnItemSelectedListener(object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                ord_status=AppConstant.NOT_AVAILABLE
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position != 0) {
                    var ord_TaskList:MutableList<String>?=ArrayList<String>()
                    var index = position-1
                    ord_status=parent?.getItemAtPosition(index).toString()
                    if(!ord_status.equals("Select Order Status"))
                    {
                        ord_TaskList=ordstateMap.get(ord_status!!)
                        if(ord_TaskList!=null&&ord_TaskList.size>0)
                        {
                            list_status.visibility=View.VISIBLE
                            var ord_Tasklistdata:MutableList<OrderTaskModel>?=ArrayList()
                            for(taskName:String in ord_TaskList)
                            {
                                ord_Tasklistdata?.add(OrderTaskModel(taskName,false))
                            }
                            setListData(ord_Tasklistdata)
                        }
                        else
                        {
                            list_status.visibility=View.VISIBLE
                            var ord_Tasklistdata:MutableList<OrderTaskModel>?=ArrayList()
                            ord_Tasklistdata?.add(OrderTaskModel(ord_status,false))
                            setListData(ord_Tasklistdata)
                        }
                    }
                    else
                    {
                        list_status.visibility=View.GONE
                        ord_status = AppConstant.NOT_AVAILABLE
                    }

                }
                else
                {
                    list_status.visibility=View.GONE
                    ord_status = AppConstant.NOT_AVAILABLE
                }
            }
        })*/
    }


    private fun setListData(orderTaskList: MutableList<OrderTaskModel>?) {
        ord_stateStatusAdapter?.refreshTaskListAdapter(orderTaskList!!)
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        gotoWorkListScreen()
    }

    private fun gotoWorkListScreen() {
        val worklistIntent= Intent(getActContext(),WorkListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(worklistIntent)
    }

    fun orderStatusChangeComplete()
    {
        gotoWorkListScreen()
    }

}
