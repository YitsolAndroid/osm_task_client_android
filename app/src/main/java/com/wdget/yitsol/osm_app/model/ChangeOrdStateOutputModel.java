package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangeOrdStateOutputModel
{
    @SerializedName("env:Body")
    @Expose
    private EnvBody envBody;

    public EnvBody getEnvBody() {
        return envBody;
    }

    public void setEnvBody(EnvBody envBody) {
        this.envBody = envBody;
    }

}
