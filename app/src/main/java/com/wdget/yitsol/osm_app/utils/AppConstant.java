package com.wdget.yitsol.osm_app.utils;


import com.wdget.yitsol.osm_app.view.activity.OrdersReportingActivity;
import com.wdget.yitsol.osm_app.view.activity.QueryActivity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by yitsol on 12-03-2018.
 */

public interface AppConstant
{
    int MALE_TYPE=1;
    int FEMALE_TYPE=2;

    String GOOD_CONNECTION="Good! Connected to Internet";
    String NO_CONNECTION="Sorry! Not connected to Internet";

    @Nullable
    String REGISTER="Register";
    @Nullable
    String LOGIN="App Login";
    String APP_PREF_NAME = "OSMAPP_PREF";
    String NO_RECORD_FOUND = "No Record Found";
    String NO_TEST_RECORD_FOUND="NO TEST RESULT FOUND";
    String NOT_AVAILABLE = "NA";
    @Nullable
    String DASHBOARD="DASHBOARD";
    @NotNull
    String AGETEXT="Age: ";
    @Nullable
    String CANDIDATEQRCODE="candidateqrcode";
    String NORANKFOUND = "No Rank Found";
    int UNPROCESSENTITY=422;
    @NotNull
    int HTTP_422=422;
    String PUBLIC_IP="192.168.0.102";
    String PORT="8080";
    @Nullable
    String ORDER_ID="orderId";
    @Nullable
    String TASK_ID="taskId";
    @Nullable
    String SOURCE="source";
    @Nullable
    String ORD_TYPE="ord_type";
    @Nullable
    String REFERNCE="reference";
    @Nullable
    String STATEACCEPTED="Accepted";
    @Nullable
    String STATECOMPLETED="Completed";
    @Nullable
    String STATERECEIVED="Received";
    @Nullable
    String STATEASSIGNED="Assigned";
    @NotNull
    String ACTIONSUSPEND="suspendOrder";
    @NotNull
    String ACTIONCANCEL="cancelOrder";
    @NotNull
    String ACTIONRESUME="resumeOrder";
    @NotNull
    String ACTIONABORT="abortOrder";
    @NotNull
    String NOTSTARTED="Not_started";
    @Nullable
    String QUERYORDERLIST="queryorderlist";
    @Nullable
    String SUSPENDORDER="Suspended";
    @NotNull
    String CANCELLEDORD="Cancelled";
    @Nullable
    String FROM_CLASS="fromclass";
    @Nullable
    String QUERYCLASS= QueryActivity.class.getSimpleName();
    @Nullable
    String REPORTCLASS= OrdersReportingActivity.class.getSimpleName();
}
