package com.wdget.yitsol.osm_app.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.OrdProcessHistOutputModel
import com.wdget.yitsol.osm_app.model.TransitionData
import com.wdget.yitsol.osm_app.presenter.IProcessHistPresenter
import com.wdget.yitsol.osm_app.presenter.impl.ProcessHistPresenter
import com.wdget.yitsol.osm_app.utils.AppConstant
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver
import com.wdget.yitsol.osm_app.utils.CurrentDate
import com.wdget.yitsol.osm_app.utils.LinearLayoutSpaceItemDecoration
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity
import com.wdget.yitsol.osm_app.view.adapter.ProcessHistAdapter
import kotlinx.android.synthetic.main.process_history.*

class ProcessHistActivity : BaseNavigationActivity() {

    private var container: FrameLayout?=null
    private var presenter:IProcessHistPresenter?=null
    private var orderId:String?=null
    private var ordtransitiondatalist:List<TransitionData>?=null
    private var procsshistAdapter: ProcessHistAdapter?=null


    override fun getActContext(): Context {
        return this@ProcessHistActivity
    }

    override fun initScreen() {
        container=getContainerLayout()
        container?.addView(inflater?.inflate(R.layout.process_history, null),
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        presenter=ProcessHistPresenter(this)
        ordtransitiondatalist=ArrayList()
        getDataFromIntent()
    }

    private fun getDataFromIntent() {
        orderId=intent.getStringExtra(AppConstant.ORDER_ID)
    }

    override fun onStart() {
        super.onStart()
        presenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        presenter?.unSubscribeCallbacks()
    }

    override fun onResume() {
        super.onResume()
        setProcessHistAdapter(ordtransitiondatalist)
        val connectStatus= ConnectivityReceiver.isConnected()
        if(connectStatus)
        {
            presenter?.getOrderProcessHistory(getActContext(),orderId)
        }
        else
        {
            showCustomSnackBar(AppConstant.NO_CONNECTION,ll_processHistory)
        }
    }

    private fun setProcessHistAdapter(ordtransitiondatalist: List<TransitionData>?) {
        rcly_ProcessHistory.layoutManager = LinearLayoutManager(getActContext(), LinearLayoutManager.VERTICAL, false)
        rcly_ProcessHistory.addItemDecoration(LinearLayoutSpaceItemDecoration(5))
        procsshistAdapter= ProcessHistAdapter(getActContext(),ordtransitiondatalist!!)
        rcly_ProcessHistory.adapter=procsshistAdapter
    }

    override fun onPause() {
        super.onPause()
    }

    @SuppressLint("SetTextI18n")
    fun showordProcessHistory(outputModel: OrdProcessHistOutputModel)
    {
        if(outputModel.getOrderProcessHistoryResponse.transitions.transition.size>0)
        {
            ordtransitiondatalist=outputModel.getOrderProcessHistoryResponse.transitions.transition
            procsshistAdapter!!.refreshAdapter(ordtransitiondatalist!!)
            txt_ph_OrderId.text="Ord Id # "+outputModel.getOrderProcessHistoryResponse.orderID
            txt_ph_Ref.visibility= View.GONE
            txt_ph_started.text="Start Date "+CurrentDate().getOSMOrderFormattedDate(outputModel.getOrderProcessHistoryResponse.summary.startDate)
            txt_ph_Actualduration.text=convertMillisToTime(outputModel.getOrderProcessHistoryResponse.summary.actualDuration)
            txt_ph_Excepted.text="Exp Start Date"+CurrentDate().getOSMOrderFormattedDate(outputModel.getOrderProcessHistoryResponse.summary.expectedCompletionDate)

        }
    }


    fun convertMillisToTime(millivalue:Long): String
    {
        val millis = millivalue % 1000
        val second = millivalue / 1000 % 60
        val minute = millivalue / (1000 * 60) % 60
        val hour = millivalue / (1000 * 60 * 60) % 24

        val time = String.format("%02d:%02d:%02d.%d"+"(hr:min:sec:mils)", hour, minute, second, millis)

        return time
    }

    override fun onBackPressed() {
        //super.onBackPressed()
        gotoWorkListScreen()
    }

    private fun gotoWorkListScreen() {
        val worklistIntent=Intent(getActContext(),WorkListActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(worklistIntent)
    }
}
