package com.wdget.yitsol.osm_app.repository.impl;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.ChangeOrdStateInputModel;
import com.wdget.yitsol.osm_app.model.ChangeOrdStateOutputModel;
import com.wdget.yitsol.osm_app.model.ProcessOrderModel;
import com.wdget.yitsol.osm_app.model.ReceivedOrdInputModel;
import com.wdget.yitsol.osm_app.model.ReceivedOrdOutputModel;
import com.wdget.yitsol.osm_app.model.WorkListDataModel;
import com.wdget.yitsol.osm_app.model.WorkListInputModel;
import com.wdget.yitsol.osm_app.model.WorkListOrderdata;
import com.wdget.yitsol.osm_app.model.WorkListResultModel;
import com.wdget.yitsol.osm_app.repository.IWorkListRepository;
import com.wdget.yitsol.osm_app.service.APIService;
import com.wdget.yitsol.osm_app.service.APIUtils;
import com.wdget.yitsol.osm_app.utils.AppConstant;
import com.wdget.yitsol.osm_app.utils.AppPreference;
import com.wdget.yitsol.osm_app.utils.Encryption;
import com.wdget.yitsol.osm_app.utils.OrderSortById;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkListRepository implements IWorkListRepository {

    APIService apiService;

    public WorkListRepository() {
        apiService= APIUtils.getAPIService();
    }

    @Override
    public void getProcessOfOrderCall(Context context, final OnProcessOfOrderCallback callback) {
        try {
            AppPreference appPreference=new AppPreference(context);
            HashMap<String,String> logindetails=appPreference.getUserDetails();
            String username=logindetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password=logindetails.get(appPreference.KEY_USERLOGINPASSWORD);
            Encryption encryption=new Encryption();
            String encodeInput=encryption.encrypt(username+":"+password);
            Map<String,String> encodedheader=new HashMap<>();
            encodedheader.put("Auth",encodeInput);
            apiService.getProcessOfOrderCall(encodedheader).enqueue(new Callback<List<ProcessOrderModel>>() {
                @Override
                public void onResponse(Call<List<ProcessOrderModel>> call, Response<List<ProcessOrderModel>> response) {
                    if(callback!=null)
                    {
                        if(response.code()== HttpURLConnection.HTTP_OK)
                        {
                            List<ProcessOrderModel> datalist=response.body();
                            if(datalist.size()>0)
                            {
                                callback.onSuccess(datalist);
                            }
                        }
                    }

                }

                @Override
                public void onFailure(Call<List<ProcessOrderModel>> call, Throwable t) {
                    if(callback!=null)
                    {
                        call.cancel();
                        callback.onError("server error");
                    }
                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void getAllWorkListDataCall(Context context, final List<ProcessOrderModel> processOrderdatalist, final OnGetAllWorkListCallback callback) {
        try {
            final List<WorkListDataModel> worklistdata=new ArrayList<>();
            AppPreference appPreference=new AppPreference(context);
            HashMap<String,String> logindetails=appPreference.getUserDetails();
            String username=logindetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password=logindetails.get(appPreference.KEY_USERLOGINPASSWORD);
            Encryption encryption=new Encryption();
            String encodeInput=encryption.encrypt(username+":"+password);
            Map<String,String> encodedheader=new HashMap<>();
            encodedheader.put("Auth",encodeInput);
            WorkListInputModel inputModel=new WorkListInputModel(username,password, AppConstant.PUBLIC_IP,AppConstant.PORT);
            apiService.getAllWorkListData(encodedheader,inputModel).enqueue(new Callback<WorkListResultModel>() {
                @Override
                public void onResponse(Call<WorkListResultModel> call, Response<WorkListResultModel> response) {
                    if(callback!=null)
                    {
                        if(response.code()==HttpURLConnection.HTTP_OK)
                        {
                            WorkListResultModel listModel=response.body();
                            if((listModel != null ? listModel.getWorklistResponse() : null) !=null)
                            {
                            if (listModel.getWorklistResponse().getOrderdata() != null) {
                                for (ProcessOrderModel model : processOrderdatalist) {
                                    long OrderSeqId = Long.parseLong(model.getOrderSeqId());

                                    for (WorkListOrderdata orderData : listModel.getWorklistResponse().getOrderdata()) {
                                        if (orderData.getOrderSeqId() == OrderSeqId) {
                                            if(worklistdata.size()==0)
                                            {
                                                worklistdata.add(new WorkListDataModel(String.valueOf(orderData.getOrderSeqId()), orderData.getNamespace()
                                                        , orderData.getOrderType(), orderData.getOrderState(), model.getProcessIdDescription(), String.valueOf(orderData.getOrderHistSeqId())
                                                        , String.valueOf(orderData.getReferenceNumber()), orderData.getVersion(), orderData.getCurrentOrderState(), orderData.getUser()
                                                        , orderData.getTaskId(), orderData.getOrderSource(), orderData.getExecutionMode(), String.valueOf(orderData.getNumRemarks())
                                                        , String.valueOf(orderData.getPriority())));
                                            }
                                            else
                                            {
                                                if(!findOrderByIdInList(worklistdata,orderData.getOrderSeqId()))
                                                {
                                                    worklistdata.add(new WorkListDataModel(String.valueOf(orderData.getOrderSeqId()), orderData.getNamespace()
                                                            , orderData.getOrderType(), orderData.getOrderState(), model.getProcessIdDescription(), String.valueOf(orderData.getOrderHistSeqId())
                                                            , String.valueOf(orderData.getReferenceNumber()), orderData.getVersion(), orderData.getCurrentOrderState(), orderData.getUser()
                                                            , orderData.getTaskId(), orderData.getOrderSource(), orderData.getExecutionMode(), String.valueOf(orderData.getNumRemarks())
                                                            , String.valueOf(orderData.getPriority())));
                                                }
                                            }

                                        }

                                    }
                                }
                                Collections.sort(worklistdata,new OrderSortById());
                                callback.onSuccess(listModel, processOrderdatalist, worklistdata);
                            }
                            else
                            {
                                callback.onError("server error");
                            }
                            }
                            else
                            {
                                callback.onError("WORK LIST NOT FOUND");
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<WorkListResultModel> call, Throwable t) {
                    if(callback!=null)
                    {
                        call.cancel();
                        callback.onError("server error");
                    }
                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private boolean findOrderByIdInList(List<WorkListDataModel> worklistdata, long orderSeqId) {
        boolean foundStatus=false;
        for(WorkListDataModel dataModel:worklistdata)
        {
            if(dataModel.getOrderId().equals(String.valueOf(orderSeqId)))
            {
                foundStatus=true;
                break;
            }
        }
        return foundStatus;
    }

    @Override
    public void changeOrderCurrentState(Context context, @Nullable final String reason, @Nullable final String actiontype, final String orderId, final String taskId, final OnOrderCurrentStateCallback callback) {
        try {
            AppPreference appPreference=new AppPreference(context);
            HashMap<String,String> userdetails=appPreference.getUserDetails();
            String username=userdetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password=userdetails.get(appPreference.KEY_USERLOGINPASSWORD);
            String encoded_string=new Encryption().encrypt(username+":"+password);
            Map<String,String> encoded_header=new HashMap<>();
            encoded_header.put("Auth",encoded_string);
            ReceivedOrdInputModel inputModel=new ReceivedOrdInputModel(username,password,orderId,AppConstant.PUBLIC_IP,AppConstant.PORT,taskId);
            apiService.receivedOrdStatusChange(encoded_header,inputModel).enqueue(new Callback<ReceivedOrdOutputModel>() {
                @Override
                public void onResponse(Call<ReceivedOrdOutputModel> call, Response<ReceivedOrdOutputModel> response) {
                    if(callback!=null)
                    {
                        if(response.code()==HttpURLConnection.HTTP_OK)
                        {
                            callback.onSuccess(reason,actiontype,orderId,taskId);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ReceivedOrdOutputModel> call, Throwable t) {
                    if(callback!=null)
                    {
                        call.cancel();
                        callback.onError("server error");
                    }
                }
            });

        }catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void changeOrderStatebyId(Context context, @NotNull String reason, @NotNull String actiontype, @NotNull String orderId,
                                     @NotNull String taskId, final OnChangeOrderStateByIdCallback callback)
    {
        try {
            AppPreference appPreference=new AppPreference(context);
            HashMap<String,String> userdetails=appPreference.getUserDetails();
            String username=userdetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password=userdetails.get(appPreference.KEY_USERLOGINPASSWORD);
            String encoded_string=new Encryption().encrypt(username+":"+password);
            Map<String,String> encoded_header=new HashMap<>();
            encoded_header.put("Auth",encoded_string);
            ChangeOrdStateInputModel inputModel=new ChangeOrdStateInputModel(username,password,orderId,reason);
            if(actiontype.equals(AppConstant.ACTIONSUSPEND))
            {
                performChangeStateNetworkCall(actiontype,encoded_header,inputModel,callback);
            }
            else if(actiontype.equals(AppConstant.ACTIONCANCEL))
            {
                performChangeStateNetworkCall(actiontype,encoded_header,inputModel,callback);
            }
            else if(actiontype.equals(AppConstant.ACTIONRESUME))
            {
                performChangeStateNetworkCall(actiontype,encoded_header,inputModel,callback);
            }
            else if(actiontype.equals(AppConstant.ACTIONABORT))
            {
                performChangeStateNetworkCall(actiontype,encoded_header,inputModel,callback);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private void performChangeStateNetworkCall(String actiontype, Map<String, String> encoded_header, ChangeOrdStateInputModel inputModel,
                                               final OnChangeOrderStateByIdCallback callback)
    {

        apiService.changeOrderById(actiontype,encoded_header,inputModel).enqueue(new Callback<ChangeOrdStateOutputModel>() {
            @Override
            public void onResponse(Call<ChangeOrdStateOutputModel> call, Response<ChangeOrdStateOutputModel> response) {
                if(callback!=null)
                {
                    if(response.code()==HttpURLConnection.HTTP_OK)
                    {
                        callback.onSuccess();
                        /*ChangeOrdStateOutputModel outputModel=response.body();
                        if(outputModel!=null)
                        {
                            if(outputModel.getEnvBody().getSuspendOrderResponse().getOrderId()!=0)
                            {
                                callback.onSuccess();
                            }
                        }
                        else
                        {
                            callback.onError("server error");
                        }*/
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangeOrdStateOutputModel> call, Throwable t) {
                if(callback!=null)
                {
                    call.cancel();
                    callback.onError("server error");
                }
            }
        });
    }
}
