package com.wdget.yitsol.osm_app.presenter;

import android.content.Context;

import com.wdget.yitsol.osm_app.presenter.base.IBasePresenter;

public interface IActionEditorPresenter extends IBasePresenter
{
    void getProcessMnemonic(Context context, String orderId, String taskId);
    void getProcessPriority(Context context);
}
