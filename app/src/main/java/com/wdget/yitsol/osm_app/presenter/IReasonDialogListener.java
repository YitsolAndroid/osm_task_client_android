package com.wdget.yitsol.osm_app.presenter;

public interface IReasonDialogListener
{
    void onOkButtonClick(String reason,String actiontype);
    void showSnackMessage(String message);
}
