package com.wdget.yitsol.osm_app.view.activity

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.ViewGroup
import android.widget.FrameLayout
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.OrdersReportResponse
import com.wdget.yitsol.osm_app.presenter.IReportingPresenter
import com.wdget.yitsol.osm_app.presenter.impl.ReportingPresenter
import com.wdget.yitsol.osm_app.utils.AppConstant
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity
import kotlinx.android.synthetic.main.chart.*
import org.achartengine.ChartFactory
import org.achartengine.GraphicalView
import org.achartengine.model.CategorySeries
import org.achartengine.renderer.DefaultRenderer
import org.achartengine.renderer.SimpleSeriesRenderer

class StatisticsActivity: BaseNavigationActivity(){
    private var container: FrameLayout?=null
    private var mChart: GraphicalView? = null
    private var presenter: IReportingPresenter?=null
    private val connectStatus= ConnectivityReceiver.isConnected()
    var total: Int = 0
    var completed: Int = 0
    var failed: Int = 0
    var pending: Int = 0

    override fun getActContext(): Context {
        return this@StatisticsActivity
    }

    override fun initScreen() {
        container = getContainerLayout()

        container!!.addView(inflater!!.inflate(R.layout.chart,null),
                ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT)

        presenter = ReportingPresenter(this)
    }

    override fun onResume() {
        super.onResume()
        if(connectStatus)
        {
            presenter?.getOrdersReport(getActContext())
        }
        else
        {
            toastMessage(AppConstant.NO_CONNECTION)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        presenter?.unSubscribeCallbacks()
    }

    private fun openChart() {

        // Pie Chart Section Names
        val code = arrayOf("Total Orders("+ total.toString() + ")", "Completed Orders("+ completed.toString() + ")", "Pending Orders("+ pending.toString() + ")", "Failed Orders("+ failed.toString() + ")")

        // Pie Chart Section Value
        val distribution = doubleArrayOf(java.lang.Double.parseDouble(total.toString()), java.lang.Double.parseDouble(completed.toString()), java.lang.Double.parseDouble(pending.toString()), java.lang.Double.parseDouble(failed.toString()))

        // Color of each Pie Chart Sections
        val colors = intArrayOf(Color.parseColor("#3090f0"), Color.parseColor("#0DEF58"), Color.parseColor("#EF9C0D"), Color.parseColor("#EF3A0D"))

        // Instantiating CategorySeries to plot Pie Chart
        val distributionSeries = CategorySeries("")
        for (i in distribution.indices) {
            // Adding a slice with its values and name to the Pie Chart
            distributionSeries.add(code[i], distribution[i])
        }
        // Instantiating a renderer for the Pie Chart


        val defaultRenderer = DefaultRenderer()
        for (i in distribution.indices) {
            val seriesRenderer = SimpleSeriesRenderer()
            seriesRenderer.color = colors[i]
            seriesRenderer.isDisplayChartValues = true
            // Adding a renderer for a slice
            defaultRenderer.addSeriesRenderer(seriesRenderer)
        }

        defaultRenderer.isApplyBackgroundColor = true
        //	defaultRenderer.setBackgroundColor(Color.parseColor("#157DEC"));
        defaultRenderer.labelsColor = Color.BLACK
        defaultRenderer.labelsTextSize = 35f
        defaultRenderer.scale = 0.75.toFloat()
        defaultRenderer.chartTitle = "Osm Reports"
        defaultRenderer.chartTitleTextSize = 45f
        defaultRenderer.legendTextSize = 40f
        defaultRenderer.isPanEnabled = false
       // defaultRenderer.setDisplayValues(true)
        defaultRenderer.isShowAxes


        mChart = ChartFactory.getPieChartView(getActContext(), distributionSeries, defaultRenderer) as GraphicalView
        ll_chart.addView(mChart)
    }
    public fun onOrderReportingSuccess(ordersReportResponseList:  MutableList<OrdersReportResponse>){
        getDifferentReports(ordersReportResponseList)
    }
    private fun getDifferentReports(ordersReportingList: MutableList<OrdersReportResponse>?){


        for (item: OrdersReportResponse in ordersReportingList!!){
            total = total.plus(item.total.toInt())
            Log.e(item.mnemonic,item.total)
            if(item.mnemonic.equals("not_started") || item.mnemonic.equals("suspended") || item.mnemonic.equals("in_progress")||
                    item.mnemonic.equals("cancelling")|| item.mnemonic.equals("amending")||
                    item.mnemonic.equals("waiting_for_revision")){

                pending = pending.plus(item.total.toInt())
            }

            if(item.mnemonic.equals("cancelled") || item.mnemonic.equals("completed") || item.mnemonic.equals("aborted")){

                completed = completed.plus(item.total.toInt())
            }

            if(item.mnemonic.equals("failed")){
                failed = failed.plus(item.total.toInt())
            }
        }

        openChart()
    }
}