package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QueryDataModel {

    @SerializedName("orderStatus")
    @Expose
    public List<OrderStateResponse> orderStatus;


    @SerializedName("task")
    @Expose
    public List<TaskResponse> task;

    @SerializedName("catridges")
    @Expose
    public List<CatridgeResponse> catridges;

    @SerializedName("source")
    @Expose
    public List<OrderSourceResponse> source;

    @SerializedName("users")
    @Expose
    public List<UserResponse> users;

    @SerializedName("processes")
    @Expose
    public List<ProcessStatusResponse> processes;

    @SerializedName("allOrderStates")
    @Expose
    public List<AllOrderStatesResponse> allOrderStates;

    @SerializedName("type")
    @Expose
    public List<OrderTypeRespnse> type;

    public List<OrderStateResponse> getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(List<OrderStateResponse> orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<TaskResponse> getTask() {
        return task;
    }

    public void setTask(List<TaskResponse> task) {
        this.task = task;
    }

    public List<CatridgeResponse> getCatridges() {
        return catridges;
    }

    public void setCatridges(List<CatridgeResponse> catridges) {
        this.catridges = catridges;
    }

    public List<OrderSourceResponse> getSource() {
        return source;
    }

    public void setSource(List<OrderSourceResponse> source) {
        this.source = source;
    }

    public List<UserResponse> getUsers() {
        return users;
    }

    public void setUsers(List<UserResponse> users) {
        this.users = users;
    }

    public List<ProcessStatusResponse> getProcesses() {
        return processes;
    }

    public void setProcesses(List<ProcessStatusResponse> processes) {
        this.processes = processes;
    }

    public List<AllOrderStatesResponse> getAllOrderStates() {
        return allOrderStates;
    }

    public void setAllOrderStates(List<AllOrderStatesResponse> allOrderStates) {
        this.allOrderStates = allOrderStates;
    }

    public List<OrderTypeRespnse> getType() {
        return type;
    }

    public void setType(List<OrderTypeRespnse> type) {
        this.type = type;
    }
}
