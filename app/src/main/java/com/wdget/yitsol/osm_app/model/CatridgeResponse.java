package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CatridgeResponse {

    @SerializedName("namespace")
    @Expose
    public String namespace;

    @SerializedName("version")
    @Expose
    public String version;

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
