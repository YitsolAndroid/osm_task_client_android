package com.wdget.yitsol.osm_app.repository.impl;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.OrdProcessHistInputModel;
import com.wdget.yitsol.osm_app.model.OrdProcessHistOutputModel;
import com.wdget.yitsol.osm_app.repository.IProcessHistRepository;
import com.wdget.yitsol.osm_app.service.APIService;
import com.wdget.yitsol.osm_app.service.APIUtils;
import com.wdget.yitsol.osm_app.utils.AppConstant;
import com.wdget.yitsol.osm_app.utils.AppPreference;
import com.wdget.yitsol.osm_app.utils.Encryption;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProcessHistRepository implements IProcessHistRepository
{
    private APIService apiService;

    public ProcessHistRepository() {
        apiService= APIUtils.getAPIService();
    }

    @Override
    public void getOrderProcessHistory(Context context, String orderId, final OnProcessHistCallback callback) {
        try {
            AppPreference appPreference=new AppPreference(context);
            HashMap<String,String> userdetails=appPreference.getUserDetails();
            String username=userdetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password=userdetails.get(appPreference.KEY_USERLOGINPASSWORD);
            String encoded_string=new Encryption().encrypt(username+":"+password);
            Map<String,String> encode_header=new HashMap<>();
            encode_header.put("Auth",encoded_string);
            OrdProcessHistInputModel inputModel=new OrdProcessHistInputModel(username,password,orderId, AppConstant.PUBLIC_IP
            ,AppConstant.PORT);
            apiService.getOrderProcessHistory(encode_header,inputModel).enqueue(new Callback<OrdProcessHistOutputModel>() {
                @Override
                public void onResponse(Call<OrdProcessHistOutputModel> call, Response<OrdProcessHistOutputModel> response) {
                    if(callback!=null)
                    {
                        if(response.code()== HttpURLConnection.HTTP_OK)
                        {
                            OrdProcessHistOutputModel outputModel=response.body();
                            callback.onSuccess(outputModel);
                        }
                    }
                }

                @Override
                public void onFailure(Call<OrdProcessHistOutputModel> call, Throwable t) {
                    if(callback!=null)
                    {
                        call.cancel();
                        callback.onError("server error");
                    }
                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }
}
