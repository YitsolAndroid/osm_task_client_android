package com.wdget.yitsol.osm_app.repository;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.OrdersReportResponse;
import com.wdget.yitsol.osm_app.model.QueryOutputModel;

import java.util.List;

public interface IReportingRepository {

    interface OnOrdersReportingCallBack{

        void onSuccess(List<OrdersReportResponse> ordersReportResponse);

        void onError(String message);
    }

    interface OnSearchQueryByOrderStateCallback
    {
        void onSuccess(List<QueryOutputModel> datalist);
        void onError(String message);
    }
    void getOrdersReport(Context context, OnOrdersReportingCallBack callBack);

    void searchQueryByOrderState(Context context,String orderState,OnSearchQueryByOrderStateCallback callBack);
}
