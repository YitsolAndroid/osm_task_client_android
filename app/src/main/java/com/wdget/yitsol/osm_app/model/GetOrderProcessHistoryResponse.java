package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetOrderProcessHistoryResponse
{
    @SerializedName("xmlns")
    @Expose
    private String xmlns;
    @SerializedName("Version")
    @Expose
    private String version;
    @SerializedName("Transitions")
    @Expose
    private TransitionsModel transitions;
    @SerializedName("Summary")
    @Expose
    private SummaryModel summary;
    @SerializedName("OrderID")
    @Expose
    private long orderID;
    @SerializedName("Namespace")
    @Expose
    private String namespace;

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public TransitionsModel getTransitions() {
        return transitions;
    }

    public void setTransitions(TransitionsModel transitions) {
        this.transitions = transitions;
    }

    public SummaryModel getSummary() {
        return summary;
    }

    public void setSummary(SummaryModel summary) {
        this.summary = summary;
    }

    public long getOrderID() {
        return orderID;
    }

    public void setOrderID(long orderID) {
        this.orderID = orderID;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }
}
