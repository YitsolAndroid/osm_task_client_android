package com.wdget.yitsol.osm_app.view.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.wdget.yitsol.osm_app.R;
import com.wdget.yitsol.osm_app.presenter.IMessageDialogItemListner;


/**
 * Created by user on 5/5/2017.
 */

public class AppMessageDialog extends AppCompatDialog implements View.OnClickListener {

    private TextView txt_message,txt_ok;
    private String usermessage;
    private IMessageDialogItemListner listner;

    public AppMessageDialog(Context context) {
        super(context);
    }

    public AppMessageDialog(Context context, int theme) {
        super(context, theme);
    }

    public AppMessageDialog(Context context,String message,int theme)
    {
        this(context,theme);
        this.usermessage=message;
    }

    public void setDialogItemClickListener(IMessageDialogItemListner listener)
    {
        this.listner=listener;
    }

    protected AppMessageDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.appmessage_dialog);
        setCanceledOnTouchOutside(false);
        initView();
        setGradientColorforText(txt_message);
        setGradientColorforText(txt_ok);
        txt_message.setText(usermessage);
    }

    private void initView() {
        txt_ok= (TextView) findViewById(R.id.txt_ok);
        txt_message= (TextView) findViewById(R.id.txt_message);
        txt_ok.setOnClickListener(this);
    }

    private void setGradientColorforText(TextView textView)
    {
        Shader shader_gradient = new LinearGradient(0, 0, 0, textView.getTextSize(), Color.parseColor("#FFC11A"), Color.parseColor("#FFFBAF"),
                Shader.TileMode.MIRROR);
        textView.getPaint().setShader(shader_gradient);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void setOnDismissListener(@Nullable OnDismissListener listener) {
        super.setOnDismissListener(listener);
    }

    @Override
    public void setOnShowListener(@Nullable OnShowListener listener) {
        super.setOnShowListener(listener);
    }

    @Override
    public void setOnCancelListener(@Nullable OnCancelListener listener) {
        super.setOnCancelListener(listener);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_ok:
                dismiss();
                listner.OnOkButtonClick();
                break;
        }
    }


}
