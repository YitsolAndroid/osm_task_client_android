package com.wdget.yitsol.osm_app.repository;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.ProcessOrderModel;
import com.wdget.yitsol.osm_app.model.WorkListDataModel;
import com.wdget.yitsol.osm_app.model.WorkListModel;
import com.wdget.yitsol.osm_app.model.WorkListOrderdata;
import com.wdget.yitsol.osm_app.model.WorkListResultModel;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IWorkListRepository
{
    interface OnProcessOfOrderCallback
    {
        void onSuccess(List<ProcessOrderModel> processOrderdatalist);
        void onError(String message);
    }

    interface OnGetAllWorkListCallback
    {
        void onSuccess(WorkListResultModel listModel, List<ProcessOrderModel> processOrderdatalist, List<WorkListDataModel> worklistdata);
        void onError(String message);
    }

    interface OnOrderCurrentStateCallback
    {
        void onSuccess(String reason, String actiontype, String orderId, String taskId);
        void onError(String message);
    }

    interface OnChangeOrderStateByIdCallback
    {
        void onSuccess();
        void onError(String message);
    }

    void getProcessOfOrderCall(Context context,OnProcessOfOrderCallback callback);

    void getAllWorkListDataCall(Context context,List<ProcessOrderModel> processOrderdatalist,OnGetAllWorkListCallback callback);

    void changeOrderCurrentState(Context context,@Nullable String reason, @Nullable String actiontype,String orderId,String taskId,OnOrderCurrentStateCallback callback);

    void changeOrderStatebyId(Context context,@NotNull String reason, @NotNull String actiontype, @NotNull String orderId, @NotNull String taskId,OnChangeOrderStateByIdCallback callback);
}
