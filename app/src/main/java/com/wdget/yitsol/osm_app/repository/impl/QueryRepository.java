package com.wdget.yitsol.osm_app.repository.impl;

import android.content.Context;
import android.util.Log;

import com.wdget.yitsol.osm_app.model.QueryDataModel;
import com.wdget.yitsol.osm_app.model.QueryInputModel;
import com.wdget.yitsol.osm_app.model.QueryInputOrderIdModel;
import com.wdget.yitsol.osm_app.model.QueryInputStateModel;
import com.wdget.yitsol.osm_app.model.QueryOutputModel;
import com.wdget.yitsol.osm_app.repository.IQueryRepository;
import com.wdget.yitsol.osm_app.service.APIService;
import com.wdget.yitsol.osm_app.service.APIUtils;
import com.wdget.yitsol.osm_app.utils.AppConstant;
import com.wdget.yitsol.osm_app.utils.AppPreference;
import com.wdget.yitsol.osm_app.utils.Encryption;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QueryRepository implements IQueryRepository{

    APIService apiService;

    public QueryRepository(){

        apiService = APIUtils.getAPIService();
    }
    @Override
    public void getQueryDataCall(Context context, final OnQueryDataCallback callback) {

        try{
            AppPreference appPreference=new AppPreference(context);
            HashMap<String,String> logindetails=appPreference.getUserDetails();
            String username=logindetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password=logindetails.get(appPreference.KEY_USERLOGINPASSWORD);
            Encryption encryption=new Encryption();
            String encodeInput=encryption.encrypt(username+":"+password);
            Map<String,String> encodedheader=new HashMap<>();
            encodedheader.put("Auth",encodeInput);

            apiService.getQueryDataCall(encodedheader).enqueue(new Callback<QueryDataModel>() {
                @Override
                public void onResponse(Call<QueryDataModel> call, Response<QueryDataModel> response) {

                    if(callback != null){
                       Log.e("Response",response.body().getCatridges().get(0).getNamespace());
                        Log.e("Responsesss  ",response.body().toString());
                        callback.onSuccess(response.body());
                    }
                }

                @Override
                public void onFailure(Call<QueryDataModel> call, Throwable t) {
                    if(callback!=null)
                    {
                        call.cancel();
                        callback.onError("server error");
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void searchQuery(Context context, String orderId, String orderState, String nameSpace, String state, final OnOrderQueryCallback callback) {

        try {
            QueryInputModel inputModel=null;
            AppPreference appPreference = new AppPreference(context);
            HashMap<String, String> logindetails = appPreference.getUserDetails();
            String username = logindetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password = logindetails.get(appPreference.KEY_USERLOGINPASSWORD);
            Encryption encryption = new Encryption();
            String encodeInput = encryption.encrypt(username + ":" + password);
            Map<String, String> encodedheader = new HashMap<>();
            encodedheader.put("Auth", encodeInput);
            inputModel=new QueryInputModel(username,password,AppConstant.PUBLIC_IP,AppConstant.PORT
            ,orderId,orderState,nameSpace,state);

            apiService.performOrderQuery(encodedheader,inputModel).enqueue(new Callback<List<QueryOutputModel>>() {
                @Override
                public void onResponse(Call<List<QueryOutputModel>> call, Response<List<QueryOutputModel>> response) {
                    if(callback!=null)
                    {
                        if(response.code()== HttpURLConnection.HTTP_OK)
                        {
                            List<QueryOutputModel> datalist=response.body();
                            if (datalist != null && datalist.size() > 0) {
                                callback.onSuccess(datalist);
                            }
                            else
                            {
                                callback.onError("NO ORDER FOUND");
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<QueryOutputModel>> call, Throwable t) {
                    if(callback!=null)
                    {
                        call.cancel();
                        callback.onError("server error");
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
