package com.wdget.yitsol.osm_app.utils;

import android.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Encryption {
	
	
	private String encryptionKey="rzrzerzerz1ttqwd";

    

    public Encryption() {
		// TODO Auto-generated constructor stub
	}

	public String encrypt(String plainText) throws Exception
    {
        Cipher cipher = getCipher(Cipher.ENCRYPT_MODE);
        byte[] encryptedBytes = cipher.doFinal(plainText.getBytes());
        String encode = Base64.encodeToString(encryptedBytes, Base64.NO_WRAP);
        encode = "Basic "+encode;
        encode = encode.trim();
        byte[] finalEncrypt = encode.getBytes();
        encode = Base64.encodeToString(finalEncrypt, Base64.NO_WRAP);
        System.out.println(encode);

        return encode;
    }

    public String decrypt(String encrypted) throws Exception
    {
        Cipher cipher = getCipher(Cipher.DECRYPT_MODE);
        byte[] plainBytes = cipher.doFinal(Base64.decode(encrypted, Base64.DEFAULT));

        return new String(plainBytes);
    }

    private Cipher getCipher(int cipherMode)
            throws Exception
    {
        String encryptionAlgorithm = "AES";
        SecretKeySpec keySpecification = new SecretKeySpec(
                encryptionKey.getBytes("UTF-8"), encryptionAlgorithm);
        Cipher cipher = Cipher.getInstance(encryptionAlgorithm);
        cipher.init(cipherMode, keySpecification);

        return cipher;
    }

}
