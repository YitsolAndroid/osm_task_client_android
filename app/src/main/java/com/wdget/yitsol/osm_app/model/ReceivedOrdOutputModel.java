package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReceivedOrdOutputModel
{
    @SerializedName("ReceiveOrder.Response")
    @Expose
    private ReceiveOrderResponse receiveOrderResponse;

    public ReceiveOrderResponse getReceiveOrderResponse() {
        return receiveOrderResponse;
    }

    public void setReceiveOrderResponse(ReceiveOrderResponse receiveOrderResponse) {
        this.receiveOrderResponse = receiveOrderResponse;
    }
}
