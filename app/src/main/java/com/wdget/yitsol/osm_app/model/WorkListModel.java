package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkListModel
{
    @SerializedName("Worklist.Response")
    @Expose
    private WorklistResponse worklistResponse;

    public WorklistResponse getWorklistResponse() {
        return worklistResponse;
    }

    public void setWorklistResponse(WorklistResponse worklistResponse) {
        this.worklistResponse = worklistResponse;
    }
}
