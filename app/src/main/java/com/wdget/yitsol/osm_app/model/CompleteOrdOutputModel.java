package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompleteOrdOutputModel
{
    @SerializedName("CompleteOrder.Response")
    @Expose
    private CompleteOrderResponse completeOrderResponse;

    public CompleteOrderResponse getCompleteOrderResponse() {
        return completeOrderResponse;
    }

    public void setCompleteOrderResponse(CompleteOrderResponse completeOrderResponse) {
        this.completeOrderResponse = completeOrderResponse;
    }
}
