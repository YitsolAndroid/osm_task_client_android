package com.wdget.yitsol.osm_app.view.activity

import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.ProcessOrderModel
import com.wdget.yitsol.osm_app.model.WorkListDataModel
import com.wdget.yitsol.osm_app.presenter.IReasonDialogListener
import com.wdget.yitsol.osm_app.presenter.IWorkListItemClickListener
import com.wdget.yitsol.osm_app.presenter.IWorkListPresenter
import com.wdget.yitsol.osm_app.presenter.impl.WorkListPresenter
import com.wdget.yitsol.osm_app.utils.AppConstant
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver
import com.wdget.yitsol.osm_app.utils.LinearLayoutSpaceItemDecoration
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity
import com.wdget.yitsol.osm_app.view.adapter.WorkListAdapter
import com.wdget.yitsol.osm_app.view.dialog.ReasonDialog
import kotlinx.android.synthetic.main.worklist.*

class WorkListActivity : BaseNavigationActivity() {

    private var container: FrameLayout? = null
    private var presenter: IWorkListPresenter? = null
    private var worklistadapter: WorkListAdapter? = null


    override fun initScreen() {
        container = getContainerLayout()
        container?.addView(inflater?.inflate(R.layout.worklist, null),
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        presenter = WorkListPresenter(this)
        val keyboard = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        keyboard.hideSoftInputFromInputMethod(edt_orderId.windowToken, 0)

        edt_orderId.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().length > 0) {
                    worklistadapter?.filter?.filter(s.toString().trim())
                } else {
                    worklistadapter?.filter?.filter(s.toString().trim())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
    }

    override fun getActContext(): Context {
        return this@WorkListActivity
    }

    override fun onStart() {
        super.onStart()
        presenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        presenter?.unSubscribeCallbacks()
    }

    override fun onResume() {
        super.onResume()

        val connectStatus = ConnectivityReceiver.isConnected()
        if (connectStatus) {
            presenter?.getProcessOfOrderCall(getActContext())
        } else {
            showCustomSnackBar(AppConstant.NO_CONNECTION, ll_worklist)
        }
    }

    override fun onPause() {
        super.onPause()
    }


    override fun onBackPressed() {
        val dashboardIntent = Intent(getActContext(), DashBoardAct::class.java)
        dashboardIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(dashboardIntent)
    }

    fun processOfOrderComplete(processOrderdatalist: List<ProcessOrderModel>) {
        val connectStatus = ConnectivityReceiver.isConnected()
        if (connectStatus) {
            presenter?.getAllWorkListDataCall(getActContext(), processOrderdatalist)
        } else {
            showCustomSnackBar(AppConstant.NO_CONNECTION, ll_worklist)
        }
    }

    fun workListOrderCallComplete(worklistdata: List<WorkListDataModel>) {

        rcly_worklist.visibility = View.VISIBLE
        txt_noordfound.visibility = View.GONE
        rcly_worklist.layoutManager = LinearLayoutManager(getActContext(), LinearLayoutManager.VERTICAL, false)
        rcly_worklist.addItemDecoration(LinearLayoutSpaceItemDecoration(5))
        worklistadapter = WorkListAdapter(getActContext(), worklistdata)
        worklistadapter?.registerItemClickListener(object : IWorkListItemClickListener {
            override fun showWorkList() {
                txt_noordfound.visibility=View.GONE
                rcly_worklist.visibility=View.VISIBLE
            }

            override fun hideWorkList() {
                txt_noordfound.visibility=View.VISIBLE
                rcly_worklist.visibility=View.GONE
            }

            override fun showSnackMessage(message: String?) {
                showCustomSnackBar(message!!,ll_worklist)
            }

            override fun OnSuspendOrder(orderId: String?, taskId: String) {
                showReasonDialog(AppConstant.ACTIONSUSPEND, orderId, taskId)
            }

            override fun OnCancelOrder(orderId: String?, taskId: String) {
                showReasonDialog(AppConstant.ACTIONCANCEL, orderId, taskId)
            }

            override fun OnResumeOrder(orderId: String?, taskId: String) {
                showReasonDialog(AppConstant.ACTIONRESUME, orderId, taskId)
            }

            override fun OnAbortOrder(orderId: String?, taskId: String) {
                showReasonDialog(AppConstant.ACTIONABORT, orderId, taskId)
            }

            override fun OnClickEditor(orderId: String, taskId: String, source: String, orderType: String, ref_no: String) {
                val editIntent = Intent(getActContext(), EditorActionActivity::class.java)
                editIntent.putExtra(AppConstant.ORDER_ID, orderId)
                editIntent.putExtra(AppConstant.TASK_ID, taskId)
                editIntent.putExtra(AppConstant.SOURCE, source)
                editIntent.putExtra(AppConstant.ORD_TYPE, orderType)
                editIntent.putExtra(AppConstant.REFERNCE, ref_no)
                //editIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                startActivity(editIntent)
            }

            override fun OnClickChangeStatus(orderId: String, taskId: String, ref_no: String) {
                val chngeStatusIntent = Intent(getActContext(), ChangeStatusActivity::class.java)
                chngeStatusIntent.putExtra(AppConstant.ORDER_ID, orderId)
                chngeStatusIntent.putExtra(AppConstant.TASK_ID, taskId)
                chngeStatusIntent.putExtra(AppConstant.REFERNCE, ref_no)
                //chngeStatusIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                startActivity(chngeStatusIntent)

            }

            override fun OnClickProcessHistory(orderId: String) {
                val procHistIntent = Intent(getActContext(), ProcessHistActivity::class.java)
                procHistIntent.putExtra(AppConstant.ORDER_ID, orderId)
                //procHistIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                startActivity(procHistIntent)
            }

        })
        rcly_worklist.adapter = worklistadapter

    }

    private fun showReasonDialog(actionsuspend: String, orderId: String?, taskId: String) {
        val reasonDialog = ReasonDialog(getActContext(), orderId, actionsuspend)
        reasonDialog.setCanceledOnTouchOutside(false)
        reasonDialog.registerClickListener(object : IReasonDialogListener {
            override fun onOkButtonClick(reason: String?, actiontype: String?) {

                presenter?.changeOrderCurrentState(getActContext(), reason, actiontype, orderId, taskId)
            }

            override fun showSnackMessage(message: String?) {
                showCustomSnackBar(message!!, ll_worklist)
            }

        })

        reasonDialog.show()
    }

    fun showNoOrderFound() {
        rcly_worklist.visibility = View.GONE
        txt_noordfound.visibility = View.VISIBLE
    }

    fun changeOrderStateComplete(reason: String, actiontype: String, orderId: String, taskId: String) {
        if (actiontype.equals(AppConstant.ACTIONSUSPEND)) {
            presenter?.changeOrderStatebyId(getActContext(), reason, actiontype, orderId, taskId)
        } else if (actiontype.equals(AppConstant.ACTIONCANCEL)) {
            presenter?.changeOrderStatebyId(getActContext(),reason,actiontype,orderId,taskId)
        }
        else if(actiontype.equals(AppConstant.ACTIONRESUME))
        {
            presenter?.changeOrderStatebyId(getActContext(),reason,actiontype,orderId,taskId)
        }
        else if(actiontype.equals(AppConstant.ACTIONABORT))
        {
            presenter?.changeOrderStatebyId(getActContext(),reason,actiontype,orderId,taskId)
        }
    }

    fun OrderStateChangeComplete()
    {
        val connectStatus = ConnectivityReceiver.isConnected()
        if (connectStatus) {
            presenter?.getProcessOfOrderCall(getActContext())
        } else {
            showCustomSnackBar(AppConstant.NO_CONNECTION, ll_worklist)
        }
    }


}
