package com.wdget.yitsol.osm_app.view.activity.base


import android.annotation.SuppressLint
import android.app.Dialog
import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat.startActivity
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBar
import android.support.v7.app.ActionBarDrawerToggle

import android.support.v7.widget.Toolbar
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.R.id.drawer_layout
import com.wdget.yitsol.osm_app.presenter.ILoginPresenter
import com.wdget.yitsol.osm_app.presenter.impl.LoginPresenter
import com.wdget.yitsol.osm_app.utils.CommonPopUp
import com.wdget.yitsol.osm_app.utils.ConnectionDetector
import com.wdget.yitsol.osm_app.utils.ShowHideKeyboard
import com.wdget.yitsol.osm_app.view.activity.*
import kotlinx.android.synthetic.main.activity_base_navigation.*


@Suppress("UNUSED_EXPRESSION")
abstract class BaseNavigationActivity : AppCompatActivity(),ShowHideKeyboard {

    var logoutPresenter: ILoginPresenter?=null
    private lateinit var mDrawerLayout: DrawerLayout
    var inflater: LayoutInflater? = null
    private var dialog: Dialog? = null
    private var animationDrawable: AnimationDrawable? = null
    private var connectionDetector: ConnectionDetector? = null

    protected abstract fun getActContext(): Context
    abstract fun initScreen()

    override fun showKeyBoard(context: Context?) {
        val imm = getActContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)

    }

    override fun hideKeyBoard(context: Context?) {
        val view = this.getCurrentFocus()
        if (view != null) {
            view.clearFocus();
            val imm = getActContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(0,InputMethodManager.HIDE_IMPLICIT_ONLY)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base_navigation)
        logoutPresenter= LoginPresenter(this)
        connectionDetector = ConnectionDetector(applicationContext)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        inflater= LayoutInflater.from(this)
        val actionbar: ActionBar? = supportActionBar

        actionbar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.mipmap.menu_icon_36)
        }
        mDrawerLayout = findViewById(R.id.drawer_layout)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.drawer_open,
                R.string.drawer_close)

        mDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView: NavigationView = findViewById(R.id.nvView)
        navigationView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true
            // close drawer when item is tapped
            mDrawerLayout.closeDrawers()

            navigationItemSelection(menuItem.itemId)

            // Add code here to update the UI based on the item selected
            // For example, swap UI fragments here

            true
        }

        mDrawerLayout.addDrawerListener(
                object : DrawerLayout.DrawerListener {
                    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
                        // Respond when the drawer's position changes
                    }

                    override fun onDrawerOpened(drawerView: View) {
                        // Respond when the drawer is opened
                    }

                    override fun onDrawerClosed(drawerView: View) {
                        // Respond when the drawer is closed
                    }

                    override fun onDrawerStateChanged(newState: Int) {
                        // Respond wgit hen the drawer motion state changes
                    }
                }
        )

        initScreen()
    }


    override fun onStart() {
        super.onStart()
        logoutPresenter?.subscribeCallbacks();
    }
    override fun onStop() {
        super.onStop()
        logoutPresenter?.unSubscribeCallbacks()
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                mDrawerLayout.openDrawer(GravityCompat.START)
                true
            }
            R.id.logout -> {
                logoutPresenter!!.performUserLogout(this@BaseNavigationActivity)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun navigationItemSelection(itemId: Int){
        if (!connectionDetector!!.isConnectedToInternet()) {
            val commonPopUp = CommonPopUp(
                    this, "Please activate internet...")
            commonPopUp.setCanceledOnTouchOutside(true)
            commonPopUp.show()
        } else {

            when(itemId){
                R.id.nav_reporting -> {
                    val intent = Intent(this,
                            ReportingActivity::class.java)
                    intent.putExtra("selected", "report")
                    intent.putExtra("flag", 1)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    startActivity(intent)
                    true
                }
                R.id.nav_notifications -> {
                    val intent = Intent(this,
                            NotificationsActivity::class.java)
                    intent.putExtra("selected", "notifications")
                    intent.putExtra("flag", 1)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    startActivity(intent)
                    true
                }
                R.id.nav_query -> {
                        val intent = Intent(this,
                                QueryActivity::class.java)
                        intent.putExtra("selected", "query")
                        intent.putExtra("flag", 1)
                        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                        startActivity(intent)
                    true
                }
                R.id.nav_work_list -> {
                    val intent = Intent(this,
                            WorkListActivity::class.java)
                    intent.putExtra("selected", "worklist")
                    intent.putExtra("flag", 1)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    startActivity(intent)
                    true
                }
            }

        }
    }
    fun showCustomSnackBar(message: String, layout: LinearLayout) {
        val mSnackBar = Snackbar.make(layout, message, Snackbar.LENGTH_LONG)
        val view = mSnackBar.view
        val params = view.layoutParams as FrameLayout.LayoutParams
        params.gravity = Gravity.BOTTOM
        view.layoutParams = params
        view.setBackgroundColor(Color.parseColor("#0C80FF"))
        val mainTextView = view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        mainTextView.setTextColor(Color.WHITE)
        mSnackBar.show()
    }

    fun toastMessage(message:String){
        Toast.makeText(applicationContext,message,Toast.LENGTH_SHORT).show()
    }

    fun getContainerLayout(): FrameLayout?
    {
        return this.content_frame
    }

    internal inner class Runloader(private val strrMsg: String) : Runnable {
        @SuppressLint("ResourceType")
        override fun run() {
            try {
                if (dialog == null) {
                    dialog = Dialog(getActContext(), R.style.Theme_Dialog_Translucent_Dialog)
                    dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog!!.getWindow()!!.setBackgroundDrawable(
                            ColorDrawable(
                                    Color.TRANSPARENT))
                }
                dialog!!.setContentView(R.layout.loading)
                dialog!!.setCancelable(false)
                if (dialog != null && dialog!!.isShowing()) {
                    dialog!!.dismiss()
                    dialog = null
                }
                if (dialog != null) {
                    dialog!!.show()
                }
                var imgeView: ImageView? = null
                if (dialog != null) {
                    imgeView = dialog!!.findViewById(R.id.imgeView)
                    val tvLoading = dialog!!.findViewById(R.id.tvLoading) as TextView
                    if (!strrMsg.equals("", ignoreCase = true))
                        tvLoading.text = strrMsg
                    imgeView!!.setBackgroundResource(R.anim.frame)
                    animationDrawable = imgeView.background as AnimationDrawable
                    imgeView.post {
                        if (animationDrawable != null)
                            animationDrawable!!.start()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun hideloader() {
        runOnUiThread(Runnable {
            try {
                if (dialog != null && dialog!!.isShowing())
                    dialog!!.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun showLoaderNew() {
        runOnUiThread(Runloader(getResources().getString(R.string.loading)))
    }

    fun logoutComplete(){
       goToLoginScreen()
    }

    private fun goToLoginScreen() {
        val dashboardIntent= Intent(this@BaseNavigationActivity, LoginActivity::class.java)
        dashboardIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(dashboardIntent)
    }

    fun showLoaderNew(msg: String) {
        runOnUiThread(Runloader(msg))
    }

    override fun onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


}
