package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EnvBody
{
    @SerializedName("SuspendOrderResponse")
    @Expose
    private SuspendOrderResponse suspendOrderResponse;
    @SerializedName("xmlns:env")
    @Expose
    private String xmlnsEnv;

    public SuspendOrderResponse getSuspendOrderResponse() {
        return suspendOrderResponse;
    }

    public void setSuspendOrderResponse(SuspendOrderResponse suspendOrderResponse) {
        this.suspendOrderResponse = suspendOrderResponse;
    }

    public String getXmlnsEnv() {
        return xmlnsEnv;
    }

    public void setXmlnsEnv(String xmlnsEnv) {
        this.xmlnsEnv = xmlnsEnv;
    }
}
