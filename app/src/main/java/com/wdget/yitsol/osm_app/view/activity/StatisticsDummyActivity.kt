package com.wdget.yitsol.osm_app.view.activity

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.util.Log
import android.view.ViewGroup
import android.widget.FrameLayout
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.OrdersReportResponse
import com.wdget.yitsol.osm_app.presenter.IReportingPresenter
import com.wdget.yitsol.osm_app.presenter.impl.ReportingPresenter
import com.wdget.yitsol.osm_app.utils.AppConstant
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity

import kotlin.collections.ArrayList

class StatisticsDummyActivity : BaseNavigationActivity() {


    private var container: FrameLayout?=null
    //private var mChart: GraphicalView? = null
    private var presenter: IReportingPresenter?=null
    private val connectStatus= ConnectivityReceiver.isConnected()
    var total: Int = 0
    var completed: Int = 0
    var failed: Int = 0
    var pending: Int = 0
    private var mChart: PieChart? = null
    protected var mParties = arrayOf("Completed","Pending","Failed")
    private var dataColorlist:MutableList<Int>?=null
    override fun getActContext(): Context {
        return this@StatisticsDummyActivity
    }

    override fun initScreen() {
        container = getContainerLayout()

        container!!.addView(inflater!!.inflate(R.layout.chart,null),
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        presenter = ReportingPresenter(this)

        addColorForReport()

        mChart = findViewById(R.id.chart1) as PieChart
        mChart!!.setUsePercentValues(true)
        mChart!!.getDescription().isEnabled = false
        mChart!!.setExtraOffsets(5f, 10f, 5f, 5f)

        mChart!!.setDragDecelerationFrictionCoef(0.95f)

       // mChart!!.setCenterTextTypeface(mTfLight)
        mChart!!.setCenterText(generateCenterSpannableText())

        mChart!!.setDrawHoleEnabled(true)
        mChart!!.setHoleColor(Color.WHITE)

        mChart!!.setTransparentCircleColor(Color.WHITE)
        mChart!!.setTransparentCircleAlpha(110)

        mChart!!.setHoleRadius(58f)
        mChart!!.setTransparentCircleRadius(61f)

        mChart!!.setDrawCenterText(true)

        mChart!!.setRotationAngle(0f)
        // enable rotation of the chart by touch
        mChart!!.setRotationEnabled(true)
        mChart!!.setHighlightPerTapEnabled(true)

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
     //   mChart!!.setOnChartValueSelectedListener(this)

       // setData(3, 100f)

        mChart!!.animateY(1400, Easing.EasingOption.EaseInOutQuad)
        // mChart.spin(2000, 0, 360);

        /*mSeekBarX.setOnSeekBarChangeListener(this)
        mSeekBarY.setOnSeekBarChangeListener(this)*/

        val l = mChart!!.getLegend()
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.textSize=10.0f
        l.setDrawInside(false)
        l.xEntrySpace = 3f
        l.yEntrySpace = 0f
        l.yOffset = 0f

        // entry label styling
        mChart!!.setEntryLabelColor(Color.WHITE)
     //   mChart!!.setEntryLabelTypeface(mTfRegular)
        mChart!!.setEntryLabelTextSize(12f)
    }

    private fun addColorForReport() {
        dataColorlist= ArrayList()
        (dataColorlist as ArrayList<Int>).add(R.color.md_green_800)
        (dataColorlist as ArrayList<Int>).add(R.color.md_amber_800)
        (dataColorlist as ArrayList<Int>).add(R.color.md_orange_900)
    }

    private fun generateCenterSpannableText(): SpannableString {

        val s = SpannableString("REPORT")
        s.setSpan(RelativeSizeSpan(1.5f), 0, 6, 0)
      /*  s.setSpan(StyleSpan(Typeface.NORMAL), 12, s.length - 7, 0)
        s.setSpan(ForegroundColorSpan(Color.GRAY), 12, s.length - 7, 0)
        s.setSpan(RelativeSizeSpan(.8f), 12, s.length - 7, 0)
        s.setSpan(StyleSpan(Typeface.ITALIC), s.length - 12, s.length, 0)
        s.setSpan(ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length - 26, s.length, 0)*/
        return s
    }

    private fun setData(count: Int, list: ArrayList<Int>) {

        val entries = ArrayList<PieEntry>()

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
       for (i in 0 until count) {
            entries.add(PieEntry((list[i]*100).div(total).toFloat(),
                    mParties[i % mParties.size]+"("+list[i]+")",
                    ContextCompat.getDrawable(getActContext(),R.drawable.star)))
        }

       /* entries.add(PieEntry(completed.toFloat(),"Completed",
                resources.getDrawable(R.drawable.star)))
        entries.add(PieEntry(pending.toFloat(),"Pending",
                resources.getDrawable(R.drawable.star)))
        entries.add(PieEntry(failed.toFloat(),"Failed",
                resources.getDrawable(R.drawable.star)))*/
        val dataSet = PieDataSet(entries, "REPORT")

        dataSet.setDrawIcons(false)

        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f

        // add a lot of colors

        val colors = ArrayList<Int>()

        /*for (c in dataColorlist!!)
            colors.add(c)*/
        colors.add(ColorTemplate.rgb("#2E7D32"))
        colors.add(ColorTemplate.rgb("#FF8F00"))
        colors.add(ColorTemplate.rgb("#E65100"))

        /*for (c in ColorTemplate.JOYFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.COLORFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.LIBERTY_COLORS)
            colors.add(c)

        for (c in ColorTemplate.PASTEL_COLORS)
            colors.add(c)*/

        colors.add(ColorTemplate.getHoloBlue())

        dataSet.colors = colors
        //dataSet.setSelectionShift(0f);

        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter())
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.WHITE)
        //data.setValueTypeface(mTfLight)
        mChart!!.setData(data)

        // undo all highlights
        mChart!!.highlightValues(null)

        mChart!!.invalidate()
    }
    override fun onResume() {
        super.onResume()
        if(connectStatus)
        {
            presenter?.getOrdersReport(getActContext())
        }
        else
        {
            toastMessage(AppConstant.NO_CONNECTION)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        presenter?.unSubscribeCallbacks()
    }

    public fun onOrderReportingSuccess(ordersReportResponseList:  MutableList<OrdersReportResponse>){
        getDifferentReports(ordersReportResponseList)
    }
    private fun getDifferentReports(ordersReportingList: MutableList<OrdersReportResponse>?){


        var arrayList: ArrayList<Int> = arrayListOf()
        for (item: OrdersReportResponse in ordersReportingList!!){
            total = total.plus(item.total.toInt())
            Log.e(item.mnemonic,item.total)
            if(item.mnemonic.equals("not_started") || item.mnemonic.equals("suspended") || item.mnemonic.equals("in_progress")||
                    item.mnemonic.equals("cancelling")|| item.mnemonic.equals("amending")||
                    item.mnemonic.equals("waiting_for_revision")){

                pending = pending.plus(item.total.toInt())
            }

            if(item.mnemonic.equals("cancelled") || item.mnemonic.equals("completed") || item.mnemonic.equals("aborted")){

                completed = completed.plus(item.total.toInt())
            }

            if(item.mnemonic.equals("failed")){
                failed = failed.plus(item.total.toInt())
            }
        }

        arrayList.add(completed)
        arrayList.add(pending)
        arrayList.add(failed)
        setData(3, arrayList)
       // openChart()
    }
}