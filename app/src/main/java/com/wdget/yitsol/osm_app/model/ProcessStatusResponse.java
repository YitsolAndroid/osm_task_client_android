package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessStatusResponse {
    @SerializedName("processId")
    @Expose
    public String processId;

    @SerializedName("processIdDesc")
    @Expose
    public String processIdDesc;

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessIdDesc() {
        return processIdDesc;
    }

    public void setProcessIdDesc(String processIdDesc) {
        this.processIdDesc = processIdDesc;
    }
}
