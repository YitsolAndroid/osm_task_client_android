package com.wdget.yitsol.osm_app.repository.impl;

import android.content.Context;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.wdget.yitsol.osm_app.model.AssignOrdInputModel;
import com.wdget.yitsol.osm_app.model.AssignOrdOutputModel;
import com.wdget.yitsol.osm_app.model.ChangeOrderInputModel;
import com.wdget.yitsol.osm_app.model.CompleteOrdInputModel;
import com.wdget.yitsol.osm_app.model.CompleteOrdOutputModel;
import com.wdget.yitsol.osm_app.model.GetOrderByIdModel;
import com.wdget.yitsol.osm_app.model.OrderStatusInputModel;
import com.wdget.yitsol.osm_app.model.OrderStatusModel;
import com.wdget.yitsol.osm_app.model.ReceivedOrdInputModel;
import com.wdget.yitsol.osm_app.model.ReceivedOrdOutputModel;
import com.wdget.yitsol.osm_app.repository.IChangeStatusRepository;
import com.wdget.yitsol.osm_app.service.APIService;
import com.wdget.yitsol.osm_app.service.APIUtils;
import com.wdget.yitsol.osm_app.utils.AppConstant;
import com.wdget.yitsol.osm_app.utils.AppPreference;
import com.wdget.yitsol.osm_app.utils.Encryption;
import com.wdget.yitsol.osm_app.utils.GetListStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeStatusRepository implements IChangeStatusRepository {
    APIService apiService;

    public ChangeStatusRepository() {
        apiService = APIUtils.getAPIService();
    }


    @Override
    public void getAllStatusForOrder(Context context, String orderId, String taskId, final OnGetAllOrderStatusCallback callback) {
        try {
            AppPreference appPreference = new AppPreference(context);
            HashMap<String, String> userdetails = appPreference.getUserDetails();
            String username = userdetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password = userdetails.get(appPreference.KEY_USERLOGINPASSWORD);
            Encryption encryption = new Encryption();
            String encodedInput = encryption.encrypt(username + ":" + password);
            Map<String, String> encodeheader = new HashMap<>();
            encodeheader.put("Auth", encodedInput);
            OrderStatusInputModel inputModel = new OrderStatusInputModel(username, password, orderId, AppConstant.PUBLIC_IP
                    , AppConstant.PORT, taskId);
            apiService.getAllStatusForOrder(encodeheader, inputModel).enqueue(new Callback<OrderStatusModel>() {
                @Override
                public void onResponse(Call<OrderStatusModel> call, Response<OrderStatusModel> response) {
                    if (callback != null) {
                        if (response.code() == HttpURLConnection.HTTP_OK) {
                            OrderStatusModel statusModel = response.body();
                            List<String> orderStatuslist = new ArrayList<>();
                            List<String> orderTaskList = new ArrayList<>();
                            String jsonresponse = new GsonBuilder().setPrettyPrinting().create().toJson(response.body());
                            //String XML_DATA=new JsonToXml.Builder(jsonresponse).build().toFormattedString();
                            getJSONObjectKey(jsonresponse, orderStatuslist, orderTaskList, callback);
                            //getDataFromXMLParsing(XML_DATA,orderStatuslist,orderTaskList,callback);
                        }
                    }
                }

                @Override
                public void onFailure(Call<OrderStatusModel> call, Throwable t) {
                    if (callback != null) {
                        call.cancel();
                        callback.onError("server error");
                    }
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void changeOrderStatus(Context context, String orderId, String taskId, String select_task, String ord_status, final OnChangeOrderStatusCallback callback) {
        try {
            AppPreference appPreference = new AppPreference(context);
            HashMap<String, String> logindetails = appPreference.getUserDetails();
            String username = logindetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password = logindetails.get(appPreference.KEY_USERLOGINPASSWORD);
            String encoded_String = new Encryption().encrypt(username + ":" + password);
            Map<String, String> encoded_header = new HashMap<>();
            encoded_header.put("Auth", encoded_String);
            if (ord_status.equals(AppConstant.STATEACCEPTED)) {
                ChangeOrderInputModel inputModel = new ChangeOrderInputModel(username, password, orderId,
                        AppConstant.PUBLIC_IP, AppConstant.PORT, "true", taskId, "");
                apiService.changeOrderStatusById(encoded_header, inputModel).enqueue(new Callback<GetOrderByIdModel>() {
                    @Override
                    public void onResponse(Call<GetOrderByIdModel> call, Response<GetOrderByIdModel> response) {
                        if (callback != null) {
                            if (response.code() == HttpURLConnection.HTTP_OK) {

                                /*String respString = null;
                                if (response.body() != null) {
                                    respString = response.body().toString();
                                }
                                try {
                                    JSONObject requestResult = new JSONObject(respString);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }*/
                                callback.onSuccess();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<GetOrderByIdModel> call, Throwable t) {
                        if (callback != null) {
                            call.cancel();
                            callback.onError("request cancel");
                        }
                    }
                });
            } else if (ord_status.equals(AppConstant.STATECOMPLETED)) {
                CompleteOrdInputModel inputModel = new CompleteOrdInputModel(username, password, orderId, AppConstant.PUBLIC_IP
                        , AppConstant.PORT, select_task, taskId);
                apiService.completeOrdStatusChange(encoded_header, inputModel).enqueue(new Callback<CompleteOrdOutputModel>() {
                    @Override
                    public void onResponse(Call<CompleteOrdOutputModel> call, Response<CompleteOrdOutputModel> response) {
                        if (callback != null) {
                            if (response.code() == HttpURLConnection.HTTP_OK) {
                                callback.onSuccess();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CompleteOrdOutputModel> call, Throwable t) {
                        if (callback != null) {
                            call.cancel();
                            callback.onError("request cancel");
                        }
                    }
                });
            } else if (ord_status.equals(AppConstant.STATERECEIVED)) {
                ReceivedOrdInputModel inputModel = new ReceivedOrdInputModel(username, password, orderId, AppConstant.PUBLIC_IP, AppConstant.PORT
                        , taskId);
                apiService.receivedOrdStatusChange(encoded_header, inputModel).enqueue(new Callback<ReceivedOrdOutputModel>() {
                    @Override
                    public void onResponse(Call<ReceivedOrdOutputModel> call, Response<ReceivedOrdOutputModel> response) {
                        if (callback != null) {
                            if (response.code() == HttpURLConnection.HTTP_OK) {
                                callback.onSuccess();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ReceivedOrdOutputModel> call, Throwable t) {
                        if (callback != null) {
                            call.cancel();
                            callback.onError("request cancel");
                        }
                    }
                });
            } else if (ord_status.equals(AppConstant.STATEASSIGNED)) {
                AssignOrdInputModel inputModel = new AssignOrdInputModel(username, password, orderId, AppConstant.PUBLIC_IP, AppConstant.PORT
                        , "", taskId);
                apiService.assignOrdStatusChange(encoded_header, inputModel).enqueue(new Callback<AssignOrdOutputModel>() {
                    @Override
                    public void onResponse(Call<AssignOrdOutputModel> call, Response<AssignOrdOutputModel> response) {
                        if (callback != null) {
                            if (response.code() == HttpURLConnection.HTTP_OK) {
                                callback.onSuccess();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AssignOrdOutputModel> call, Throwable t) {
                        if (callback != null) {
                            call.cancel();
                            callback.onError("server error");
                        }
                    }
                });
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void getDataFromXMLParsing(String xml_data, List<String> orderStatuslist, List<String> orderTaskList, OnGetAllOrderStatusCallback callback) {
        GetListStatus getListStatus = new GetListStatus();
        orderStatuslist = getListStatus.getStatus(xml_data);
        if (callback != null) {
            //callback.onSuccess(orderStatuslist, orderTaskList, orderTaskStatuslist);
        }
    }

    private void getJSONObjectKey(String jsonresponse, List<String> orderStatuslist, List<String> orderTaskList,
                                  OnGetAllOrderStatusCallback callback) {
        try {
            Map<String, List<String>> ordstateMap = new HashMap<>();
            List<String> orderTaskStatuslist = new ArrayList<>();
            JSONObject jobj_resp = new JSONObject(jsonresponse);
            if (jobj_resp.has("ListStatesNStatuses.Response")) {
                JSONObject listStatusjobj = jobj_resp.getJSONObject("ListStatesNStatuses.Response");
                if (listStatusjobj.has("TaskStatesNStatuses")) {
                    JSONObject taskstateJobj = listStatusjobj.getJSONObject("TaskStatesNStatuses");
                    if (taskstateJobj != null && taskstateJobj.length() > 0) {
                        Iterator<String> nameit = taskstateJobj.keys();
                        while (nameit.hasNext()) {
                            String ordStatus = (String) nameit.next();
                            orderStatuslist.add(ordStatus);
                            orderTaskStatuslist.add(ordStatus);
                            if (taskstateJobj.has(ordStatus)) {
                                if (taskstateJobj.get(ordStatus) instanceof JSONObject) {
                                    orderTaskList = new ArrayList<>();
                                    JSONObject statusJobj = taskstateJobj.getJSONObject(ordStatus);
                                    if (statusJobj != null && statusJobj.length() > 0) {
                                        Iterator<String> taskit = statusJobj.keys();
                                        while (taskit.hasNext()) {

                                            String taskName = (String) taskit.next();
                                            if (statusJobj.getString(taskName).length() > 0) {
                                                if(statusJobj.get(taskName) instanceof JSONArray)
                                                {
                                                    String userlistString=statusJobj.getString(taskName);
                                                    JSONArray userlistJarry=new JSONArray(userlistString);
                                                    for(int i=0;i<userlistJarry.length();i++)
                                                    {
                                                        orderTaskList.add(userlistJarry.getString(i));
                                                    }
                                                }

                                            } else {
                                                orderTaskList.add(taskName);
                                            }

                                        }
                                        ordstateMap.put(ordStatus, orderTaskList);
                                    }
                                }
                            }
                        }
                        orderStatuslist.add(0, "Select Order Status");
                        callback.onSuccess(orderStatuslist, ordstateMap);
                    } else {
                        callback.onError("No OrdState/OrdStatus Found");
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
