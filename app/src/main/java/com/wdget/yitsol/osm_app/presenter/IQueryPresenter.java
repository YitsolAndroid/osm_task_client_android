package com.wdget.yitsol.osm_app.presenter;

import android.content.Context;
import android.widget.Spinner;

import com.wdget.yitsol.osm_app.presenter.base.IBasePresenter;

public interface IQueryPresenter extends IBasePresenter {

    void getQueryDataCall(Context context);

    void searchQuery(Context context,String orderId,String orderState,String nameSpace,String state);
}
