package com.wdget.yitsol.osm_app.presenter;

public interface ConnectivityReceiverListener
{
    void onNetworkConnectionChanged(boolean isConnected);
}
