package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProcessOrderModel
{
    @SerializedName("processIdDescription")
    @Expose
    private String processIdDescription;
    @SerializedName("orderSeqId")
    @Expose
    private String orderSeqId;

    public String getProcessIdDescription() {
        return processIdDescription;
    }

    public void setProcessIdDescription(String processIdDescription) {
        this.processIdDescription = processIdDescription;
    }

    public String getOrderSeqId() {
        return orderSeqId;
    }

    public void setOrderSeqId(String orderSeqId) {
        this.orderSeqId = orderSeqId;
    }
}
