package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginInput
{
    @SerializedName("publicIP")
    @Expose
    private String publicIP;
    @SerializedName("port")
    @Expose
    private String port;

    public LoginInput(String publicIP, String port) {
        this.publicIP = publicIP;
        this.port = port;
    }

    public String getPublicIP() {
        return publicIP;
    }

    public void setPublicIP(String publicIP) {
        this.publicIP = publicIP;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
