package com.wdget.yitsol.osm_app.presenter;

import com.wdget.yitsol.osm_app.presenter.base.IBasePresenter;

public interface ISplashPresenter extends IBasePresenter
{
    void checkInternetConnection();
}
