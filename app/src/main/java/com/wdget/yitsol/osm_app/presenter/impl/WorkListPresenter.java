package com.wdget.yitsol.osm_app.presenter.impl;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.ProcessOrderModel;
import com.wdget.yitsol.osm_app.model.WorkListDataModel;
import com.wdget.yitsol.osm_app.model.WorkListModel;
import com.wdget.yitsol.osm_app.model.WorkListOrderdata;
import com.wdget.yitsol.osm_app.model.WorkListResultModel;
import com.wdget.yitsol.osm_app.presenter.IWorkListPresenter;
import com.wdget.yitsol.osm_app.repository.IWorkListRepository;
import com.wdget.yitsol.osm_app.repository.impl.WorkListRepository;
import com.wdget.yitsol.osm_app.view.activity.QueryOrderListActivity;
import com.wdget.yitsol.osm_app.view.activity.WorkListActivity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class WorkListPresenter implements IWorkListPresenter
{
    private WorkListActivity view;
    private QueryOrderListActivity queryview;
    private IWorkListRepository repository;
    private IWorkListRepository.OnProcessOfOrderCallback orderCallback;
    private IWorkListRepository.OnGetAllWorkListCallback workListCallback;
    private IWorkListRepository.OnOrderCurrentStateCallback currentStateCallback;
    private IWorkListRepository.OnChangeOrderStateByIdCallback orderStateByIdCallback;

    public WorkListPresenter(WorkListActivity view) {
        this.view = view;
        repository=new WorkListRepository();
    }

    public WorkListPresenter(@NotNull QueryOrderListActivity view) {
        this.queryview=view;
        repository=new WorkListRepository();
    }

    @Override
    public void getProcessOfOrderCall(Context context) {
        try {
            if(view!=null)
            {
                view.showLoaderNew();
                repository.getProcessOfOrderCall(context,orderCallback);
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void getAllWorkListDataCall(Context context, List<ProcessOrderModel> processOrderdatalist) {
        try {
                repository.getAllWorkListDataCall(context,processOrderdatalist,workListCallback);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void changeOrderCurrentState(Context context,@Nullable String reason, @Nullable String actiontype,String orderId,String taskId) {
        try {
            if(view!=null)
            {
                view.showLoaderNew();
                repository.changeOrderCurrentState(context,reason,actiontype,orderId,taskId,currentStateCallback);
            }
            else
            {
                queryview.showLoaderNew();
                repository.changeOrderCurrentState(context,reason,actiontype,orderId,taskId,currentStateCallback);
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void changeOrderStatebyId(Context context,@NotNull String reason, @NotNull String actiontype, @NotNull String orderId, @NotNull String taskId) {
        try {
            if(view!=null)
            {
                view.showLoaderNew();
                repository.changeOrderStatebyId(context,reason,actiontype,orderId,taskId,orderStateByIdCallback);
            }
            else
            {
                queryview.showLoaderNew();
                repository.changeOrderStatebyId(context,reason,actiontype,orderId,taskId,orderStateByIdCallback);
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void subscribeCallbacks() {
        orderCallback=new IWorkListRepository.OnProcessOfOrderCallback() {
            @Override
            public void onSuccess(List<ProcessOrderModel> processOrderdatalist) {
                view.hideloader();
                view.processOfOrderComplete(processOrderdatalist);
            }

            @Override
            public void onError(String message) {
                view.hideloader();
            }
        };

        workListCallback=new IWorkListRepository.OnGetAllWorkListCallback() {
            @Override
            public void onSuccess(WorkListResultModel listModel, List<ProcessOrderModel> processOrderdatalist, List<WorkListDataModel> worklistdata) {
                view.hideloader();
                view.workListOrderCallComplete(worklistdata);
            }

            @Override
            public void onError(String message) {
                view.hideloader();
                view.showMessage(message);
                view.showNoOrderFound();
            }
        };

        currentStateCallback=new IWorkListRepository.OnOrderCurrentStateCallback() {
            @Override
            public void onSuccess(String reason, String actiontype, String orderId, String taskId) {
                if(view!=null)
                {
                    view.hideloader();
                    view.changeOrderStateComplete(reason,actiontype,orderId,taskId);
                }
                else
                {
                    queryview.hideloader();
                    queryview.changeOrderStateComplete(reason,actiontype,orderId,taskId);
                }

            }

            @Override
            public void onError(String message) {
                if(view!=null)
                {
                    view.hideloader();
                    view.showMessage(message);
                }
                else
                {
                    queryview.hideloader();
                    queryview.showMessage(message);
                }

            }
        };

        orderStateByIdCallback=new IWorkListRepository.OnChangeOrderStateByIdCallback() {
            @Override
            public void onSuccess() {
                if(view!=null)
                {
                    view.hideloader();
                    view.OrderStateChangeComplete();
                }
                else
                {
                    queryview.hideloader();
                    queryview.OrderStateChangeComplete();
                }

            }

            @Override
            public void onError(String message) {
                if(view!=null)
                {
                    view.hideloader();
                    view.showMessage(message);
                }
                else
                {
                    queryview.hideloader();
                    queryview.showMessage(message);
                }

            }
        };
    }

    @Override
    public void unSubscribeCallbacks() {
        orderCallback=null;
        workListCallback=null;
        currentStateCallback=null;
        orderStateByIdCallback=null;
    }
}
