package com.wdget.yitsol.osm_app.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.OrdersReportResponse
import com.wdget.yitsol.osm_app.model.QueryOutputModel
import com.wdget.yitsol.osm_app.presenter.IOrdersReportingItemClickListener
import com.wdget.yitsol.osm_app.presenter.IReportingPresenter
import com.wdget.yitsol.osm_app.presenter.impl.ReportingPresenter
import com.wdget.yitsol.osm_app.utils.AppConstant
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver
import com.wdget.yitsol.osm_app.utils.LinearLayoutSpaceItemDecoration
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity
import com.wdget.yitsol.osm_app.view.adapter.OrderReportingAdapter
import com.wdget.yitsol.osm_app.view.adapter.OrderStateStatusAdapter
import kotlinx.android.synthetic.main.change_status.*
import kotlinx.android.synthetic.main.orders.*
import kotlinx.android.synthetic.main.query.*
import kotlinx.android.synthetic.main.reporting.*
import java.util.ArrayList

class OrdersReportingActivity: BaseNavigationActivity(),View.OnClickListener {


    private var container: FrameLayout ?=null
    private var presenter: IReportingPresenter?=null
    private val connectStatus= ConnectivityReceiver.isConnected()
    var ordersReportingList: MutableList<OrdersReportResponse>?= null
    private var orderStateLatest = AppConstant.NOT_AVAILABLE
    var totalList: MutableList<OrdersReportResponse> = arrayListOf()
    var completedList: MutableList<OrdersReportResponse> = arrayListOf()
    var pendingList: MutableList<OrdersReportResponse> = arrayListOf()
    var failedList: MutableList<OrdersReportResponse> = arrayListOf()
    var total: Int = 0
    var completed: Int = 0
    var failed: Int = 0
    var pending: Int = 0

    private var adapter:OrderReportingAdapter?=null
    override fun getActContext(): Context {
        return this@OrdersReportingActivity
    }

    override fun initScreen() {
        container = getContainerLayout()
        container?.addView(inflater?.inflate(R.layout.orders,null),
                ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT)

        presenter = ReportingPresenter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lst_orders.layoutManager = LinearLayoutManager(getActContext(), LinearLayoutManager.VERTICAL, false)
        //lst_orders.addItemDecoration(LinearLayoutSpaceItemDecoration(1))

        ll_Total.setOnClickListener(this)
        ll_Complete.setOnClickListener(this)
        ll_Pending.setOnClickListener(this)
        ll_Failed.setOnClickListener(this)

    }

    override fun onResume() {
        super.onResume()
        if(connectStatus)
        {
            presenter?.getOrdersReport(getActContext())
        }
        else
        {
            toastMessage(AppConstant.NO_CONNECTION)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter?.subscribeCallbacks()
    }

    override fun onStop() {
        super.onStop()
        presenter?.unSubscribeCallbacks()
    }

    override fun onClick(view: View?) {

        when(view!!.id){
            R.id.ll_Total -> switchOrderTypes(ordersReportingList)
            R.id.ll_Complete -> switchOrderTypes(completedList)
            R.id.ll_Pending ->  switchOrderTypes(pendingList)
            R.id.ll_Failed -> switchOrderTypes(failedList)
        }
    }

    private fun switchOrderTypes(ordersReportingList: MutableList<OrdersReportResponse>?){
        adapter = OrderReportingAdapter(this,ordersReportingList)
        lst_orders.adapter = adapter
        adapter?.registerItemClickListener(object : IOrdersReportingItemClickListener {
            override fun onItemClicked(orderState: String?) {

                if(orderState.equals("no_state")||orderState.equals("not_started")||orderState.equals("suspended")||
                        orderState.equals("cancelled")||orderState.equals("cancelling")||orderState.equals("waiting_for_revision")||
                        orderState.equals("aborted")||orderState.equals("failed"))
                {
                    orderStateLatest = StringBuilder().append("open.not_running.").append(orderState).toString()
                }
                else
                {
                    orderStateLatest = StringBuilder().append("open.running.").append(orderState).toString()
                }

                if(connectStatus)
                {
                    presenter?.searchQueryByOrderState(getActContext(),orderStateLatest)
                }
                else
                {
                    showCustomSnackBar(AppConstant.NO_CONNECTION,ll_query)
                }
            }

        })
    }
    public fun onOrderReportingSuccess(ordersReportResponseList:  MutableList<OrdersReportResponse>){
        ordersReportingList = ordersReportResponseList
        getDifferentReports(ordersReportResponseList)
        adapter = OrderReportingAdapter(this,ordersReportResponseList)
        lst_orders.adapter = adapter
        adapter?.registerItemClickListener(object : IOrdersReportingItemClickListener {
            override fun onItemClicked(orderState: String?) {

                if(orderState.equals("no_state")||orderState.equals("not_started")||orderState.equals("suspended")||
                        orderState.equals("cancelled")||orderState.equals("cancelling")||orderState.equals("waiting_for_revision")||
                        orderState.equals("aborted")||orderState.equals("failed"))
                {
                    orderStateLatest = StringBuilder().append("open.not_running.").append(orderState).toString()
                }
                else
                {
                    orderStateLatest = StringBuilder().append("open.running.").append(orderState).toString()
                }

                if(connectStatus)
                {
                    presenter?.searchQueryByOrderState(getActContext(),orderStateLatest)
                }
                else
                {
                    showCustomSnackBar(AppConstant.NO_CONNECTION,ll_query)
                }
            }

        })
    }

    private fun getDifferentReports(ordersReportingList: MutableList<OrdersReportResponse>?){

        totalList.clear()
        completedList.clear()
        pendingList.clear()
        failedList.clear()

        for (item: OrdersReportResponse in ordersReportingList!!){
            totalList.add(item)
            total = total.plus(item.total.toInt())
            Log.e(item.mnemonic,item.total)
            if(item.mnemonic.equals("not_started") || item.mnemonic.equals("suspended") || item.mnemonic.equals("in_progress")||
                    item.mnemonic.equals("cancelling")|| item.mnemonic.equals("amending")||
                    item.mnemonic.equals("waiting_for_revision")){

                pending = pending.plus(item.total.toInt())
                pendingList.add(item)
            }

            if(item.mnemonic.equals("completed")){

                completed = completed.plus(item.total.toInt())
                completedList.add(item)
            }

            if(item.mnemonic.equals("failed")){
                failed = failed.plus(item.total.toInt())
                failedList.add(item)
            }
        }
    }

    fun showQueryOrderList(datalist: MutableList<QueryOutputModel>)
    {
        var querydatalist=ArrayList<QueryOutputModel>()
        for(outputmodel: QueryOutputModel in datalist)
        {
            querydatalist.add(outputmodel)
        }
        val queryorderIntent=Intent(getActContext(),QueryOrderListActivity::class.java)
        val bundle=Bundle()
        bundle.putString(AppConstant.FROM_CLASS,AppConstant.REPORTCLASS)
        bundle.putParcelableArrayList(AppConstant.QUERYORDERLIST,querydatalist)
        queryorderIntent.putExtras(bundle)
        queryorderIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(queryorderIntent)
    }
}