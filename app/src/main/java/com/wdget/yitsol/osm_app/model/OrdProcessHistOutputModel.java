package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrdProcessHistOutputModel
{
    @SerializedName("GetOrderProcessHistory.Response")
    @Expose
    private GetOrderProcessHistoryResponse getOrderProcessHistoryResponse;

    public GetOrderProcessHistoryResponse getGetOrderProcessHistoryResponse() {
        return getOrderProcessHistoryResponse;
    }

    public void setGetOrderProcessHistoryResponse(GetOrderProcessHistoryResponse getOrderProcessHistoryResponse) {
        this.getOrderProcessHistoryResponse = getOrderProcessHistoryResponse;
    }
}
