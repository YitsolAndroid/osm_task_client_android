package com.wdget.yitsol.osm_app.model;

import java.util.Comparator;

public class WorkListDataModel
{
    private String orderId;
    private String nameSpace;
    private String orderType;
    private String orderState;
    private String process;
    private String ord_hist_seq_id;
    private String ref_no;
    private String version;
    private String current_order_state;
    private String user;
    private String taskId;
    private String source;
    private String executionMode;
    private String num_remarks;
    private String priority;
    private boolean optionStatus;

   /* public WorkListDataModel() {
    }
*/

    public WorkListDataModel(String orderId, String nameSpace, String orderType, String orderState, String process, String ord_hist_seq_id, String ref_no, String version, String current_order_state, String user, String taskId, String source, String executionMode, String num_remarks, String priority) {
        this.orderId = orderId;
        this.nameSpace = nameSpace;
        this.orderType = orderType;
        this.orderState = orderState;
        this.process = process;
        this.ord_hist_seq_id = ord_hist_seq_id;
        this.ref_no = ref_no;
        this.version = version;
        this.current_order_state = current_order_state;
        this.user = user;
        this.taskId = taskId;
        this.source = source;
        this.executionMode = executionMode;
        this.num_remarks = num_remarks;
        this.priority = priority;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getNameSpace() {
        return nameSpace;
    }

    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getOrd_hist_seq_id() {
        return ord_hist_seq_id;
    }

    public void setOrd_hist_seq_id(String ord_hist_seq_id) {
        this.ord_hist_seq_id = ord_hist_seq_id;
    }

    public String getRef_no() {
        return ref_no;
    }

    public void setRef_no(String ref_no) {
        this.ref_no = ref_no;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCurrent_order_state() {
        return current_order_state;
    }

    public void setCurrent_order_state(String current_order_state) {
        this.current_order_state = current_order_state;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getExecutionMode() {
        return executionMode;
    }

    public void setExecutionMode(String executionMode) {
        this.executionMode = executionMode;
    }

    public String getNum_remarks() {
        return num_remarks;
    }

    public void setNum_remarks(String num_remarks) {
        this.num_remarks = num_remarks;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public boolean isOptionStatus() {
        return optionStatus;
    }

    public void setOptionStatus(boolean optionStatus) {
        this.optionStatus = optionStatus;
    }

}
