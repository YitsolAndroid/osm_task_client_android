package com.wdget.yitsol.osm_app.repository;

import android.content.Context;

import com.wdget.yitsol.osm_app.model.QueryDataModel;
import com.wdget.yitsol.osm_app.model.QueryOutputModel;

import java.util.List;

public interface IQueryRepository {

    interface OnQueryDataCallback{

        void onSuccess(QueryDataModel queryDataModel);

        void onError(String message);
    }

    interface OnOrderQueryCallback
    {
        void onSuccess(List<QueryOutputModel> datalist);
        void onError(String message);
    }

    void getQueryDataCall(Context context, OnQueryDataCallback callback);

    void searchQuery(Context context,String orderId,String orderState,String nameSpace,String state,OnOrderQueryCallback callback);
}
