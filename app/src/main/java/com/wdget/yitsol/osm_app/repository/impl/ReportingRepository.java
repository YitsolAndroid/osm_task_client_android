package com.wdget.yitsol.osm_app.repository.impl;

import android.content.Context;
import android.util.Log;

import com.wdget.yitsol.osm_app.model.OrdersReportResponse;
import com.wdget.yitsol.osm_app.model.QueryInputModel;
import com.wdget.yitsol.osm_app.model.QueryOutputModel;
import com.wdget.yitsol.osm_app.model.SearchQueryByOrderStateModel;
import com.wdget.yitsol.osm_app.repository.IReportingRepository;
import com.wdget.yitsol.osm_app.service.APIService;
import com.wdget.yitsol.osm_app.service.APIUtils;
import com.wdget.yitsol.osm_app.utils.AppConstant;
import com.wdget.yitsol.osm_app.utils.AppPreference;
import com.wdget.yitsol.osm_app.utils.Encryption;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportingRepository implements IReportingRepository {

    APIService apiService;

    public ReportingRepository(){
        apiService = APIUtils.getAPIService();
    }


    @Override
    public void getOrdersReport(Context context, final OnOrdersReportingCallBack callBack) {
        try {
            AppPreference appPreference = new AppPreference(context);
            HashMap<String, String> logindetails = appPreference.getUserDetails();
            String username = logindetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password = logindetails.get(appPreference.KEY_USERLOGINPASSWORD);
            Encryption encryption = new Encryption();
            String encodeInput = encryption.encrypt(username + ":" + password);
            Map<String, String> encodedheader = new HashMap<>();
            encodedheader.put("Auth", encodeInput);

            apiService.getOrdersReport(encodedheader).enqueue(new Callback<List<OrdersReportResponse>>() {
                @Override
                public void onResponse(Call<List<OrdersReportResponse>> call, Response<List<OrdersReportResponse>> response) {
                    Log.e("Responsesss  ",response.body().get(0).getMnemonic().toString());
                    callBack.onSuccess(response.body());
                }

                @Override
                public void onFailure(Call<List<OrdersReportResponse>> call, Throwable t) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void searchQueryByOrderState(Context context, String orderState, final OnSearchQueryByOrderStateCallback callBack) {

        try {
            SearchQueryByOrderStateModel inputModel = null;
            AppPreference appPreference = new AppPreference(context);
            HashMap<String, String> logindetails = appPreference.getUserDetails();
            String username = logindetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password = logindetails.get(appPreference.KEY_USERLOGINPASSWORD);
            Encryption encryption = new Encryption();
            String encodeInput = encryption.encrypt(username + ":" + password);
            Map<String, String> encodedheader = new HashMap<>();
            encodedheader.put("Auth", encodeInput);

            inputModel=generateJsonObject(username,password,orderState);

            apiService.searchQueryByOrderState(encodedheader,inputModel).enqueue(new Callback<List<QueryOutputModel>>() {
                @Override
                public void onResponse(Call<List<QueryOutputModel>> call, Response<List<QueryOutputModel>> response) {
                    if(callBack!=null)
                    {
                        if(response.code()== HttpURLConnection.HTTP_OK)
                        {
                            List<QueryOutputModel> datalist=response.body();
                            if (datalist != null && datalist.size() > 0) {
                                callBack.onSuccess(datalist);

                            }
                            else
                            {
                                callBack.onError("NO ORDER FOUND");
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<QueryOutputModel>> call, Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private SearchQueryByOrderStateModel generateJsonObject(String username, String password, String orderState ) {
        return new SearchQueryByOrderStateModel(username,password, AppConstant.PUBLIC_IP,AppConstant.PORT,orderState);
    }
}
