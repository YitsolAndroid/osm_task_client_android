package com.wdget.yitsol.osm_app.repository.impl;

import android.content.Context;

import com.wdget.yitsol.osm_app.R;
import com.wdget.yitsol.osm_app.model.ProcessMnemonicModel;
import com.wdget.yitsol.osm_app.repository.IActionEditorRepository;
import com.wdget.yitsol.osm_app.service.APIService;
import com.wdget.yitsol.osm_app.service.APIUtils;
import com.wdget.yitsol.osm_app.utils.AppPreference;
import com.wdget.yitsol.osm_app.utils.Encryption;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActionEditorRepository implements IActionEditorRepository

{
    private APIService apiService;

    public ActionEditorRepository() {

        apiService= APIUtils.getAPIService();
    }

    @Override
    public void getProcessMnemonic(Context context, String orderId, String taskId, final OnGetMnemonicByOrderIdCallback callback) {
        try {
            final List<String> mNemoniclist=new ArrayList<>();
            AppPreference appPreference=new AppPreference(context);
            HashMap<String,String> logindetails=appPreference.getUserDetails();
            String username=logindetails.get(appPreference.KEY_USERLOGINUSERNAME);
            String password=logindetails.get(appPreference.KEY_USERLOGINPASSWORD);
            Encryption encryption=new Encryption();
            String encoded_input=encryption.encrypt(username+":"+password);
            Map<String,String> encoded_header=new HashMap<>();
            encoded_header.put("Auth",encoded_input);
            apiService.getProcessMnemonicForOrder(encoded_header,orderId,taskId).enqueue(new Callback<List<ProcessMnemonicModel>>() {
                @Override
                public void onResponse(Call<List<ProcessMnemonicModel>> call, Response<List<ProcessMnemonicModel>> response) {
                    if(callback!=null)
                    {
                        if(response.code()== HttpURLConnection.HTTP_OK)
                        {
                            List<ProcessMnemonicModel> datalist=response.body();
                            if(datalist.size()>0)
                            {
                                for(ProcessMnemonicModel mnemonicModel:datalist)
                                {
                                    mNemoniclist.add(mnemonicModel.getProcessStatusMnemonic());
                                }
                                callback.onSuccess(datalist,mNemoniclist);
                            }
                            else
                            {
                                callback.onError("server error");
                            }

                        }
                    }
                }

                @Override
                public void onFailure(Call<List<ProcessMnemonicModel>> call, Throwable t) {
                    if(callback!=null)
                    {
                        if(call!=null)
                        {
                            call.cancel();
                            callback.onError("server error");
                        }
                    }
                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void getProcessPriority(Context context,OnGetProcessPriorityCallback callback)
    {
        try {
            String[] priorityarray = {"1","2","3","4","5","6","7","8","9"};
            List<String> prioritylist = new ArrayList<>();
           for(int i=0;i<priorityarray.length;i++)
           {
               prioritylist.add(priorityarray[i]);
           }
            if(prioritylist.size()>0)
            {
                if(callback!=null)
                {
                    callback.onSuccess(prioritylist);
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }
}
