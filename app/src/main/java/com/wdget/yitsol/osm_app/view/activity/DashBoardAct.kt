package com.wdget.yitsol.osm_app.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.utils.CommonPopUp
import com.wdget.yitsol.osm_app.utils.ConnectionDetector
import com.wdget.yitsol.osm_app.utils.FontType
import com.wdget.yitsol.osm_app.view.activity.base.BaseActivity
import com.wdget.yitsol.osm_app.view.activity.base.BaseNavigationActivity
import kotlinx.android.synthetic.main.login.*
import kotlinx.android.synthetic.main.select_page.*

class DashBoardAct : BaseActivity() {

    override fun getActContext(): Context {
       return this@DashBoardAct
    }
    var userName: String? = null
    var password: String? = null

    private var ll_worklist: LinearLayout? = null
    private var ll_reporting: LinearLayout? = null
    private var ll_query: LinearLayout? = null

    private var connectionDetector: ConnectionDetector? = null

    private var root: ViewGroup? = null
    private var ll_notifications: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.select_page)
        ll_worklist = findViewById(R.id.ll_worklist) as LinearLayout
        ll_reporting = findViewById(R.id.ll_reporting) as LinearLayout
        ll_query = findViewById(R.id.ll_query) as LinearLayout
        ll_notifications = findViewById(R.id.ll_notifications) as LinearLayout
        root = findViewById(R.id.ll_next) as ViewGroup
        //FontType(applicationContext, root)
        root = findViewById(R.id.ll_heading) as ViewGroup
       // FontType(applicationContext, root)
        connectionDetector = ConnectionDetector(applicationContext)
       // setTextLabelFont(actContext,txt_applabel)

        ll_notifications!!.setOnClickListener {
            if (!connectionDetector!!.isConnectedToInternet()) {
                val commonPopUp = CommonPopUp(
                        this, "Please activate internet...")
                commonPopUp.setCanceledOnTouchOutside(true)
                commonPopUp.show()
            } else {

                val intent = Intent(this,
                        NotificationsActivity::class.java)
                intent.putExtra("selected", "notifications")
                intent.putExtra("flag", 1)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                startActivity(intent)
            }
        }

        ll_reporting!!.setOnClickListener {
            if (!connectionDetector!!.isConnectedToInternet()) {
                val commonPopUp = CommonPopUp(
                        this, "Please activate internet...")
                commonPopUp.setCanceledOnTouchOutside(true)
                commonPopUp.show()
            } else {

                val intent = Intent(this,
                        ReportingActivity::class.java)
                intent.putExtra("selected", "report")
                intent.putExtra("flag", 1)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                startActivity(intent)
            }
        }

        ll_worklist!!.setOnClickListener {
            if (!connectionDetector!!.isConnectedToInternet()) {
                val commonPopUp = CommonPopUp(
                        this, "Please activate internet...")
                commonPopUp.setCanceledOnTouchOutside(true)
                commonPopUp.show()
            } else {
                val intent = Intent(this,
                        WorkListActivity::class.java)
                intent.putExtra("selected", "worklist")
                intent.putExtra("flag", 1)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                startActivity(intent)
            }
        }

        ll_query!!.setOnClickListener {
            if (!connectionDetector!!.isConnectedToInternet()) {
                val commonPopUp = CommonPopUp(
                        this, "Please activate internet...")
                commonPopUp.setCanceledOnTouchOutside(true)
                commonPopUp.show()
            } else {

                val intent = Intent(this,
                        QueryActivity::class.java)
                intent.putExtra("selected", "query")
                intent.putExtra("flag", 1)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                startActivity(intent)
            }
        }


    }


    override fun onBackPressed() {
        //super.onBackPressed()
    }
}
