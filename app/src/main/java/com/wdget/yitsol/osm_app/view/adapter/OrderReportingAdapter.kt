package com.wdget.yitsol.osm_app.view.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.view.menu.ActionMenuItemView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.model.OrdersReportResponse
import com.wdget.yitsol.osm_app.presenter.IOrdersReportingItemClickListener
import com.wdget.yitsol.osm_app.presenter.IReportingPresenter
import com.wdget.yitsol.osm_app.presenter.IWorkListItemClickListener
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver
import kotlinx.android.synthetic.main.txt_lstdescription.view.*

class OrderReportingAdapter(context: Context,ordersReportResponse: MutableList<OrdersReportResponse>?): RecyclerView.Adapter<OrderReportingAdapter.ViewHolder>() {
    private var context:Context?=null
    private var orderReportingResponse:MutableList<OrdersReportResponse>?=null
    private var clickListener: IOrdersReportingItemClickListener ?=null

    init {
        this.context =context
        this.orderReportingResponse = ordersReportResponse
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.txt_lstdescription, parent, false)

        return ViewHolder(v);
       //return ViewHolder(LayoutInflater.from(context).inflate(R.layout.txt_lstdescription,null,false))
    }

    override fun getItemCount(): Int {
        return orderReportingResponse!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItems(orderReportingResponse?.get(position),context,position)
        holder.itemView.ll_orders.setOnClickListener{

            clickListener!!.onItemClicked(orderReportingResponse!!.get(position).mnemonic)
        }
    }

    fun registerItemClickListener(clickListener: IOrdersReportingItemClickListener)
    {
        this.clickListener=clickListener;
    }
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){



        fun bindItems(ordersReportResponse: OrdersReportResponse?,context: Context?,position: Int){

            if(position % 2 ==1){
                itemView.setBackgroundColor(Color.parseColor("#1E90FF"))
            }
            else{
                itemView.setBackgroundColor(Color.parseColor("#87CEFA"))
            }

            itemView.txt_name.text = nameCaptilised(ordersReportResponse?.mnemonic!!)
            itemView.txt_count.text = ordersReportResponse.total

        }
    }


    private fun nameCaptilised(nametoCaps:String):String{
        val name=nametoCaps.substring(0,1).toUpperCase() + nametoCaps.substring(1).toLowerCase()
        return name
    }
}