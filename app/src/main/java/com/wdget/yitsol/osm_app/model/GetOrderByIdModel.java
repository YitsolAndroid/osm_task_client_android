package com.wdget.yitsol.osm_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetOrderByIdModel
{
    @SerializedName("GetOrder.Response")
    @Expose
    private GetOrderResponse getOrderResponse;
    @SerializedName("Status")
    @Expose
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public GetOrderResponse getGetOrderResponse() {
        return getOrderResponse;
    }

    public void setGetOrderResponse(GetOrderResponse getOrderResponse) {
        this.getOrderResponse = getOrderResponse;
    }
}
