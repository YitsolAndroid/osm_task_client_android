package com.wdget.yitsol.osm_app.presenter;

import android.content.Context;

import com.wdget.yitsol.osm_app.presenter.base.IBasePresenter;

public interface IChangeStatusPresenter extends IBasePresenter
{
    void getAllStatusForOrder(Context context,String orderId, String taskId);

    void changeOrderStatus(Context context,String orderId,String taskId,String select_task,String ord_status);
}
