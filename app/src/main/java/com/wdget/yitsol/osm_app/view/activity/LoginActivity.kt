package com.wdget.yitsol.osm_app.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.presenter.ILoginPresenter
import com.wdget.yitsol.osm_app.presenter.impl.LoginPresenter
import com.wdget.yitsol.osm_app.utils.AppConstant
import com.wdget.yitsol.osm_app.utils.ConnectivityReceiver
import com.wdget.yitsol.osm_app.view.activity.base.BaseActivity
import com.wdget.yitsol.osm_app.view.dialog.ChangeIPDialog
import kotlinx.android.synthetic.main.login.*
import kotlinx.android.synthetic.main.login.view.*

class LoginActivity : BaseActivity() {

    override fun getActContext(): Context {
        return this@LoginActivity
    }

    var presenter:ILoginPresenter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        presenter=LoginPresenter(this)
        //setLoginCredential()

        setTextLabelFont(actContext,txt_applabel)

        txt_changesetting.setOnClickListener({
            showChangeIpDialog()
        })

        img_changesetting.setOnClickListener({
            showChangeIpDialog()
        })

        btn_login.setOnClickListener({
            performUserLogin(edt_username,edt_password)
        })
    }

    private fun setLoginCredential() {
        edt_username.setText("admin")
        edt_password.setText("Osmadmin1")
    }

    private fun performUserLogin(edt_username: EditText?, edt_password: EditText?) {
        val connectStatus= ConnectivityReceiver.isConnected()
        if(connectStatus)
        {
            presenter?.performUserLogin(actContext,edt_username,edt_password,ll_login)
        }
        else
        {
            showCustomSnackBar(AppConstant.NO_CONNECTION,ll_login)
        }

    }

    fun showSnackMessage(message:String)
    {
        showCustomSnackBar(message,ll_login)
    }

    private fun showChangeIpDialog() {
        val changeIpDialog=ChangeIPDialog(actContext)
        changeIpDialog.setCanceledOnTouchOutside(false)
        changeIpDialog.show()
    }

    override fun onStart() {
        super.onStart()
        presenter?.subscribeCallbacks();
    }
    override fun onStop() {
        super.onStop()
        presenter?.unSubscribeCallbacks()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }

    fun loginComplete()
    {
        gotoDashBoardScreen()
    }

    private fun gotoDashBoardScreen() {
        val dashboardIntent= Intent(actContext,DashBoardAct::class.java)
        startActivity(dashboardIntent)
    }

}
