package com.wdget.yitsol.osm_app.presenter;

public interface IOrdersReportingItemClickListener {

    void onItemClicked(String orderState);
}
