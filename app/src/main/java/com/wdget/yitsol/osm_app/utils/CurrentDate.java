package com.wdget.yitsol.osm_app.utils;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Class used to create the date with different format.
 */
@SuppressLint("SimpleDateFormat")
public class CurrentDate {
    public String currentDate, kotdate, crt_ts;
    public String fullDateTime, slashDate, onlytime, buyerregisterdate,
            billdate, mealorderdate, sellersubsribedate, appcurrentdate, vyuli_gamedate,reportdate;
    Date date = null;

    public CurrentDate() {
        SimpleDateFormat sdf1 = new SimpleDateFormat("ddMMMyy");
        currentDate = sdf1.format(new Date());

        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        fullDateTime = sdf2.format(new Date());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        vyuli_gamedate = simpleDateFormat.format(new Date());

        SimpleDateFormat sdf3 = new SimpleDateFormat("dd/MM/yyyy");
        slashDate = sdf3.format(new Date());

        SimpleDateFormat sdf4 = new SimpleDateFormat("h:mm a");
        onlytime = sdf4.format(new Date());

        SimpleDateFormat sdf5 = new SimpleDateFormat("ddMMyy");
        kotdate = sdf5.format(new Date());

        SimpleDateFormat sdf6 = new SimpleDateFormat("yyMMdd");
        billdate = sdf6.format(new Date());

        SimpleDateFormat sdf7 = new SimpleDateFormat("yyyy-M-d");
        mealorderdate = sdf7.format(new Date());

        SimpleDateFormat sdf8 = new SimpleDateFormat("dd-MMM-yyyy");
        sellersubsribedate = sdf8.format(new Date());

        SimpleDateFormat sdf9 = new SimpleDateFormat("dd-MMM-yyyy");
        buyerregisterdate = sdf9.format(new Date());

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
        String inputcrt_ts = df.format(new Date());

        SimpleDateFormat appdateformat = new SimpleDateFormat("dd-MM-yyyy");
        appcurrentdate = appdateformat.format(new Date());

        SimpleDateFormat reportformat=new SimpleDateFormat("MM/dd/yyyy");
        reportdate=reportformat.format(new Date());

        DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            date = df.parse(inputcrt_ts);

            crt_ts = outputformat.format(date);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    /**
     * We are getting the Indain timezone Time and Date in 24 hour format
     *
     * @return String
     */
    public String getOrdertime() {

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
        DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sbCurrentTimestamp = null;
        Calendar cSchedStartCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        long gmtTime = cSchedStartCal.getTime().getTime();

        long timezoneAlteredTime = gmtTime + TimeZone.getTimeZone("Asia/Calcutta").getRawOffset();
        Calendar cSchedStartCal1 = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
        cSchedStartCal1.setTimeInMillis(timezoneAlteredTime);


        Date date = cSchedStartCal1.getTime();

        String input_crt_ts = df.format(date);

        Date outputDate = null;
        try {
            outputDate = df.parse(input_crt_ts);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        sbCurrentTimestamp = outputformat.format(outputDate);

        return sbCurrentTimestamp;
    }

    public String getVyuliCurrentDateTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        vyuli_gamedate = simpleDateFormat.format(new Date());
        return vyuli_gamedate;
    }

    public String getVyuliCurrentTime(Date date) {
        String crt_time = "NA";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        crt_time = simpleDateFormat.format(date);
        return crt_time;
    }


    public String getOrdertime(Date passed_date) {

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa");
        DateFormat outputformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sbCurrentTimestamp = null;
        /*Calendar cSchedStartCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        long gmtTime = cSchedStartCal.getTime().getTime();

		long timezoneAlteredTime = gmtTime + TimeZone.getTimeZone("Asia/Calcutta").getRawOffset();
		Calendar cSchedStartCal1 = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
		cSchedStartCal1.setTimeInMillis(timezoneAlteredTime);


		Date date = cSchedStartCal1.getTime();*/

        String input_crt_ts = df.format(passed_date);

        Date outputDate = null;
        try {
            outputDate = df.parse(input_crt_ts);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        sbCurrentTimestamp = outputformat.format(outputDate);

        return sbCurrentTimestamp;
    }

    public String getforamtteddate(String datetoformat) {
        String formatteddate = null;

        SimpleDateFormat sdf7 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        SimpleDateFormat sdf3 = new SimpleDateFormat("MM/dd/yyyy");

        try {
            Date date = sdf7.parse(datetoformat);

            formatteddate = sdf3.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatteddate;
    }


    public boolean menutimevalidator(String cutofftime, String appsystemtime) {
        DateFormat dateintimeformat = new SimpleDateFormat("h:mm a");

        DateFormat appsystimeformat = new SimpleDateFormat("hh:mm:ss");

        Date cutofftimeindate = new Date(), currenttime = new Date();

        //String systime="12:51:20";

        try {
            cutofftimeindate = dateintimeformat.parse(cutofftime);

            //currenttime=dateintimeformat.parse(onlytime);

            currenttime = appsystimeformat.parse(appsystemtime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return cutofftimeindate.getTime() - currenttime.getTime() > 0;

    }


    public String gettommoroworderdate(Date inputdate) {
        Date outputdate;

        String tommorowdate;

        SimpleDateFormat sdf7 = new SimpleDateFormat("yyyy-M-d");


        Calendar calendar = Calendar.getInstance();

        calendar.setTime(inputdate);

        calendar.add(Calendar.DATE, 1);

        outputdate = calendar.getTime();

        tommorowdate = sdf7.format(outputdate);

        return tommorowdate;
    }

    public Date nextdaydate(Date inputdate) {
        Date outputdate;

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(inputdate);

        calendar.add(Calendar.DATE, 1);

        outputdate = calendar.getTime();

        return outputdate;

    }

    public String getmealorderdate(String inputdate) {
        String outputdate = null;

        Date outputDate;

        SimpleDateFormat sdf7 = new SimpleDateFormat("yyyy-MM-dd");

        try {
            outputDate = sdf7.parse(inputdate);

            outputdate = sdf7.format(outputDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputdate;
    }

    public String getafteroneyeardate() {
        String nextyeardate;

        SimpleDateFormat sdf7 = new SimpleDateFormat("dd-MMM-yyyy");

        Calendar cal = Calendar.getInstance();

        Date today = cal.getTime();

        cal.add(Calendar.YEAR, 1); // to get previous year add -1

        Date nextYear = cal.getTime();

        nextyeardate = sdf7.format(nextYear);

        return nextyeardate;
    }

    public String getafterthreeyeardate() {
        String nextyeardate;

        SimpleDateFormat sdf7 = new SimpleDateFormat("dd-MMM-yyyy");

        Calendar cal = Calendar.getInstance();

        Date today = cal.getTime();

        cal.add(Calendar.YEAR, 3); // to get previous year add -1

        Date nextYear = cal.getTime();

        nextyeardate = sdf7.format(nextYear);

        return nextyeardate;
    }

    public String getafterfiveyeardate() {
        String nextyeardate;

        SimpleDateFormat sdf7 = new SimpleDateFormat("dd-MMM-yyyy");

        Calendar cal = Calendar.getInstance();

        Date today = cal.getTime();

        cal.add(Calendar.YEAR, 5); // to get previous year add -1

        Date nextYear = cal.getTime();

        nextyeardate = sdf7.format(nextYear);

        return nextyeardate;
    }

    public Date getdateforcompare(String inputdate) {
        Date appdate = null;

        SimpleDateFormat appdateformat = new SimpleDateFormat("dd-MM-yyyy");

        try {
            appdate = appdateformat.parse(inputdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return appdate;
    }

    public String getOSOrderedPlacedDate(String orderDate) {
        Date serverDateTime;
        String OrderDate = "NA";
        SimpleDateFormat serverCurrformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat appInfoDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            serverDateTime = serverCurrformat.parse(orderDate);
            OrderDate = appInfoDateFormat.format(serverDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return OrderDate;
    }

    public Date getServerGenerateDate(String servertime)
    {
        Date serverDateTime=null;
        SimpleDateFormat serverCurrformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            serverDateTime = serverCurrformat.parse(servertime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return serverDateTime;
    }


    public String getProductPostDate(String orderDate) {
        Date serverDateTime;
        String OrderDate = "NA";
        SimpleDateFormat serverCurrformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat appInfoDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        try {
            serverDateTime = serverCurrformat.parse(orderDate);
            OrderDate = appInfoDateFormat.format(serverDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return OrderDate;
    }

    public String getBSProductPostedDate(String orderDate) {
        Date serverDateTime;
        String OrderDate = "NA";
        SimpleDateFormat serverCurrformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat appInfoDateFormat = new SimpleDateFormat("MM-dd-yy");
        try {
            serverDateTime = serverCurrformat.parse(orderDate);
            OrderDate = appInfoDateFormat.format(serverDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return OrderDate;
    }

    public String getCandidateFormattedDOB(String dob)
    {
        Date serverDateTime;
        String formattedDate = "NA";
        SimpleDateFormat serverCurrformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat appInfoDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            serverDateTime = serverCurrformat.parse(dob);
            formattedDate=appInfoDateFormat.format(serverDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate;
    }


    public String getOSOrderedPlacedTime(String orderDate) {
        Date serverDateTime;
        String OrderTime = "NA";
        SimpleDateFormat serverCurrformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm aa");
        serverCurrformat.setTimeZone(TimeZone.getTimeZone("IST"));
        try {
            serverDateTime = serverCurrformat.parse(orderDate);
            OrderTime = timeformat.format(serverDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return OrderTime;
    }

    public String getQatarFormattedOrderTime(String orderdate) {
        Date serverDateTime;
        String OrderTime = "NA";
        SimpleDateFormat serverCurrformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm aa");
        serverCurrformat.setTimeZone(TimeZone.getTimeZone("GMT+3"));
        try {
            serverDateTime = serverCurrformat.parse(orderdate);
            OrderTime = timeformat.format(serverDateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return OrderTime;
    }

    public String getISTOrdertime(String input_date) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DateFormat outputformat = new SimpleDateFormat("hh:mm a");
        df.setTimeZone(TimeZone.getTimeZone("IST"));
        String sbCurrentTimestamp = null;

        //String input_crt_ts = df.format(passed_date);

        Date outputDate = null;
        try {
            outputDate = df.parse(input_date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        sbCurrentTimestamp = outputformat.format(outputDate);

        return sbCurrentTimestamp;
    }

    public String getPlacedOrderDate(String input_date) {

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy,hh:mm:ss.a");
        DateFormat outputformat = new SimpleDateFormat("dd/MM/yyyy");
        String sbCurrentTimestamp = null;
        Date outputDate = null;
        try {
            outputDate = df.parse(input_date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        sbCurrentTimestamp = outputformat.format(outputDate);

        return sbCurrentTimestamp;
    }

    public String getOrderOnlytime(String input_date) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        DateFormat outputformat = new SimpleDateFormat("dd/MM/yyyy");
        String sbCurrentTimestamp = null;
        Date outputDate = null;
        try {
            outputDate = df.parse(input_date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        sbCurrentTimestamp = outputformat.format(outputDate);

        return sbCurrentTimestamp;
    }

    public String getOSMOrderFormattedDate(String input_date)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss 'PDT'");
        DateFormat outputformat = new SimpleDateFormat("dd/MM/yyyy");
        String sbCurrentTimestamp = null;
        Date outputDate = null;
        try {
            outputDate = df.parse(input_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sbCurrentTimestamp = outputformat.format(outputDate);
        return sbCurrentTimestamp;
    }

    public Date getAfterSevenDelvryDate(String delvrydate) {
        try {
            DateFormat delverydateformat = new SimpleDateFormat("dd/MM/yyyy");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(delverydateformat.parse(delvrydate));
            calendar.add(Calendar.DATE, 7);
            return new Date(calendar.getTimeInMillis());
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public String getFormattedStringDate(Long datevlaue)
    {
        Date newDate=new Date(datevlaue);
        SimpleDateFormat appdateformat = new SimpleDateFormat("dd-MM-yyyy");
        return appdateformat.format(newDate);
    }

    public String getReturnExchangeRemainsDays(Date currentDate, Date delveryDate) {
        try {
            long difference = delveryDate.getTime() - currentDate.getTime();
            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;
            int daysBetween = (int) (difference / (1000 * 60 * 60 * 24));

            //float daysBetween= TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);
            long elapsedDays = difference / daysInMilli;
            difference = difference % daysInMilli;

            long elapsedHours = difference / hoursInMilli;
            difference = difference % hoursInMilli;

            long elapsedMinutes = difference / minutesInMilli;
            difference = difference % minutesInMilli;

            long elapsedSeconds = difference / secondsInMilli;

            if (daysBetween <= 7 && daysBetween > 0)
                return String.valueOf(daysBetween) + " " + "days remain for return/exchange";
            else
                return AppConstant.NOT_AVAILABLE;
        } catch (Exception ex) {
            ex.printStackTrace();
            return AppConstant.NOT_AVAILABLE;
        }
    }
}
