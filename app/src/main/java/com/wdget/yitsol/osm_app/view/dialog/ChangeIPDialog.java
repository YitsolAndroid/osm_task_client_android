package com.wdget.yitsol.osm_app.view.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.wdget.yitsol.osm_app.R;
import com.wdget.yitsol.osm_app.utils.AppPreference;

public class ChangeIPDialog extends AppCompatDialog
{
    private Context context;
    private EditText edt_ip,edt_port;
    private CheckBox chkSave;
    private TextView btn_Ok,btn_Cancel;

    AppPreference appPreference;

    public ChangeIPDialog(Context context) {
        super(context);
        this.context=context;
        appPreference=new AppPreference(context);
    }

    public ChangeIPDialog(Context context, int theme) {
        super(context, theme);
    }

    protected ChangeIPDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().getAttributes().windowAnimations = R.style.OSFilterDialogTheme;
        setContentView(R.layout.change_ip);
        initView();
        edt_ip.setText(appPreference.getPublicIP());
        edt_port.setText(appPreference.getPort());
        btn_Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(chkSave.isChecked())
                {

                    appPreference.savePublicIPAndPort(edt_ip.getText().toString().trim(),edt_port.getText().toString().trim());
                }
                dismiss();
            }
        });
        btn_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void initView() {
        edt_ip=findViewById(R.id.edt_ip);
        edt_port=findViewById(R.id.edt_port);
        chkSave=findViewById(R.id.chkSave);
        btn_Cancel=findViewById(R.id.btn_Cancel);
        btn_Ok=findViewById(R.id.btn_Ok);
    }
}
