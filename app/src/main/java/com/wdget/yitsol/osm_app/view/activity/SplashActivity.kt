package com.wdget.yitsol.osm_app.view.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.wdget.yitsol.osm_app.R
import com.wdget.yitsol.osm_app.view.activity.base.BaseActivity
import java.util.*
import kotlin.concurrent.schedule

class SplashActivity : BaseActivity() {

    override fun getActContext(): Context {
       return this@SplashActivity
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)
        gotoLoginScreen()
    }

    private fun gotoLoginScreen() {
      val handler=Handler();
        val runnable= Runnable {
            gotoNextScreen()
        }
        handler.postDelayed(runnable,1000)
    }

    private fun gotoNextScreen() {
        val loginIntent= Intent(getActContext(), LoginActivity::class.java)
        startActivity(loginIntent)
    }
}
